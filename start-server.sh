#!/bin/bash

if [[ -z "$1" ]];then
    echo "Please provide env!"
    exit 1
fi


pwd=`pwd`
execDir=`dirname $0`
projectPath=`cd ${execDir};pwd`

# Add private modules to NODE_PATH
export NODE_PATH=$NODE_PATH:${projectPath}/modules

env=$1

if [ "${env}" = 'local' ] ;then
    export NODE_ENV=local
    export NODE_LION_ENV=local
    ./bin/index.js --noauth --port 8010 | ./node_modules/.bin/bunyan
elif [ "${env}" = 'dev' ] ;then
    export NODE_ENV=development
    export NODE_LION_ENV=dev
    open "http://localhost:8010/activity/admin"
    ./bin/index.js --noauth --port 8010 | ./node_modules/.bin/bunyan
elif [ "${env}" = 'alpha' ] ;then
    export NODE_ENV=alpha
    export NODE_LION_ENV=alpha
    ./bin/index.js --port 8010 | ./node_modules/.bin/bunyan
elif [ "${env}" = 'beta' ] ;then
    export NODE_ENV=beta
    export NODE_LION_ENV=beta
    ./bin/index.js --port 8010 | ./node_modules/.bin/bunyan
elif [ "${env}" = 'product' ] ;then
    export NODE_ENV=production
    export NODE_LION_ENV=production
    ./bin/index.js --port 8010 | ./node_modules/.bin/bunyan
else
    echo "Wrong env provided!";
    exit 1
fi
    
    


