module.exports = function(req, res, next) {
    req.info = {};

    // default set to shanghai?
    var cityId = req.cookies.cy || 1;
    try {
        cityId = parseInt(cityId, 10);
    } catch (e) {
        return next();
    }

    req.info.cityId = cityId;
    next();
};