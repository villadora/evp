var composite = require('connect-composite'),
    ip = require('./ip'),
    dper = require('./dper'),
    baseInfo = require('./baseInfo');

module.exports = composite(baseInfo, ip, dper);

module.exports.baseInfo = baseInfo;
module.exports.ip = ip;
module.exports.dper = dper;