"use strict";

var _ = require('underscore'),
    moment = require('moment'),
    sanitize = require('obj-validator').sanitize,
    lion = require('common').lion,
    dao = require('../dao'),
    encryption = require('common-encryption');


module.exports = function(req, res, next) {
    var encryptStr = req.cookies.dper;
    if (encryptStr && encryptStr.length > 0) {
        var userInfo = encryption.decryptDefault(encryptStr);
        getUserId(userInfo, function(err, userId) {
            if (err) return next(err);
            if (userId) {
                req.dper = {
                    userId: userId
                };

                var originalUrl = [req.protocol, '://', req.host, req.url].join('');
                // THe activity platform don't need to exclude logout pattern
                lion.get('avatar-biz.logoutExcludePattern', function(err, logoutExcludePattern) {
                    if (err) return next(err);
                    if (logoutExcludePattern && logoutExcludePattern.length) {
                        var re = new RegExp(logoutExcludePattern, 'i');
                        if (re.test(originalUrl)) { // no need to check in black
                            return next();
                        }
                    }

                    isInBlackList(userId, function(err, blocked) {
                        if (err) return next(err);
                        if (blocked)
                            return logout(req, res);

                        // TODO: skip loginLogged, pending for swallow integration
                        next();
                    });
                });
            } else {
                signout(req, res);
                next();
            }
        });
    } else {
        if (req.app && req.app.get('test.mockUser')) {
            req.dper = {
                userId: req.app.get('test.mockUser')
            };
        }

        next();
    }
};

// force logout
function logout(req, res) {
    return res.redirect('http://event.dianping.com/logout');
}


function signout(req, res) {
    req.dper = {};
    res.cookie('dper', '', {
        maxAge: 0
    });
    res.cookie('ll', '', {
        maxAge: 0
    });
}

function isInBlackList(userId, callback) {
    dao.configuration.find('UserBlackListOpen', function(err, open) {
        if (err) return callback(err);
        if (!open) {
            callback(err, false);
        } else {
            dao.userBlackList.contains(userId, function(err, blocked) {
                callback(err, blocked);
            });
        }
    });
}

function getUserId(dper, callback) {
    var tokens = dper.split(/\|/);

    if (!tokens || tokens.length < 5)
        return callback(null, null);

    var userId = sanitize(tokens[0]).toInt();
    if (isNaN(userId))
        return callback(null, null);

    var expired = sanitize(tokens[3]).toInt();
    if (isNaN(expired))
        return callback(null, null);

    if (moment.unix(expired).isBefore(moment()))
        return callback(null, null);


    // cached in lion
    lion.getBoolean('dp-common-web.userauthlevel.check', function(err, val) {
        if (err) return callback(err);

        if (!val)
            return callback(err, userId);

        var level = sanitize(tokens[2]).toInt();
        if (isNaN(level)) {
            return callback(err, null);
        }

        dao.userAuthLevel.find(userId, function(err, dbLevel) {
            if (err || level != dbLevel)
                return callback(null, null);
            else
                return callback(null, userId);
        });
    });
}