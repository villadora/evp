var _s = require('underscore.string'),
    lion = require('common').lion;


module.exports = function(req, res, next) {
    var ip;
    lion.get('avatar-biz.IPHttpHeader', function(err, ipheader) {
        if (err) return next(err);
        if (ipheader)
            ip = req.get(ipheader);

        if (_s.isBlank(ip))
            ip = req.get('x-forwarded-for'); // reverse proxy
        if (_s.isBlank(ip))
            ip = req.get('proxy-client-ip');
        if (_s.isBlank(ip))
            ip = req.get('wl-proxy-client-ip'); // apache+weblogic
        if (_s.isBlank(ip))
            ip = req.connection.remoteAddress;

        if (_s.isBlank(ip)) ip = "0.0.0.0"; // default ip

        var ips = ip.split(',').map(function(i) {
            return _s.trim(i);
        }).filter(function(s) {
            return s.length;
        });

        initFilterList(function(err) {
            var i = 0,
                len = ips.length;

            for (; i < len; ++i) {
                if (!isFilter(ips[i])) {
                    req.ip = ips[i];
                    break;
                }
            }

            if (i === len)
                req.ip = ips[len - 1];
            next();
        });
    });
};


var filterList = [];


function initFilterList(callback) {
    if (!filterList.length) {
        // TODO: monitor lion change
        lion.get('dp-common-web.UserIP.filter', function(err, ipFilter) {
            if (!ipFilter) return callback(err);
            if (ipFilter) {
                var ips = ipFilter.split('|');
                ips.forEach(function(filterIp) {
                    if (!filterIp)
                        return;
                    filterIp = _s.trim(filterIp);
                    if (/\*$/.test(filterIp)) {
                        filterIp = filterIp.substring(0, filterIp.length - 1);
                    }
                    filterList.push(filterIp);
                });
            }
            callback();
        });
    } else {
        callback();
    }
}


function isFilter(ip) {
    if (!ip) return true;
    ip = _s.trim(ip);
    for (var i = 0, len = filterList.length; i < len; ++i) {
        if (_s.startsWith(ip, filterList[i]))
            return true;
    }

    return false;
}