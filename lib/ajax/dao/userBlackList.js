"use strict";

var mysql = require('persist').mysql,
    cache = require('lru-cache')({
        max: 2000,
        length: function(n) {
            return 1;
        },
        maxAge: 24 * 60 * 60 * 1000
    });



module.exports.contains = function(userId, callback) {
    if (cache.has(userId))
        return callback(null, cache.get(userId));

    mysql.writeConnection(function(err, connection) {
        connection.query("SELECT Count(*) as Count FROM DP_UserBlackList WHERE UserID = ?", [userId], function(err, rows) {
            connection.release(); // release connection first, then call callback
            if (err) return callback(err);
            if (rows && rows.length) {
                var blocked = !! rows[0].Count;
                cache.set(userId, blocked);
                callback(err, blocked);
            } else {
                cache.set(userId, false);
                callback(err, false);
            }
        });
    });
};

module.exports.reset = function() {
    cache.reset();
};