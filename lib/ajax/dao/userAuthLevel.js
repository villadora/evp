"use strict";

var mysql = require('persist').mysql,
    cache = require('lru-cache')({
        max: 2000,
        length: function(n) {
            return 1;
        },
        maxAge: 24 * 60 * 60 * 1000
    });


module.exports.find = function(userId, callback) {
    if (cache.has(userId))
        return callback(null, cache.get(userId));

    mysql.readConnection(function(err, connection) {
        connection.query("SELECT Level FROM DP_UserAuthenticationLevel WHERE UserID = ?", [userId], function(err, rows) {
            connection.release();
            if (err) return callback(err);
            if (rows && rows.length > 0) {
                cache.set(userId, rows[0].Level);
                callback(err, rows[0].Level);
            } else {
                callback(err, null);
            }
        });
    });
};

module.exports.add = function(userId, level, callback) {
    mysql.writeConnection(function(err, connection) {
        connection.query("INSERT INTO DP_UserAuthenticationLevel (UserID, Level, AddTime, UpdateTime) Values (?, ?, NOW(), NOW())", [userId, level], function(err, result) {
            connection.release();
            callback(err, result);
        });
    });
};

module.exports.update = function(userId, level, callback) {
    mysql.writeConnection(function(err, connection) {
        connection.query('UPDATE DP_UserAuthenticationLevel SET Level = ?, UpdateTime = NOW() WHERE UserID = ?', [level, userId], function(err, result) {
            connection.release();
            cache.del(userId);
            callback(err, result);
        });
    });
};


module.exports.reset = function() {
    cache.reset();
};