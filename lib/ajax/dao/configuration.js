"use strict";

var mysql = require('persist').mysql,
    cache = require('lru-cache')({
        max: 100,
        length: function(n) {
            return 1;
        },
        maxAge: 48 * 60 * 60 * 1000
    });


module.exports.findAll = function(callback) {
    mysql.readConnection(function(err, connection) {
        connection.query("SELECT ConfigName, StrSettings FROM DP_Configuration", function(err, rows) {
            connection.release();
            if (err) return callback(err);
            if (rows && rows.length) {
                rows.forEach(function(cfg) {
                    cache.set(cfg.ConfigName, cfg.StrSettings);
                });

                callback(err, rows);
            } else {
                callback(err, null);
            }
        });
    });
};

module.exports.find = function(configName, callback) {
    if (cache.has(configName))
        return callback(null, cache.get(configName));

    mysql.readConnection(function(err, connection) {
        connection.query("SELECT ConfigName, StrSettings FROM DP_Configuration WHERE ConfigName = ?", [configName], function(err, rows) {
            connection.release();
            if (err) return callback(err);
            if (rows && rows.length) {
                cache.set(configName, rows[0].StrSettings);
                callback(err, rows[0].StrSettings);
            } else
                callback(err, null);
        });
    });
};

module.exports.reset = function() {
    cache.reset();
};