var express = require('express'),
    persist = require('persist'),
    sms = require('sms-service'),
    moment = require('moment'),
    components = require('event-components'),
    objValidator = require('obj-validator'),
    sanitize = objValidator.sanitize,
    log = require('common').logger.createLogger('ajax:generator');

module.exports = function(event) {
    // generate hooks
    var app = express();
    app.use(function(req, res, next) {
        var db = req.conn = persist.create();
        db.events.get(event._id, function(err, event) {
            if (err) return next(err);
            if (!event) return res.status(404).send(res.__('The event %s does not exist', event._id));

            if (event.cityId && event.cityId !== req.info.cityId) {
                return res.status(400).send(res.__('The event is not opened in your city'));
            }

            req.event = event;
            req.sms = sms;

            req.apiDebug = sanitize(req.param('apiDebug')).toBoolean();

            var now = moment();
            if (now.isAfter(event.endTime))
                return res.status(410).send(res.__('The event you visited was offline'));
            else if (now.isBefore(event.startTime) && !req.apiDebug)
                return res.status(410).send(res.__('The event you visited does not start yet'));

            next();
        });
    });

    // app.get('/', function(req, res) {
    //     res.send(event);
    // });

    components.generate(app, event);
    return app;
};