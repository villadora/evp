"use strict";

var _ = require('underscore'),
    express = require('express'),
    ajax = module.exports = express(),
    moment = require('moment'),
    persist = require('persist'),
    log = require('common').logger.createLogger('ajax');

//==================
// Extension
//==================
_.extend(ajax, {
    __hooked: {},
    unmount: function(route) {
        // find in stack
        for (var i = 0; i < this.stack.length; ++i) {
            if (this.stack[i].route === route)
                this.stack.splice(i--, 1);
        }
    },
    reset: function() {
        this.__hooked = {};
        for (var method in this.routes)
            delete this.routes[method];

        for (var i = 0; i < this.stack.length; ++i) {
            if (/^\/\d+/.test(this.stack[i].route))
                this.stack.splice(i--, 1);
        }
    },
    findHook: function(eventId) {
        return this.__hooked[eventId];
    },
    hook: function(eventId, hooker) {
        if (!_.isFunction(hooker))
            hooker = this.generator(hooker);


        if (/^\d+$/.test(eventId + '')) {
            log.info('Hook event: ' + eventId);
            if (hooker.handle && hooker.set && hooker.init && hooker.use) {
                // Duck Typing, is a connect application
                this.unhook(eventId);
                this.__hooked[eventId] = hooker;

                this.use('/' + eventId, hooker);
                var handler = this.stack[this.stack.length - 1].handle;

                // reorder the hooker just before router
                for (var i = 0; i < this.stack.length; ++i) {
                    if (this.stack[i].handle == this.router) {
                        this.stack.splice(i, 0, this.stack.pop());
                        break;
                    }
                }
                this.emit('hooked', eventId, hooker);

                return handler;

            } else if (typeof hooker === 'function' && hooker.length == 2) {
                this.unhook(eventId);
                this.__hooked[eventId] = hooker;
                this.all('/' + eventId, hooker);
                this.emit('hooked', eventId, hooker);
                return hooker;
            }
        }
        return;
    },
    unhook: function(eventId) {
        if (/^\d+$/.test(eventId + '')) {
            // find in stack
            this.unmount('/' + eventId);

            // remove from routes
            for (var method in this.routes) {
                var routes = this.routes[method];
                for (var i = 0; i < routes.length; ++i) {
                    var router = routes[i],
                        path = router.path;
                    if (path === '/' + eventId)
                        routes.splice(i--, 1);
                }
                if (!routes.length)
                    delete this.routes[method];
            }

            var hooker;
            if (this.__hooked.hasOwnProperty(eventId)) {
                hooker = this.__hooked[eventId];
                delete this.__hooked[eventId];
                this.emit('unhooked', eventId);
            }
            log.info('Unhook event: ' + eventId);
            return hooker;
        }
    }
});



function unmatch(req, res, next) {
    var url = req.url,
        capture = /^\/(\d+)\//.exec(url);
    log.info(url);
    if (capture) {
        var id = parseInt(capture[1], 10);

        log.info('Unmatch for event: ' + id);
        persist.create().events.get(id, function(err, event) {
            if (err) return next(err);
            if (event) {
                var now = moment();
                if (now.isAfter(event.endTime))
                    res.status(400).send(res.__('The event you visited was offline'));
                // Still online debug event
                // else if (now.isBefore(event.startTime))
                //     res.status(400).send(res.__('The event you visited does not start yet'));
                else {
                    // find a match event but not online
                    // 1. hook this event
                    var handler = ajax.hook(event._id, ajax.generator(event)),
                        oldUrl = oldUrl;

                    // 2. handle this request
                    req.url = req.url.substring(capture[0].length - 1);
                    console.log('after capture', req.url);
                    handler(req, res, function(err) {
                        req.url = oldUrl;
                        next(err);
                    });
                }
            } else {
                res.status(404).send(res.__('The event %s does not exist', id));
            }
        });
    } else
        next();
}

ajax.use(function(req, res, next) {
    if (req.query.callback) {
        // use jsonp
        var oldSend = res.send;
        res.send = function() {
            res.jsonp.apply(this, arguments);
        };
    } else
        next();
});

ajax.use(express.cookieParser()); // set coookie
ajax.use(require('./middleware'));
// add router first
ajax.use(ajax.router);
ajax.use(unmatch);

ajax.get('/', function(req, res) {
    res.send(res.__('OK'));
});


// export generator
module.exports.generator = require('./generator');