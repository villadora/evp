var express = require('express'),
    app = express(),
    path = require('path'),
    log = require('common').logger.createLogger('staging');

app.use('/', express.static(path.join(__dirname, 'ftp')));

module.exports = app;
