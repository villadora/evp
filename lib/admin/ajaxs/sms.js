"use strict";

var _ = require('underscore'),
    moment = require('moment'),
    sms = require('sms-service'),
    app = require('express')(),
    validator = require('obj-validator'),
    Checker = validator.Checker,
    check = validator.check,
    sanitize = validator.sanitize,
    persist = require('persist'),
    entitlement = require('entitlement'),
    Roles = entitlement.Roles,
    log = require('common').logger.createLogger('admin:ajaxs:sms');


app.get('/records/resend/:recordId', function(req, res) {
    var recordId = sanitize(req.params.recordId).xss(),
        db = persist.create();

    db.smses.get(recordId, function(err, record) {
        if (err) return res.status(500).send(res.__(err.message));
        if (!record) return res.status(404).send(res.__('Can not find record'));

        if (!/^\+?[1-9]{1}[0-9]{7,11}$/.test(record.mobileNo)) {
            return res.send({
                smsCode: 400,
                message: res.__('Invalid phone number')
            });
        }

        sms.send(sms.Types.EVENTS, record.mobileNo, {
            'body': record.content
        }, function(err, code, msg) {
            // store to record
            db.smses.modify(recordId, {
                code: err ? 500 : code,
                err: err,
                msg: msg
            });

            if (err) {
                res.status(500).send(res.__(msg));
            } else
                res.send({
                    smsCode: code,
                    recordId: recordId,
                    message: msg
                });
        });
    });
});

var recDelHandler = function(req, res) {
    var recordId = sanitize(req.params.recordId).xss(),
        db = persist.create();

    db.smses.destroy(recordId, function(err, num) {
        if (err) return res.status(500).send(res.__(err.message));
        res.send({
            deleted: num
        });
    });
};

app.del('/records/:recordId', recDelHandler);
app.get('/records/del/:recordId', recDelHandler);


app.getAndPost = function() {
    app.get.apply(app, arguments);
    return app.post.apply(app, arguments);
};


app.getAndPost('/records/:page', function(req, res) {
    var page = sanitize(req.params.page).toInt(),
        limit = sanitize(req.param('limit')).toInt() || 10,
        eventId = req.param('eventId'),
        mobileNo = sanitize(req.param('mobileNo')).xss(),
        content = sanitize(req.param('content')).xss(),
        query = {},
        db = persist.create();

    if (eventId)
        query.eventId = sanitize(eventId).toInt();

    if (mobileNo)
        query.mobileNo = {
            $regex: ".*" + mobileNo + ".*"
        };

    if (content)
        query.content = {
            $regex: ".*" + content + ".*"
        };

    // find normal records
    query.code = 200;

    db.smses.numOf(query, function(err, num) {
        if (err) return res.status(500).send(res.__(err.message));
        db.smses.query(query, page - 1, limit, function(err, results) {
            if (err) return res.status(500).send(res.__(err.message));
            results.forEach(function(rec) {
                rec.create && (rec.create = moment(rec.create).format('YYYY-MM-DD HH:mm:ss'));
                rec.modifiedOn && (rec.modifiedOn && (rec.modifiedOn = moment(rec.modifiedOn).format('YYYY-MM-DD HH:mm:ss')));
            });

            res.send({
                total: Math.ceil(num / limit),
                list: results
            });
        });
    });
});

app.getAndPost('/records/:page/failed', function(req, res) {
    var page = sanitize(req.params.page).toInt(),
        limit = sanitize(req.param('limit')).toInt() || 10,
        eventId = req.param('eventId'),
        mobileNo = sanitize(req.param('mobileNo')).xss(),
        content = sanitize(req.param('content')).xss(),
        query = {},
        db = persist.create();

    if (eventId)
        query.eventId = sanitize(eventId).toInt();

    if (mobileNo)
        query.mobileNo = {
            $regex: ".*" + mobileNo + ".*"
        };

    if (content)
        query.content = {
            $regex: ".*" + content + ".*"
        };

    query.code = {
        $ne: 200
    };

    db.smses.numOf(query, function(err, num) {
        if (err) return res.status(500).send(res.__(err.message));
        db.smses.query(query, page - 1, limit, function(err, results) {
            if (err) return res.status(500).send(res.__(err.message));
            results.forEach(function(rec) {
                rec.create = moment(rec.create).format('YYYY-MM-DD HH:mm:ss');
                rec.modifiedOn && (rec.modifiedOn = moment(rec.modifiedOn).format('YYYY-MM-DD HH:mm:ss'));
            });

            res.send({
                total: Math.ceil(num / limit),
                list: results
            });
        });
    });
});

module.exports = app;