var _ = require('underscore'),
    objValidator = require('obj-validator'),
    xlsx = require('node-xlsx'),
    Checker = objValidator.Checker,
    check = objValidator.check,
    sanitize = objValidator.sanitize,
    persist = require('persist'),
    entitlement = require('entitlement'),
    Roles = entitlement.Roles,
    log = require('common').logger.createLogger('admin:events:results');

function analysQuery(req) {
    var obj = {}, query = {}, orig = {},
        type = sanitize(req.params.type).xss();

    if (req.method === 'GET') orig = req.query;
    else if (req.method === 'PUT' || req.method === 'POST') orig = req.body;
    else if (req.method === 'DELETE') {
        if (req.body && !_.isEmpty(req.body))
            orig = req.body;
        else if (req.query && !_.isEmpty(req.query))
            orig = req.query;
    }

    for (var key in orig) {
        if (orig.hasOwnProperty(key))
            if (/^-?\d+$/.test(orig[key]))
                obj[key] = sanitize(orig[key]).toInt();
            else
                obj[key] = sanitize(orig[key]).xss();
    }

    delete obj.limit;


    if ('range_start' in obj && 'range_end' in obj) {
        obj.range = obj.range || {};
        obj.range.start = obj.range_start;
        obj.range.end = obj.range_end;
        delete obj.range_start;
        delete obj.range_end;
    }

    if ('range' in obj) {
        if (obj.range.start && obj.range.end) {
            obj.range.start = new Date(obj.range.start);
            obj.range.end = new Date(obj.range.end);
        } else
            delete obj.range;
    }

    // TODO: add more fields support

    if (obj.range) {
        query.create = {
            $gte: obj.range.start,
            $lte: obj.range.end
        };
        delete obj.range;
    }

    for (var p in obj) {
        if (obj[p] === 0 || obj[p])
            if (/\.\*/.test(obj[p])) {
                try {
                    query[p] = new RegExp(obj[p]);
                } catch (e) {
                    query[p] = obj[p];
                }
            } else
                query[p] = obj[p];
    }
    return query;
}


var results = {
    create: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            debug = req.event_debug,
            obj = _.omit(req.body, '_id'),
            db = persist.create(),
            results = debug ? db.results_debug : db.results;

        //  if(isNaN(aid) return res.status(400).send(res.__('The property is not valid: [%s] %s', 'event id', req.params.aid));

        Object.keys(obj).forEach(function(key) {
            obj[key] = sanitize(obj[key]).xss();
        });

        obj.createOn = new Date();

        results.create(aid, [obj], function(err) {
            if (err) return res.status(500).send(res.__(err.message));
            res.status(202).send();
        });
        return this;
    },
    get: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            rid = sanitize(req.params.rid).xss(),
            debug = req.event_debug,
            db = persist.create(),
            results = debug ? db.results_debug : db.results;

        if (isNaN(aid)) return res.status(400).send(res.__('Event Id %s is invalid', aid));

        results.get(aid, rid, function(err, rec) {
            if (err) return res.status(500).send(res.__(err.message));

            if (!rec) return res.status(404).send(null);

            res.send(rec);
        });
        return this;
    },
    destroy: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            rid = sanitize(req.params.rid).xss(),
            debug = req.event_debug,
            db = persist.create(),
            results = debug ? db.results_debug : db.results;

        if (isNaN(aid)) return res.status(400).send(res.__('Event Id %s is invalid', aid));

        results.destroy(aid, [rid], function(err, num) {
            if (err) return res.status(500).send(res.__(err.message));
            res.send({
                deleted: num
            });
        });

        return this;
    },
    update: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            rid = sanitize(req.params.rid).xss(),
            debug = req.event_debug;
        res.status(404).send();
    },
    count: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            type = sanitize(req.params.type).xss(),
            debug = req.event_debug,
            db = persist.create(),
            results = debug ? db.results_debug : db.results,
            query = analysQuery(req);

        if (isNaN(aid)) return res.status(400).send(res.__('Event Id %s is invalid', aid));

        if (type) query.type = type;

        results.numOf(aid, query, function(err, num) {
            if (err)
                return res.status(500).send(res.__(err.message));
            res.send(num + '');
        });
        return this;
    },
    paginate: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            debug = req.event_debug,
            type = sanitize(req.params.type).xss(),
            page = sanitize(req.params.page).toInt() || 1,
            limit = sanitize(req.param('limit')).toInt() || 20,
            query = analysQuery(req),
            db = persist.create(),
            results = debug ? db.results_debug : db.results;

        if (isNaN(req.params.page) || page < 1) return res.status(400).send(res.__('Invalid page number: %s', req.params.page));

        if (isNaN(aid)) return res.status(400).send(res.__('Event Id %s is invalid', aid));


        if (type) query.type = type;

        results.numOf(aid, query, function(err, num) {
            if (err) return res.status(500).send(res.__(err.message));
            results.paginate(aid, query, page - 1, limit, function(err, records) {
                if (err) return res.status(500).send(res.__(err.message));
                res.send({
                    total: Math.ceil(num / limit),
                    list: records
                });
            });
        });
    },
    multiGet: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            debug = req.event_debug,
            query = analysQuery(req),
            db = persist.create(),
            results = debug ? db.results_debug : db.results;
        if (isNaN(aid)) return res.status(400).send(res.__('Event Id %s is invalid', aid));

        results.query(aid, query, function(err, records) {
            if (err) return res.status(500).send(res.__(err.message));
            res.send(records);
        });
    },
    multiDel: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            debug = req.event_debug,
            query = analysQuery(req),
            db = persist.create(),
            results = debug ? db.results_debug : db.results;

        if (isNaN(aid)) return res.status(400).send(res.__('Event Id %s is invalid', aid));

        results.destroy(aid, query, function(err, num) {
            if (err) return res.status(500).send(res.__(err.message));
            res.send(num + '');
        });
    },
    exportXlsx: function(req, res) {
        var aid = sanitize(req.params.aid).toInt(),
            debug = req.event_debug,
            query = analysQuery(req),
            type = sanitize(req.params.type).xss(),
            db = persist.create(),
            results = debug ? db.results_debug : db.results;

        if (type)
            query.type = type;

        results.query(aid, query, function(err, records) {
            var data = [];
            if (!err && records.length > 0) {
                var take = records[0],
                    cols = [];
                for (var prop in take) {
                    if (prop != '_id') {
                        if (type == 'comment' && prop == 'likeUsers')
                            continue;

                        if (typeof take[prop] === 'object') {
                            // expand first level only
                            for (var inner in take[prop]) {
                                cols.push(prop + '.' + inner);
                            }
                        } else
                            cols.push(prop);
                    }
                }

                data.push(cols);
                records.forEach(function(rec) {
                    var rows = [];
                    cols.forEach(function(prop) {
                        var props = prop.split('.'),
                            value = rec;

                        while (props.length > 0) {
                            if (value)
                                value = value[props.shift()];
                        }

                        if (typeof value === 'object') {
                            try {
                                value = JSON.stringify(value);
                            } catch (e) {
                                value = value.toString();
                            }
                        }

                        rows.push({
                            'value': value.toString(),
                            'formatCode': 'General'
                        });
                    });

                    data.push(rows);
                });
            }

            var buffer = xlsx.build({
                worksheets: [{
                    "name": '活动结果',
                    'data': data
                }]
            });

            res.contentType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.set('Content-Disposition', 'attachment; filename=results_' + aid + '.xlsx');
            res.set('Content-Length', buffer.length);
            res.end(buffer, 'binary');
        });

    }
};


module.exports = results;