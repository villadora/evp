"use strict";

var _ = require('underscore'),
    moment = require('moment'),
    app = require('express')(),
    params = require('express-params'),
    validator = require('obj-validator'),
    Checker = validator.Checker,
    check = validator.check,
    sanitize = validator.sanitize,
    persist = require('persist'),
    entitlement = require('entitlement'),
    Roles = entitlement.Roles,
    resultsHook = require('./results'),
    async = require('async'),
    eventValidator = require('common').eventValidator,
    log = require('common').logger.createLogger('admin:ajaxs:event');



/// check for event existense
app.use(function(req, res, next) {
    if (/\/\d+\/results\//.test(req.url)) {
        var capture = /\/(\d+)\/results\//.exec(req.url);
        if (capture) {
            var aid = sanitize(capture[1]).toInt();
            persist.create().events.get(aid,
                function(err, item) {
                    if (err) return res.status(500).send(res.__(err.message));
                    if (!item) return res.status(404).send(res.__('The event %s does not exist', aid));
                    req.event = item;
                    // TODO:
                    if (req.param('debug'))
                        req.event_debug = sanitize(req.param('debug')).toBoolean();

                    next();
                });
        }
    } else
        next();
});

app.use('/draft', require('./draft'));

// add params support
params.extend(app);


// analys query
function analyseQuery(req) {
    var name = sanitize(req.param('name')).xss(),
        active = sanitize(req.param('active')).xss(),
        components = sanitize(req.param('components')).xss(),
        owner = sanitize(req.param('owner')).xss(),
        eventId = req.param('eventId'),
        range;


    try {
        var range_start = sanitize(req.param('range_start')).xss(),
            range_end = sanitize(req.param('range_end')).xss();


        range_start && check(range_start).isDate();
        range_start && range_end && check(range_end).isDate().isAfter(range_start);

        if (range_start || range_end) {
            range = {};

            if (range_start)
                range.start = new Date(range_start);
            if (range_end)
                range.end = new Date(range_end);
        }
    } catch (e) {
        // ignore the range if invalid
        range = undefined;
    }



    var query = {};

    if (eventId) {
        query._id = sanitize(eventId).toInt();
    }

    if (name) query.name = {
        $regex: '.*' + name + '.*',
        $options: 'i'
    };

    if (owner) query.owner = owner;


    if (components) {
        try {
            if (typeof components === 'string')
                components = components.split(',');
        } catch (e) {
            components = undefined;
        }

        if (!components instanceof Array) {
            components = Object.keys(components);
        }

        components = components.map(function(val) {
            return sanitize(val).xss();
        });

        if (components.length) {
            components.forEach(function(val) {
                query['components.' + val] = {
                    $exists: true
                };
            });
        }
    }

    if (range) {
        query.$or = [{
            startTime: {}
        }, {
            endTime: {}
        }];

        if (range.start) {
            query.$or[0].startTime.$gte = range.start;
            query.$or[1].endTime.$gte = range.start;
        }
        if (range.end) {
            query.$or[0].startTime.$lte = range.end;
            query.$or[1].endTime.$lte = range.end;
        }
    }


    if (active) {
        active = sanitize(active).toBoolean();
    }

    if (active === true) {
        query.startTime = query.startTime || {};
        query.endTime = query.endTime || {};
        query.startTime.$lte = new Date();
        query.endTime.$gte = new Date();
    } else if (active === false) {
        if (!query.$or) {
            query.$or = [{
                startTime: {}
            }, {
                endTime: {}
            }];
        }

        query.$or[0].startTime.$gt = new Date();
        query.$or[1].endTime.$lt = new Date();
    }

    return query;
}


//==========================
// register routes`
//==========================
var actions = {
    // PUT POST
    create: function(req, res) {
        if (_.isArray(req.body))
            return res.status(400).send(res.__('The property is not valid: [%s] %s', 'event', req.body.toString()));
        // save to db
        var db = persist.create(),
            did = sanitize(req.body._id).xss();

        // delete draft

        // create/activity
        var eventObj = eventValidator(req.body),
            createLog = {
                createdBy: (req.user ? req.user.uid : 'anonymouse'),
                createdOn: new Date()
            };

        // mandatory properties
        ['name', 'startTime', 'endTime', 'components'].forEach(function(key) {
            if (!eventObj.hasOwnProperty(key))
                return res.status(400).send(res.__('The property is required: [%s] %s', key, eventObj[key]));
        });

        // manage components
        eventObj.components = eventObj.components || {};
        eventObj.create = createLog;

        async.waterfall([

            function(cb) {
                if (did) {
                    db.drafts.query(did, function(err, drafts) {
                        // not found this event
                        if (err) return cb({
                            status: 500,
                            message: err.message
                        });

                        if (drafts.length) {
                            var data = drafts[0].eventObj;
                            // CHeck whetehr use can change this event
                            if (req.user && req.user.role < Roles.ADMIN) {
                                if (data.create.createdBy !== "anonymouse" && data.create.createdBy != req.user.uid) {
                                    cb({
                                        status: 401,
                                        message: "You have no permission to perform this action"
                                    });
                                }
                            }
                            db.drafts.destroy({
                                _id: did
                            }, function(err) {});
                        }
                        cb();
                    });
                } else {
                    cb();
                }
            }
        ], function(err) {
            if (err)
                return res.status(err.status || 500, res.__(err.message || err.toString()));

            // get events seq id
            db.counter.nextSeq('events', function(err, id) {
                if (err) throw err;

                // set id
                eventObj._id = id;
                db.events.create(eventObj, function(err) {
                    if (err) return res.status(500).send(res.__(err.message));
                    res.send(eventObj);
                });
            });
        });
    },
    // PATCH PUT
    update: function(req, res) {
        var id = sanitize(req.params.aid).toInt(),
            updates = eventValidator(req.body),
            db = persist.create();

        db.events.query({
            _id: id
        }, function(err, events) {
            // not found this event
            if (events.length === 0)
                return res.status(400).send(res.__('The event %s does not exist', id));

            var data = events[0];

            // Check whetehr use can change this event
            if (req.user && req.user.role < Roles.ADMIN) {
                if (data.create.createdBy !== "anonymouse" && data.create.createdBy != req.user.uid) {
                    return res.status(401).send(res.__("You have no permission to perform this action"));
                }
            }


            var changeLog = {
                changedBy: (req.user ? req.user.uid : 'anonymouse'),
                changedOn: new Date()
            };


            if (_.isEmpty(updates.components)) {
                delete updates.components;
            }

            db.events.modify(id, updates,
                function(err, updated) {
                    if (err) return res.status(500).send(res.__(err.message));
                    res.send(updated);
                });
        });
    },
    // DELETE GET
    destroy: function(req, res) {
        var id = sanitize(req.params.aid).toInt(),
            db = persist.create();

        db.events.query({
            _id: id
        }, function(err, events) {
            // not found this event
            if (events.length === 0)
                return res.status(404).send(res.__('The event %s does not exist', id));

            var data = events[0];

            // CHeck whetehr use can change this event
            if (req.user && req.user.role < Roles.ADMIN) {
                if (data.create.createdBy !== "anonymouse" && data.create.createdBy != req.user.uid) {
                    return res.status(401).send(res.__("You have no permission to perform this action"));
                }
            }

            db.events.destroy({
                _id: id
            }, function(err) {
                if (err) return res.status(500).send(res.__(err.message));
                res.send({
                    id: id
                });
            });
        });
    },
    get: function(req, res) {
        var id = sanitize(req.params.aid).toInt(),
            db = persist.create();

        db.events.query({
            _id: id
        }, function(err, events) {
            if (err) return res.status(500).send(res.__(err.message));

            if (events.length === 0)
                return res.status(404).send(res.__('The event %s does not exist', id));

            events[0].startTime = moment(events[0].startTime).format('YYYY-MM-DD HH:mm:ss');
            events[0].endTime = moment(events[0].endTime).format('YYYY-MM-DD HH:mm:ss');

            res.send(events[0]);
        });
    },
    multiGet: function(req, res) {
        var query = analyseQuery(req);

        persist.create().events.query(query, function(err, events) {
            if (err) return res.stauts(500).send(res.__(err.message));

            events.forEach(function(evt) {
                evt.startTime = moment(evt.startTime).format('YYYY-MM-DD HH:mm:ss');
                evt.endTime = moment(evt.endTime).format('YYYY-MM-DD HH:mm:ss');
            });
            res.send(events);
        });
    },
    multiDel: function(req, res) {
        var query = analyseQuery(req);

        if (req.user && req.user.role < Roles.ADMIN) {
            query['create.createBy'] = req.user.uid;
        }


        persist.create().events.destroy(query, function(err, num) {
            if (err) return res.status(500).send(res.__(err.message));

            res.send(num + '');
        });
    },
    paginate: function(req, res) {
        var page = sanitize(req.params.page).toInt(),
            limit = sanitize(req.param('limit')).toInt() || 20,
            query = analyseQuery(req),
            db = persist.create();

        if (isNaN(req.params.page) || page < 1) return res.status(400).send(res.__('Invalid page number: %s', req.params.page));

        db.events.numOf(query, function(err, num) {
            if (err) return res.status(500).send(res.__(err.message));
            db.events.paginate(query, page - 1 || 0, limit, function(err, events) {
                if (err) return res.status(500).send(res.__(err.message));
                events.forEach(function(evt) {
                    evt.startTime = moment(evt.startTime).format('YYYY-MM-DD HH:mm:ss');
                    evt.endTime = moment(evt.endTime).format('YYYY-MM-DD HH:mm:ss');
                });

                res.send({
                    total: Math.ceil(num / limit),
                    list: events
                });
            });
        });
    },
    getComponent: function(req, res) {
        var id = sanitize(req.params.aid).toInt(),
            db = persist.create();

        db.events.findOne({
            _id: id
        }, {
            fields: {
                components: 1
            }
        }, function(err, doc) {
            if (err) return res.status(500).send(res.__(err.message));

            if (doc)
                res.send(doc.components);
            else
                res.status(404).send(res.__("The event %s does not exist", id));
        });
    },
    updateComponent: function(req, res) {
        var id = sanitize(req.params.aid).toInt(),
            db = persist.create(),
            components = req.body;

        components = eventValidator.checkComponents(components);

        db.events.update({
            _id: id
        }, {
            $set: {
                components: components
            }
        }, {
            w: 1
        }, function(err) {
            if (err) return res.status(500).send(res.__(err.message));

            res.send(components);
        });
    }
};

app.param('aid', /^\d+$/);

/**
 * Event Restful APIs
 */

// CREATE
app.post('/', entitlement.roleWriter, actions.create);
app.put('/', entitlement.roleWriter, actions.create);

// READ
app.get('/:aid', entitlement.roleReader, actions.get);


// UPDATE
app.put('/:aid', entitlement.roleWriter, actions.update);
app.patch('/:aid', entitlement.roleWriter, actions.update);


// DELETE
app.del('/:aid', entitlement.roleWriter, actions.destroy);
app.get('/:aid/delete', entitlement.roleWriter, actions.destroy);

// Page
app.get('/page/:page', entitlement.roleReader, actions.paginate);
app.post('/page/:page', entitlement.roleReader, actions.paginate);

// Multi-Ops
app.get('/query', entitlement.roleReader, actions.multiGet);
app.post('/query', entitlement.roleReader, actions.multiGet);

app.del('/query', entitlement.roleWriter, actions.multiDel);

// GET components
app.get('/:aid/components', entitlement.roleReader, actions.getComponent);

// UPDATE components
app.put('/:aid/components', entitlement.roleWriter, actions.updateComponent);
app.patch('/:aid/components', entitlement.roleWriter, actions.updateComponent);



/**
 * Event Results Restful APIs
 */


// Results hook
app.param('rid', /^[0-9a-f]{24}$/);
app.param('page', /^\d+$/);


// Create
app.post('/:aid/results/create', entitlement.roleAdmin, resultsHook.create);
app.put('/:aid/results/create', entitlement.roleAdmin, resultsHook.create);

// Read
app.get('/:aid/results/:rid', entitlement.roleReader, resultsHook.get);

// Delete Results, never provide destroy all
app.del('/:aid/results/:rid', entitlement.roleWriter, resultsHook.destroy);
app.get('/:aid/results/:rid/delete', entitlement.roleWriter, resultsHook.destroy);

// Update Results
app.put('/:aid/results/:rid', entitlement.roleWriter, resultsHook.update);
app.patch('/:aid/results/:rid', entitlement.roleWriter, resultsHook.update);

// Count
app.get('/:aid/results/count', entitlement.roleReader, resultsHook.count);
app.get('/:aid/results/:type/count', entitlement.roleReader, resultsHook.count);


// PAGE
app.get('/:aid/results/page/:page', entitlement.roleReader, resultsHook.paginate);
app.post('/:aid/results/page/:page', entitlement.roleReader, resultsHook.paginate);

app.get('/:aid/results/:type/page/:page', entitlement.roleReader, resultsHook.paginate);
app.post('/:aid/results/:type/page/:page', entitlement.roleReader, resultsHook.paginate);

// MULTI OPTS
app.get('/:aid/results/query', entitlement.roleReader, resultsHook.multiGet);
app.post('/:aid/results/query', entitlement.roleReader, resultsHook.multiGet);
app.del('/:aid/results/query', entitlement.roleWriter, resultsHook.multiDel);

app.get('/:aid/results/export/xlsx/:type', entitlement.roleReader, resultsHook.exportXlsx);

module.exports = app;