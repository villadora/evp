var _ = require('underscore'),
    app = require('express')(),
    params = require('express-params'),
    validator = require('obj-validator'),
    Checker = validator.Checker,
    check = validator.check,
    sanitize = validator.sanitize,
    entitlement = require('entitlement'),
    moment = require('moment'),
    Roles = entitlement.Roles,
    persist = require('persist'),
    eventValidator = require('common').eventValidator,
    log = require('common').logger.createLogger('admin:event-draft');


// add params support
params.extend(app);

app.param('did', /^[0-9a-f]{24}$/);


var steps = ['create', 'draw', 'vote', 'comment'];

//==========================
// register routes`
//==========================
var actions = {
    // PUT POST
    save: function(req, res) {
        if (_.isArray(req.body))
            return res.status(400).send(res.__('The property is not valid: [%s] %s', 'event', req.body.toString()));


        // create/activity
        var id = sanitize(req.body._id).xss(),
            eventObj = eventValidator(req.body.eventObj);


        eventObj.create = {
            createdBy: (req.user ? req.user.uid : 'anonymouse'),
            createdOn: new Date()
        };


        var currentStep = sanitize(req.body.currentStep).xss(),
            nextSteps = req.body.nextSteps,
            prevSteps = req.body.prevSteps;

        if (steps.indexOf(currentStep) == -1)
            return res.status(400).send(res.__('Wrong steps is given: %s', currentStep));

        var errorStep;
        nextSteps = nextSteps.map(function(step) {
            step = sanitize(step).xss();
            if (steps.indexOf(step) == -1)
                errorStep = step;
            return step;
        });

        prevSteps = prevSteps.map(function(step) {
            step = sanitize(step).xss();
            if (steps.indexOf(step) == -1)
                errorStep = step;
            return step;
        });


        if (errorStep) return res.status(400).send(res.__('Wrong steps is given: %s', errorStep));


        // save to db
        var db = persist.create();

        if (!id) {
            // get events seq id
            db.drafts.create({
                eventObj: eventObj,
                currentStep: currentStep,
                nextSteps: nextSteps,
                prevSteps: prevSteps
            }, function(err, obj) {
                if (err) return res.status(500).send(res.__(err.message));
                res.send(obj);
            });
        } else {
            db.drafts.get(id, function(err, data) {
                // not found this event
                if (!data)
                    return res.status(404).send(res.__('The event %s does not exist', id));

                // Check whetehr use can change this event
                if (req.user && req.user.role < Roles.ADMIN) {
                    if (data.eventObj.create.createdBy !== "anonymouse" && data.eventObj.create.createdBy != req.user.uid) {
                        return res.status(401).send(res.__("You have no permission to perform this action"));
                    }
                }

                db.drafts.modify(id, {
                    eventObj: eventObj,
                    currentStep: currentStep,
                    nextSteps: nextSteps,
                    prevSteps: prevSteps
                }, function(err, updated) {
                    if (err) return res.status(500).send(res.__(err.message));
                    res.send(updated);
                });
            });
        }
    },
    // DELETE GET
    remove: function(req, res) {
        var id = sanitize(req.params.did).xss(),
            db = persist.create();

        db.drafts.query({
            _id: id
        }, function(err, events) {
            // not found this event
            if (events.length === 0)
                return res.status(404).send(res.__('The event %s does not exist', id));

            var data = events[0].eventObj;

            // CHeck whetehr use can change this event
            if (req.user && req.user.role < Roles.ADMIN) {
                if (data.create.createdBy !== "anonymouse" && data.create.createdBy != req.user.uid) {
                    return res.status(401).send(res.__("You have no permission to perform this action"));
                }
            }

            db.drafts.destroy({
                _id: id
            }, function(err) {
                if (err) return res.status(500).send(res.__(err.message));
                res.send({
                    id: id
                });
            });
        });
    },
    get: function(req, res) {
        var id = sanitize(req.params.did).xss(),
            db = persist.create();

        db.drafts.get(id, function(err, item) {
            if (err) return res.status(500).send(res.__(err.message));

            if (!item)
                return res.status(404).send(res.__('The event %s does not exist', id));

            if (item.eventObj.startTime)
                item.eventObj.startTime = moment(item.eventObj.startTime).format('YYYY-MM-DD HH:mm:ss');
            if (item.eventObj.endTime)
                item.eventObj.endTime = moment(item.eventObj.endTime).format('YYYY-MM-DD HH:mm:ss');

            res.send(item);
        });
    },
    list: function(req, res) {
        var user = req.user && req.user.uid,
            db = persist.create();
        if (user) {
            db.drafts.query({
                'eventObj.create.createBy': user
            }, function(err, drafts) {
                if (err) return res.status(500).send(res.__(err.message));

                drafts.forEach(function(item) {
                    if (item.eventObj.startTime)
                        item.eventObj.startTime = moment(item.eventObj.startTime).format('YYYY-MM-DD HH:mm:ss');
                    if (item.eventObj.endTime)
                        item.eventObj.endTime = moment(item.eventObj.endTime).format('YYYY-MM-DD HH:mm:ss');
                });
                res.send(drafts);
            });
        } else {
            res.send([]);
        }
    }
};


/**
 * Event Restful APIs
 */

// CREATE
app.post('/', entitlement.roleWriter, actions.save);
app.put('/', entitlement.roleWriter, actions.save);
app.patch('/', entitlement.roleWriter, actions.save);

// READ
app.get('/:did', entitlement.roleReader, actions.get);

// DELETE
app.del('/:did', entitlement.roleWriter, actions.remove);
app.get('/:did/delete', entitlement.roleWriter, actions.remove);

app.get('/list', entitlement.roleReader, actions.list);


module.exports = app;