"use strict";

var _ = require('underscore'),
    moment = require('moment'),
    app = require('express')(),
    validator = require('obj-validator'),
    Checker = validator.Checker,
    check = validator.check,
    sanitize = validator.sanitize,
    persist = require('persist'),
    entitlement = require('entitlement'),
    Roles = entitlement.Roles,
    log = require('common').logger.createLogger('admin:ajaxs:admin');



app.getAndPost = function() {
    app.get.apply(app, arguments);
    return app.post.apply(app, arguments);
};

app.post('/users/new', function(req, res) {
    var email = sanitize(req.param('email')).xss(),
        role = sanitize(req.param('role')).toInt(),
        db = persist.create();

    if (role >= Roles.ROOT)
        return res.status(400).send(res.__('Can not set ROOT role via http api'));

    if (!/@dianping\.com$/.test(email))
        return res.status(400).send(res.__('Email is invalid format'));

    try {
        check(email, 'Email is invalid format').isEmail();
    } catch (e) {
        return res.status(400).send(res.__(e.message));
    }

    db.users.query({
        email: email
    }, function(err, users) {
        if (err) return res.status(500).send(res.__(err.message));

        if (users.length > 0)
            return res.status(400).send(res.__('User already exists: %s', email));

        db.users.addOrUpdate({
            email: email,
            role: role
        }, function(err, newobj) {
            if (err) return res.status(500).send(res.__(err.message));

            res.send(newobj);
        });
    });
});

app.getAndPost('/users/changeRole/:id', function(req, res) {
    var userId = sanitize(req.params.id).xss(),
        role = sanitize(req.param('role')).toInt(),
        db = persist.create();

    if (role >= Roles.ROOT)
        return res.status(400).send(res.__('Can not set ROOT role via http api'));

    if (!app.get('noauth') && req.user._id.toString() == userId)
        return res.status(400).send(res.__('Can not change own roles'));

    db.users.get(userId, function(err, user) {
        if (err) return res.status(500).send(res.__(err.message));

        if (user.role == Roles.ROOT) {
            return res.status(403).send(res.__('Can not change the user\'s privilege'));
        }

        db.users.changeRole(userId, role, function(err, updated) {
            if (err) return res.status(500).send(res.__(err.message));

            res.send(updated);
        });
    });
});



app.getAndPost('/users/:page', function(req, res) {
    var page = sanitize(req.params.page).toInt(),
        limit = sanitize(req.param('limit')).toInt() || 10,
        username = sanitize(req.param('username')).xss(),
        email = sanitize(req.param('email')).xss(),
        query = {},
        db = persist.create();

    if (username)
        query.name = {
            $regex: ".*" + username + ".*"
        };

    if (email)
        query.email = {
            $regex: ".*" + email + ".*"
        };

    db.users.numOf(query, function(err, num) {
        if (err) return res.status(500).send(res.__(err.message));
        db.users.query(query, page - 1, limit, function(err, results) {
            if (err) return res.status(500).send(res.__(err.message));

            res.send({
                total: Math.ceil(num / limit),
                list: results.map(function(user) {
                    user.roleName = res.__(entitlement.roleDesc(user.role));
                    return _.omit(user, 'uid');
                })
            });
        });
    });
});



var userDelHandler = function(req, res) {
    var userId = sanitize(req.params.id).xss(),
        db = persist.create();

    db.users.get(userId, function(err, user) {
        if (err) return res.status(500).send(res.__(err.message));

        if (user.role >= Roles.ROOT) {
            return res.status(403).send(res.__('Can not delete the user with this privilege'));
        }

        if (!app.get('noauth') && req.user._id.toString() == userId)
            return res.status(400).send(res.__('Can not delete your own profile'));

        db.users.destroy(userId, function(err, num) {
            if (err) return res.status(500).send(res.__(err.message));
            res.send({
                deleted: num
            });
        });
    });
};

app.del('/users/:id', userDelHandler);
app.get('/users/del/:id', userDelHandler);


module.exports = app;