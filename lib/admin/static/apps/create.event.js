var app = angular.module("createEvent", ['ui.bootstrap', 'utils', 'events.service']);


app.controller('CreateEventCtrl', function($scope, $modal, $timeout, $log, draftService, eventService) {
    $scope.alerts = [];

    $scope.allSteps = ['create', 'draw', 'vote', 'comment'];
    $scope.prevSteps = [];
    $scope.nextSteps = [];

    $scope.currentStep = $scope.allSteps[0];

    var ajaxError = function(data) {
        $scope.alerts.push({
            msg: data
        });
    };

    $scope.closeAlert = function(idx) {
        $scope.alerts.splice(idx, 1);
    };

    $scope.event = {
        types: {}
    };


    var deleteConfirm = function(ok, cancel) {
        var modalInstance = $modal.open({
            templateUrl: 'confirmModal.html',
            controller: "confirmModalCtrl"
        });

        modalInstance.result.then(function() {
            ok && ok();
        }, function() {
            cancel && cancel();
        });
    };


    $scope.moveForward = function() {
        $scope.nextSteps.unshift($scope.currentStep);
        $scope.currentStep = $scope.prevSteps.pop();
    };

    $scope.createConfig = function() {
        if (!$scope.createForm.$valid) {
            return;
        }

        var types = [],
            allTypes = ['draw', 'comment', 'vote'];
        for (var i = 0; i < allTypes.length; ++i) {
            var t = allTypes[i];
            if ($scope.event.types[t])
                types.push(t);
        }

        $scope.nextSteps = types.concat();

        if (types.length === 0) {
            $scope.createForm.types = $scope.createForm.types || {};
            $scope.createForm.types.empty = true;
        } else {
            // go next page
            $scope.prevSteps.push($scope.currentStep);
            $scope.currentStep = $scope.nextSteps.shift();
        }

    };



    // ============================
    //  Draw Config
    // ============================
    $scope.draw = {
        prizes: [],
        limitTypes: [{
            value: '',
            text: '不限制'
        }, {
            value: 'byUser',
            text: '用户ID限制'
        }, {
            value: 'byIP',
            text: '用户IP限制'
        }],
        limitPeriods: [{
            value: 'event',
            text: '整期限制'
        }, {
            value: 'day',
            text: '每日限制'
        }],
        limitNumber: 0,
        limitWins: 0,
        selected: {},
        select: function(index) {
            var selected = $scope.draw.selected;
            if (selected[index])
                delete selected[index];
            else
                selected[index] = true;
        },
        selectAll: function() {
            var selected = $scope.draw.selected,
                value = !$scope.draw.isAllSelected();
            for (var i = 0; i < $scope.draw.prizes.length; ++i)
                selected[i] = value;
        },
        isAllSelected: function() {
            var selected = $scope.draw.selected;
            for (var i = 0; i < $scope.draw.prizes.length; ++i)
                if (!selected[i])
                    return false;
            return true;
        },
        isSelected: function(index) {
            return $scope.draw.selected[index];
        },
        edit: function(prize, index) {
            var modalInstance = $modal.open({
                templateUrl: 'newDrawPrize.html',
                controller: "newDrawPrizeCtrl",
                resolve: {
                    prize: function() {
                        return prize;
                    }
                }
            });

            modalInstance.result.then(function(prize) {
                $scope.draw.prizes.splice(index, 1, prize);
            }, function() {

            });
        },
        new: function() {
            var modalInstance = $modal.open({
                templateUrl: 'newDrawPrize.html',
                controller: "newDrawPrizeCtrl",
                resolve: {
                    prize: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(prize) {
                $scope.draw.prizes.push(prize);
            }, function() {

            });
        },
        deleteSelected: function() {
            deleteConfirm(function() {
                for (var idx in $scope.draw.selected) {
                    if ($scope.draw.selected[idx])
                        $scope.draw.prizes.splice(idx, 1);
                }
            });
        },
        delete: function(p, index) {
            deleteConfirm(function() {
                $scope.draw.prizes.splice(index, 1);
            });
        }
    };

    $scope.draw.limitType = $scope.draw.limitTypes[2];
    $scope.draw.limitPeriod = $scope.draw.limitPeriods[0];

    $scope.drawConfig = function() {
        if (!$scope.drawForm.$valid) {
            return;
        }

        $scope.draw.$dirty = true;

        if ($scope.draw.prizes.length === 0)
            return;

        if ($scope.nextSteps.length) {
            // go next page
            $scope.prevSteps.push($scope.currentStep);
            $scope.currentStep = $scope.nextSteps.shift();
            $scope.alerts = [];
        } else
            $scope.finalSubmit();
    };



    // ============================
    //  Vote Config
    // ============================
    $scope.vote = {
        candidates: [],
        limits: [{
            value: '',
            text: '不限制'
        }, {
            value: 'event',
            text: '整期限制'
        }, {
            value: 'day',
            text: '每日限制'
        }],
        ipLimitNumber: 0,
        idLimitNumber: 0,
        selected: {},
        select: function(index) {
            var selected = $scope.vote.selected;
            if (selected[index])
                delete selected[index];
            else
                selected[index] = true;
        },
        selectAll: function() {
            var selected = $scope.vote.selected,
                value = !$scope.vote.isAllSelected();
            for (var i = 0; i < $scope.vote.candidates.length; ++i)
                selected[i] = value;
        },
        isAllSelected: function() {
            var selected = $scope.vote.selected;
            for (var i = 0; i < $scope.vote.candidates.length; ++i)
                if (!selected[i])
                    return false;
            return true;
        },
        isSelected: function(index) {
            return $scope.vote.selected[index];
        },
        edit: function(cand, index) {
            var modalInstance = $modal.open({
                templateUrl: 'newVoteCandidate.html',
                controller: "newVoteCandidateCtrl",
                resolve: {
                    cand: function() {
                        return cand;
                    }
                }
            });

            modalInstance.result.then(function(cand) {
                $scope.vote.candidates.splice(index, 1, cand);
            }, function() {

            });
        },
        new: function() {
            var modalInstance = $modal.open({
                templateUrl: 'newVoteCandidate.html',
                controller: "newVoteCandidateCtrl",
                resolve: {
                    cand: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(cand) {
                $scope.vote.candidates.push(cand);
            }, function() {

            });
        },
        deleteSelected: function() {
            deleteConfirm(function() {
                for (var idx in $scope.vote.selected) {
                    if ($scope.vote.selected[idx])
                        $scope.vote.candidates.splice(idx, 1);
                }
            });
        },
        delete: function(p, index) {
            deleteConfirm(function() {
                $scope.vote.candidates.splice(index, 1);
            });
        }
    };

    $scope.vote.ipLimitType = $scope.vote.limits[0];
    $scope.vote.idLimitType = $scope.vote.limits[0];

    $scope.voteConfig = function() {
        if (!$scope.voteForm.$valid)
            return;

        $scope.vote.$dirty = true;
        if ($scope.vote.candidates.length === 0)
            return;

        if ($scope.nextSteps.length) {
            // go next page
            $scope.prevSteps.push($scope.currentStep);
            $scope.currentStep = $scope.nextSteps.shift();
            $scope.alerts = [];
        } else
            $scope.finalSubmit();

    };



    // ============================
    // Comment Config
    // ============================
    $scope.comment = {
        limitNumber: 0,
        limitTypes: [{
            value: '',
            text: '不限制'
        }, {
            value: 'byUser',
            text: '用户ID限制'
        }, {
            value: 'byIP',
            text: '用户IP限制'
        }],
        limitPeriods: [{
            value: 'event',
            text: '整期限制'
        }, {
            value: 'day',
            text: '每日限制'
        }],
        estimatedParticipants: 0
    };

    $scope.comment.limitType = $scope.comment.limitTypes[0];
    $scope.comment.limitPeriod = $scope.comment.limitPeriods[0];

    $scope.commentConfig = function() {
        if (!$scope.commentForm.$valid)
            return;


        $scope.comment.$dirty = true;

        if ($scope.nextSteps.length) {
            // go next page
            $scope.prevSteps.push($scope.currentStep);
            $scope.currentStep = $scope.nextSteps.shift();
            $scope.alerts = [];
        } else
            $scope.finalSubmit();
    };



    $scope.finalSubmit = function() {
        var eventObj = makeEvent();
        if ($scope.draftId)
            eventObj._id = $scope.draftId;

        eventService.createEvent(eventObj).success(function(data) {
            window.location = "/activity/admin/event/" + data._id;
        }).error(ajaxError);
    };

    $scope.saveDraft = function() {
        if (!$scope.event.name) {
            $scope.alerts.push({
                msg: '请先输入活动名称'
            });
            return;
        }

        var eventObj = makeEvent(),
            draft = {
                currentStep: $scope.currentStep,
                nextSteps: $scope.nextSteps,
                prevSteps: $scope.prevSteps,
                eventObj: eventObj
            };

        if ($scope.draftId)
            draft._id = $scope.draftId;

        draftService.saveDraft(draft).success(function(data) {
            $scope.draftId = data._id;
            $scope.alerts.push({
                type: 'info',
                msg: '保存草稿成功'
            });
        }).error(ajaxError);
    };

    $scope.init = function(draftId, editMode) {
        if (draftId) {
            draftService.getDraft(draftId).success(function(data) {
                loadDraft(data);
            }).error(ajaxError);
        }
    };


    function loadDraft(draft) {
        $scope.draftId = draft._id;
        $scope.nextSteps = draft.nextSteps;
        $scope.prevSteps = draft.prevSteps;
        $scope.currentStep = draft.currentStep;

        var eventObj = draft.eventObj;
        $scope.event.name = eventObj.name;
        $scope.event.cityId = eventObj.cityId;
        $scope.event.owner = eventObj.owner;
        $scope.event.startTime = eventObj.startTime;
        $scope.event.endTime = eventObj.endTime;
        $scope.event.message = eventObj.message;
        $scope.event.types = {};

        if (eventObj.components.draw) {
            var draw = eventObj.components.draw;
            $scope.event.types.draw = true;

            if (draw.limitType) {
                angular.forEach($scope.draw.limitTypes, function(type) {
                    if (draw.limitType == type.value)
                        $scope.draw.limitType = type;
                });
            } else {
                // unlimit
                $scope.draw.limitType = $scope.draw.limitTypes[0];
            }


            if (draw.limitPeriod) {
                angular.forEach($scope.draw.limitPeriods, function(p) {
                    if (draw.limitPeriod == p.value)
                        $scope.draw.limitPeriod = p;
                });
            } else {
                // unlimit
                $scope.draw.limitPeriod = $scope.draw.limitPeriods[0];
            }
            $scope.draw.limitNumber = draw.limitNumber || 0;
            $scope.draw.limitWins = draw.limitWins || 0;
            $scope.draw.prizes = draw.prizes;

            $scope.draw.estimatedParticipants = draw.estimatedParticipants;
        }

        if (eventObj.components.comment) {
            var comment = eventObj.components.comment;
            $scope.event.types.comment = true;

            $scope.comment.estimatedParticipants = comment.estimatedParticipants || 0;

            angular.forEach($scope.comment.limitTypes, function(type) {
                if (comment.limitType == type.value)
                    $scope.comment.limitType = type;
            });

            if (!$scope.comment.limitType) {
                $scope.comment.limitType = $scope.comment.limitTypes[0];
            }

            angular.forEach($scope.comment.limitPeriods, function(p) {
                if (comment.limitPeriod == p.value)
                    $scope.comment.limitPeriod = p;
            });

            $scope.comment.limitNumber = comment.limitNumber;
        }

        if (eventObj.components.vote) {
            var vote = eventObj.components.vote;
            $scope.event.types.vote = true;

            $scope.vote.candidates = vote.candidates;


            angular.forEach($scope.vote.limits, function(type) {
                if (vote.ipLimitType == type.value)
                    $scope.vote.ipLimitType = type;
                if (vote.idLimitType == type.value)
                    $scope.vote.idLimitType = type;
            });

            if (!$scope.vote.ipLimitType) {
                $scope.vote.ipLimitType = $scope.vote.limits[0];
            }
            if (!$scope.vote.idLimitType) {
                $scope.vote.idLimitType = $scope.vote.limits[0];
            }

            $scope.vote.ipLimitNumber = vote.ipLimitNumber;
            $scope.vote.idLimitNumber = vote.idLimitNumber;
        }


    }

    function makeEvent() {

        var eventObj = {
            name: $scope.event.name,
            cityId: $scope.event.cityId,
            owner: $scope.event.owner,
            message: $scope.event.message,
            components: {}
        };

        $scope.event.startTime && (eventObj.startTime = $scope.event.startTime);
        $scope.event.endTime && (eventObj.endTime = $scope.event.endTime);

        if ($scope.event.types.draw) {
            // parseDrawComponent
            var draw = eventObj.components.draw = {};

            if ($scope.draw.limitType.value) {
                draw.limitType = $scope.draw.limitType.value;
                draw.limitNumber = $scope.draw.limitNumber;
                draw.limitWins = $scope.draw.limitWins;
                draw.limitPeriod = $scope.draw.limitPeriod.value;
            }

            draw.prizes = $scope.draw.prizes;

            draw.estimatedParticipants = $scope.draw.estimatedParticipants;
        }

        if ($scope.event.types.comment) {
            var comment = eventObj.components.comment = {};
            comment.estimatedParticipants = $scope.comment.estimatedParticipants || 0;

            if ($scope.comment.limitType.value)
                comment.limitType = $scope.comment.limitType.value;

            comment.limitPeriod = $scope.comment.limitPeriod.value;
            comment.limitNumber = $scope.comment.limitNumber;
        }

        if ($scope.event.types.vote) {
            var vote = eventObj.components.vote = {};

            vote.candidates = $scope.vote.candidates;

            if ($scope.vote.ipLimitType.value) {
                vote.ipLimitType = $scope.vote.ipLimitType.value;
                vote.ipLimitNumber = $scope.vote.ipLimitNumber;
            }

            if ($scope.vote.idLimitType.value) {
                vote.idLimitType = $scope.vote.idLimitType.value;
                vote.idLimitNumber = $scope.vote.idLimitNumber;
            }
        }

        return eventObj;
    }
});



app.controller('newDrawPrizeCtrl', function($scope, $modalInstance, prize) {

    $scope.hardRate = 1;
    $scope.total = 0;

    if (prize) {
        // read prize
        $scope.name = prize.name;
        $scope.level = prize.level;
        $scope.total = prize.total;
        $scope.hardRate = prize.hardRate;
        $scope.message = prize.message;
        $scope.types = {};

        for (var i = 1; i < 3; ++i) {
            if (prize.type & i) {
                $scope.types[i] = true;
            }
        }
    }

    $scope.ok = function() {
        // checkbox but in bootstrap, which override the event make ng-model not work
        var type = 0;

        for (var t in $scope.types) {
            if ($scope.types[t])
                type += parseInt(t, 10);
        }

        var prize = {
            name: $scope.name,
            level: $scope.level,
            total: $scope.total,
            hardRate: $scope.hardRate,
            type: '' + type,
            message: $scope.message
        };
        $modalInstance.close(prize);
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };
});



app.controller('newVoteCandidateCtrl', function($scope, $modalInstance, cand) {
    if (cand) {
        // read prize
        $scope.name = cand.name;
        $scope.desc = cand.desc;
        $scope.votes = cand.votes;
    }

    $scope.ok = function() {
        // checkbox but in bootstrap, which override the event make ng-model not work
        var cand = {
            name: $scope.name,
            votes: $scope.votes || 0,
            desc: $scope.desc
        };

        $modalInstance.close(cand);
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };
});

app.controller('confirmModalCtrl', function($scope, $modalInstance) {
    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});