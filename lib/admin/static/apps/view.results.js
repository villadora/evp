var app = angular.module("viewResults", ['ui.bootstrap', 'utils', 'events.service']);

app.controller('drawResultsCtrl', function($scope, $modal, $timeout, eventResultsService) {
    $scope.alerts = [];
    $scope.userId = '';
    $scope.prizeId = '';
    $scope.records = [];

    var ajaxError = function(data) {
        $scope.alerts.push({
            msg: data
        });
    };

    $scope.init = function(eventId, prizeId) {
        $scope.eventId = eventId;
        if (prizeId !== undefined)
            $scope.prizeId = prizeId;
        $scope.setCurrentPage(1);
    };

    $scope.totalPages = 0;
    $scope.currentPage = 0;

    // init first page
    $scope.setCurrentPage = function(pn) {
        var eventId = $scope.eventId,
            query = {};

        if ($scope.userId)
            query.userId = $scope.userId;

        if ($scope.prizeId)
            query['prize.id'] = $scope.prizeId;

        eventResultsService.queryResults(pn, eventId, 'draw', query).success(function(data) {
            $scope.records = data.list;
            $scope.currentPage = pn;
            $scope.totalPage = data.total;
        }).error(ajaxError);
    };


    $scope.deleteRec = function(idx) {
        var record = $scope.records[idx];
        if (record) {
            var modalInstance = $modal.open({
                templateUrl: 'confirmModal.html',
                controller: "confirmModalCtrl"
            });

            modalInstance.result.then(function() {
                eventResultsService.deleteResult($scope.eventId, record._id).success(function() {
                    if ($scope.records.length > 1)
                        $scope.setCurrentPage($scope.currentPage);
                    else
                        $scope.setCurrentPage($scope.currentPage > 1 ? ($scope.currentPage - 1) : 1);
                }).error(ajaxError);
            }, function() {});
        }
    };

});

app.controller('voteResultsCtrl', function($scope, $modal, $timeout, eventResultsService) {

    $scope.alerts = [];
    $scope.userId = '';
    $scope.candId = '';
    $scope.records = [];

    var ajaxError = function(data) {
        $scope.alerts.push({
            msg: data
        });
    };

    $scope.init = function(eventId, candId) {
        $scope.eventId = eventId;
        if (candId !== undefined)
            $scope.candId = candId;
        $scope.setCurrentPage(1);
    };

    $scope.totalPages = 0;
    $scope.currentPage = 0;

    // init first page
    $scope.setCurrentPage = function(pn) {
        var eventId = $scope.eventId,
            query = {};

        if ($scope.userId)
            query.userId = $scope.userId;

        if ($scope.candId)
            query.vid = $scope.candId;

        eventResultsService.queryResults(pn, eventId, 'vote', query).success(function(data) {
            $scope.records = data.list;
            $scope.currentPage = pn;
            $scope.totalPage = data.total;
        }).error(ajaxError);
    };

    $scope.deleteRec = function(idx) {
        var record = $scope.records[idx];
        if (record) {
            var modalInstance = $modal.open({
                templateUrl: 'confirmModal.html',
                controller: "confirmModalCtrl"
            });

            modalInstance.result.then(function() {
                eventResultsService.deleteResult($scope.eventId, record._id).success(function() {
                    if ($scope.records.length > 1)
                        $scope.setCurrentPage($scope.currentPage);
                    else {
                        $scope.setCurrentPage($scope.currentPage > 1 ? ($scope.currentPage - 1) : 1);
                    }
                }).error(ajaxError);
            }, function() {});
        }
    };

});



app.controller('commentResultsCtrl', function($scope, $modal, $timeout, eventResultsService) {

    $scope.alerts = [];
    $scope.userId = '';
    $scope.content = '';
    $scope.records = [];

    var ajaxError = function(data) {
        $scope.alerts.push({
            msg: data
        });
    };

    $scope.init = function(eventId) {
        $scope.eventId = eventId;
        $scope.setCurrentPage(1);
    };

    $scope.totalPages = 0;
    $scope.currentPage = 0;

    // init first page
    $scope.setCurrentPage = function(pn) {
        var eventId = $scope.eventId,
            query = {};

        if ($scope.userId)
            query.userId = $scope.userId;

        if ($scope.content)
            query['comment.content'] = ".*" + $scope.content + ".*";

        eventResultsService.queryResults(pn, eventId, 'comment', query).success(function(data) {
            $scope.records = data.list;
            console.log(pn);
            $scope.currentPage = pn;
            $scope.totalPage = data.total;
        }).error(ajaxError);
    };

    $scope.deleteRec = function(idx) {
        var record = $scope.records[idx];
        if (record) {
            var modalInstance = $modal.open({
                templateUrl: 'confirmModal.html',
                controller: "confirmModalCtrl"
            });

            modalInstance.result.then(function() {
                eventResultsService.deleteResult($scope.eventId, record._id).success(function() {
                    if ($scope.records.length > 1)
                        $scope.setCurrentPage($scope.currentPage);
                    else
                        $scope.setCurrentPage($scope.currentPage > 1 ? $scope.currentPage - 1 : 1);
                }).error(ajaxError);
            }, function() {});
        }
    };

});

app.controller('confirmModalCtrl', function($scope, $modalInstance) {
    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});