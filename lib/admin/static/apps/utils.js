var utils = angular.module('utils', ['utils.template', 'utils.pager', 'utils.datepicker']);

utils.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});


angular.module('utils.datepicker', []).run(function() {
    $("<link>").attr({
        rel: "stylesheet",
        type: "text/css",
        href: "/activity/admin/static/bootstrap/css/datetimepicker.css"
    }).appendTo("head");

    $.fn.datetimepicker.dates['zh-CN'] = {
        days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
        daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
        daysMin: ["日", "一", "二", "三", "四", "五", "六", "日"],
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        today: "今日",
        suffix: [],
        meridiem: []
    };
    var date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);

    $.fn.datetimepicker.defaults.language = 'zh-CN';
    $.fn.datetimepicker.defaults.pickerPosition = 'bottom-left';
    $.fn.datetimepicker.defaults.minView = 'hour';
    $.fn.datetimepicker.defaults.initialDate = date;
    $.fn.datetimepicker.defaults.autoclose = true;
    $.fn.datetimepicker.defaults.forceParse = false;

    // $(".date-picker").datetimepicker();
}).directive('datetimePicker', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            $(element).datetimepicker().on('changeDate', function(e) {
                $scope.$apply(function() {
                    if (!ngModel) return;
                    var date = e.date;
                    ngModel.$setViewValue(moment.utc(e.date).format('YYYY-MM-DD HH:mm:ss'));
                });
            });

            if (ngModel) {
                ngModel.$render = function() {
                    if (ngModel.$viewValue) {
                        var date = new Date(ngModel.$viewValue);
                        if (date && !isNaN(date.getTime()))
                            $(element).datetimepicker('setDate', new Date(ngModel.$viewValue));
                    }
                };
            }
        }
    };
});

angular.module('utils.template', ['utils/template/pager.html']);

angular.module('utils.pager', [])
    .controller('PagerCtrl', ['$scope', '$attrs',
        function($scope, $attrs, pagerConfig) {
            var self = this,
                maxSize = $attrs.maxSize || 5;

            // self.render = function() {
            //     this.page = page;
            //     if (this.page > 0 && this.page <= $scope.totalPages) {
            //         // $scope.pages = this.getPages(this.page, $scope.totalPages);
            //         $scope.pages = ["start", 1, 2, 3, 4, 5, 6, "end"];
            //     }
            // };

            self.getPages = function(page, totalPages) {
                var pages = [],
                    startPage = Math.max(page - Math.floor(maxSize / 2), 1),
                    endPage = Math.min(page + Math.floor(maxSize / 2), totalPages);

                for (var i = startPage; i <= endPage; ++i)
                    pages.push({
                        disable: (page == i),
                        pn: i
                    });

                if (startPage > 1) {
                    if (startPage > 2)
                        pages.unshift({
                            disable: true,
                            pn: '...'
                        });

                    pages.unshift({
                        disable: false,
                        pn: 1
                    });
                }

                if (endPage < totalPages) {
                    if (endPage < totalPages - 1)
                        pages.push({
                            disable: true,
                            pn: '...'
                        });

                    pages.push({
                        disable: false,
                        pn: totalPages
                    });
                }
                return pages;
            };

            $scope.setPage = function(page) {
                $scope.page = page;
                $scope.pages = self.getPages(page, $scope.totalPages);
                $scope.onSelectPage({
                    page: page
                });
            };


            $scope.$watch('page', function(page) {
                $scope.page = page;
                $scope.pages = self.getPages(page, $scope.totalPages);
            });

            $scope.$watch('totalPages', function(value) {
                if (self.page > value) {
                    $scope.page = value;
                }

                $scope.pages = self.getPages($scope.page, value);
            });
        }
    ]).directive('upager', ['$parse',
        function($parse) {
            return {
                restrict: 'EA',
                scope: {
                    page: '=page',
                    totalPages: '=',
                    onSelectPage: "&"
                },
                controller: 'PagerCtrl',
                templateUrl: 'utils/template/pager.html',
                replace: true
            };
        }
    ]);


angular.module("utils/template/datepicker.html", []).run(['$templateCache'], function($templateCache) {

});

angular.module("utils/template/pager.html", []).run(["$templateCache",
    function($templateCache) {
        $templateCache.put("utils/template/pager.html", "<div class=\"pagination pagination-centered\"><ul>" +
            "<li><a href=\"#\" ng-click=\"setPage(1)\">&laquo;</a></li>" +
            "<li ng-repeat=\"p in pages\" class=\"{{p.disable?'disabled':''}}\">" +
            "<a href=\"#\" ng-click=\"p.disable || setPage(p.pn)\">{{p.pn}}</a></li>" +
            "<li><a href=\"#\"ng-click=\"setPage(totalPages)\">&raquo;</a></li></ul></div>");
    }
]);