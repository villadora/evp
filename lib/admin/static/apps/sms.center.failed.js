var app = angular.module('smsCenterFailed', ['ui.bootstrap', 'sms.center', 'utils']);

app.controller('confirmModalCtrl', function($scope, $modalInstance) {
    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('RecordsCtrl', function($scope, $timeout, $modal, $log, recordsService) {
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };


    $scope.page = 0;
    $scope.totalPages = 0;

    // init first page
    $scope.setCurrentPage = function(pn) {
        var eventId, mobileNo, content;
        if ($scope.form) {
            eventId = $scope.form.eventId;
            mobileNo = $scope.form.mobileNo;
            content = $scope.form.content;
        }

        recordsService.getFailedRecords(pn, eventId, mobileNo, content).success(function(data) {
            $scope.records = data.list.map(function(rec) {
                if (rec._id.length > 4) {
                    rec.id = rec._id.substring(0, 4) + '...';
                } else rec.id = rec._id;
                return rec;
            });
            $scope.currentPage = pn;
            $scope.totalPage = data.total;
        });
    };

    $scope.setCurrentPage(1);

    $scope.resend = function(rec) {
        recordsService.resend(rec._id).success(function(data) {
            // refresh
            if (data.smsCode == 200) {
                $scope.setCurrentPage($scope.currentPage);
            } else {
                $scope.alerts.push({
                    msg: "重发失败: " + data.message
                });
            }
        }).error(function(data) {
            $scope.alerts.push({
                msg: data
            });
        });
    };

    $scope.deleteRec = function(rec, idx) {
        var modalInstance = $modal.open({
            templateUrl: 'confirmModal.html',
            controller: "confirmModalCtrl"
        });

        modalInstance.result.then(function() {
            recordsService.deleteRec(rec._id).success(function(data) {
                $scope.records.splice(idx, 1);
            }).error(function(data) {
                var idx = $scope.alerts.length;
                $scope.alerts.push({
                    msg: data.msg.message
                });
                $timeout(function() {
                    $scope.alerts.splice(idx, 1);
                }, 10000);
            });
        }, function() {
            $log.info('cancel delete at: ' + new Date());
        });
    };
});