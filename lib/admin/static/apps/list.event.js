var app = angular.module('listEvent', ['ui.bootstrap', 'events.service', 'utils']);

app.filter('eventState', function() {
    return function(input) {
        if (input && input.startTime && input.endTime) {
            if (new Date(input.startTime) > new Date()) {
                return '未开始';
            } else if (new Date(input.endTime) < new Date()) {
                return '已结束';
            } else {
                return '在线';
            }
        }

        return '';
    };
});

app.controller('listEventCtrl', function($scope, $modal, eventService) {
    $scope.alerts = [];
    $scope.events = [];
    $scope.currentPage = 0;
    $scope.totalPage = 0;

    var ajaxError = function(data) {
        $scope.alerts.push({
            msg: data
        });
    };


    $scope.setCurrentPage = function(page) {
        if ($scope.queryForm && $scope.queryForm.$invalid)
            return;

        var query = {};

        if ($scope.search) {
            angular.forEach(['active', 'range_start', 'range_end', 'eventId', 'name'], function(k) {
                if ($scope.search.hasOwnProperty(k))
                    query[k] = $scope.search[k];
            });
        }

        eventService.paginate(page, query).success(function(data) {
            $scope.currentPage = page;
            $scope.totalPage = data.total;
            $scope.events = data.list;
        }).error(ajaxError);
    };

    $scope.deleteEvent = function(eventId) {
        var modalInstance = $modal.open({
            templateUrl: 'confirmModal.html',
            controller: "confirmModalCtrl"
        });

        modalInstance.result.then(function() {
            eventService.deleteEvent(eventId).success(function(data) {
                if ($scope.events.length > 1)
                    $scope.setCurrentPage($scope.currentPage);
                else
                    $scope.setCurrentPage($scope.currentPage > 1 ? $scope.currentPage - 1 : 1);
            }).error(ajaxError);
        }, function() {});
    };


    $scope.setCurrentPage(1);
});

app.controller('confirmModalCtrl', function($scope, $modalInstance) {
    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});