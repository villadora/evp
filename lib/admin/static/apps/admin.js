angular.module('admin', ['admin.users']);
angular.module('admin.users', ['admin.users.service']);


angular.module('admin.users.service', []).factory('usersService', function($http) {
    return {
        getUsers: function(pn, username, email) {
            if (pn <= 0) pn = 1;
            var data = {};
            if (username) data.username = username;
            if (email) data.email = email;

            return $http.post('/activity/admin/ajax/admin/users/' + pn, data).success(function(data, status) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        },
        deleteUser: function(id) {
            return $http.delete('/activity/admin/ajax/admin/users/' + id).success(function(data) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        },
        changeRole: function(id, role) {
            return $http.post('/activity/admin/ajax/admin/users/changeRole/' + id, {
                role: role
            }).success(function(data) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        },
        addUser: function(email, role) {
            return $http.post('/activity/admin/ajax/admin/users/new', {
                email: email,
                role: role
            }).success(function(data) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        }
    };
});