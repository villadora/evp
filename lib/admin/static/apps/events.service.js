angular.module('events.service', ['events.draft.service', 'events.event.service']);

angular.module('events.draft.service', []).factory('draftService', function($http) {
    return {
        getDraft: function(did) {
            return $http.get('/activity/admin/ajax/events/draft/' + did).success(function(data, status) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        },
        saveDraft: function(draft) {
            return $http.post('/activity/admin/ajax/events/draft', draft).success(function(data) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        }
    };
});


angular.module('events.event.service', []).factory('eventService', function($http) {
    return {
        paginate: function(pageNo, query) {
            return $http.post('/activity/admin/ajax/events/page/' + (pageNo || 1), query).success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });
        },
        createEvent: function(eventObj) {
            return $http.post('/activity/admin/ajax/events/', eventObj).success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });

        },
        deleteEvent: function(eventId) {
            return $http.delete('/activity/admin/ajax/events/' + eventId).success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });
        },
        getEvent: function(eventId) {
            return $http.get('/activity/admin/ajax/events/' + eventId).success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });
        },
        getEventComponents: function(eventId) {
            return $http.get('/activity/admin/ajax/events/' + eventId + '/components').success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });
        },
        updateEventComponents: function(eventId, components) {
            return $http.put('/activity/admin/ajax/events/' + eventId + '/components', components).success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });
        },
        updateEventBaseInfo: function(eventId, baseInfo) {
            delete baseInfo.components;
            return $http.put('/activity/admin/ajax/events/' + eventId, baseInfo).success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });
        }
    };
}).factory('eventResultsService', function($http) {
    return {
        deleteResult: function(eventId, rid) {
            return $http.delete('/activity/admin/ajax/events/' + eventId + '/results/' + rid).success(function(data) {
                return data;
            }).error(function(data) {
                return data;
            });
        },
        queryResults: function(pn, eventId, type, query) {
            if (pn <= 0) pn = 1;

            return $http.post('/activity/admin/ajax/events/' + eventId + '/results/' + type + '/page/' + pn, query).success(function(data, status) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        }
    };
});