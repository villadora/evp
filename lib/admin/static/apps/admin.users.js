var app = angular.module("adminRoles", ['ui.bootstrap', 'admin', 'utils']);

var roles = {
    15: "管理员",
    7: "高级用户(可发短信)",
    4: "短信通知用户(只可发短信)",
    3: "普通用户",
    2: "只写用户",
    1: "只读用户",
    0: "无权限"
};

app.controller('rolesCtrl', function($scope, $modalInstance, user) {
    $scope.roles = roles;

    $scope.user = user;
    $scope.role = user.role;
    $scope.roleName = $scope.roles[user.role];

    $scope.setRole = function(role) {
        $scope.role = role;
        $scope.roleName = role == 31 ? "管理员" : $scope.roles[role];
    };

    $scope.ok = function() {
        $modalInstance.close($scope.role);
    };
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel");
    };
});

app.controller('confirmModalCtrl', function($scope, $modalInstance, user) {
    $scope.user = user;
    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('addNewUserCtrl', function($scope, $modalInstance) {
    $scope.roles = roles;
    $scope.setRole = function(role) {
        $scope.role = role;
        $scope.roleName = $scope.roles[role];
    };

    $scope.ok = function() {
        $modalInstance.close({
            email: $scope.email + "@dianping.com",
            role: $scope.role
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

    $scope.setRole(2);
});


app.controller('UsersCtrl', function($scope, $modal, $timeout, $log, usersService) {
    $scope.alerts = [];
    $scope.totalPages = 0;
    $scope.currentPage = 0;

    $scope.closeAlert = function(idx) {
        $scope.alerts.splice(idx, 1);
    };

    // init first page
    $scope.setCurrentPage = function(pn) {
        var username, email;
        if ($scope.form) {
            username = $scope.form.username;
            email = $scope.form.email;
        }

        usersService.getUsers(pn, username, email).success(function(data) {
            $scope.users = data.list.map(function(user) {
                if (user._id.length > 4) {
                    user.id = user._id.substring(0, 4) + '...';
                } else user.id = user._id;
                return user;
            });
            $scope.currentPage = pn;
            $scope.totalPage = data.total;
        }).error(function(data) {
            $scope.alerts.push({
                msg: data
            });
        });
    };

    $scope.setCurrentPage(1);

    $scope.addNewUser = function() {
        var modalInstance = $modal.open({
            templateUrl: "addUserModal.html",
            controller: "addNewUserCtrl"
        });

        modalInstance.result.then(function(user) {
            usersService.addUser(user.email, user.role).success(function(data) {
                $scope.setCurrentPage($scope.currentPage);
            }).error(function(data) {
                $scope.alerts.push({
                    msg: data
                });
            });
        }, function() {});
    };

    $scope.changeRole = function(user, idx) {
        var modalInstance = $modal.open({
            templateUrl: 'rolesModal.html',
            controller: 'rolesCtrl',
            resolve: {
                user: function() {
                    return user;
                }
            }
        });


        modalInstance.result.then(function(role) {
            usersService.changeRole(user._id, role).success(function(data) {
                $scope.setCurrentPage($scope.currentPage);
            }).error(function(data) {
                $scope.alerts.push({
                    msg: data
                });
            });
        }, function() {});
    };


    $scope.deleteUser = function(user, idx) {
        var modalInstance = $modal.open({
            templateUrl: 'confirmModal.html',
            controller: "confirmModalCtrl",
            resolve: {
                user: function() {
                    return user;
                }
            }
        });

        modalInstance.result.then(function() {
            usersService.deleteUser(user._id).success(function(data) {
                $scope.users.splice(idx, 1);
            }).error(function(data) {
                var idx = $scope.alerts.length;
                $scope.alerts.push({
                    msg: data
                });
                $timeout(function() {
                    $scope.alerts.splice(idx, 1);
                }, 10000);
            });
        }, function() {
            $log.info('cancel delete at: ' + new Date());
        });
    };
});