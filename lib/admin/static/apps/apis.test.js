var app = angular.module("apiTest", ['ui.bootstrap', 'utils']);

app.controller('ApiTestCtrl', function($scope, $log, $http) {
    $scope.draw = {
        drawAPI: function() {
            $http.get('/activity/ajax/' + $scope.eventId + '/draw').then(function(data) {
                $scope.draw.drawResult = JSON.stringify(data);
            });
        },
        userInfoAPI: function() {
            $http.post('/activity/ajax/' + $scope.eventId + '/draw/userInfo', {
                'mobileNo': '18433323443',
                'recordId': $scope.draw.recordId,
                'userName': 'name',
                'address': 'address 13323'
            }).then(function(data) {
                $scope.draw.userInfoResult = JSON.stringify(data);
            });
        }
    };


    $scope.vote = {
        voteAPI: function() {
            $http.get('/activity/ajax/' + $scope.eventId + '/vote/voteFor/' + $scope.vote.voteId).then(function(data) {
                $scope.vote.voteResult = JSON.stringify(data);
            });
        },
        listAPI: function() {
            $http.get('/activity/ajax/' + $scope.eventId + '/vote/votelist').then(function(data) {
                $scope.vote.listResult = JSON.stringify(data);
            });
        }
    };

    $scope.comment = {
        commentAPI: function() {
            $http.post('/activity/ajax/' + $scope.eventId + '/comment', {
                'content': 'hello comment',
                'pic': 'http://dp.com/pic/thumb.png'
            }).then(function(data) {
                $scope.comment.commentResult = JSON.stringify(data);
            });
        },
        pageAPI: function() {
            $http.get('/activity/ajax/' + $scope.eventId + '/comment/page/1').then(function(data) {
                $scope.comment.pageResult = JSON.stringify(data);
            });
        },
        likeAPI: function() {
            $http.get('/activity/ajax/' + $scope.eventId + '/comment/likes/' + $scope.comment.recordId).then(function(data) {
                $scope.comment.likeResult = JSON.stringify(data);
            });
        }
    };
});