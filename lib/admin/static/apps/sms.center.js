angular.module('sms.center', ['sms.center.service']);

angular.module('sms.center.service', []).factory('recordsService', function($http) {
    return {
        getFailedRecords: function(pn, eventId, mobileNo, content) {
            if (pn <= 0) pn = 1;
            var data = {};
            if (eventId) data.eventId = eventId;
            if (mobileNo) data.mobileNo = mobileNo;
            if (content) data.content = content;

            return $http.post('/activity/admin/ajax/sms/records/' + pn + '/failed', data).success(function(data, status) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        },
        getRecords: function(pn, eventId, mobileNo, content) {
            if (pn <= 0) pn = 1;
            var data = {};
            if (eventId) data.eventId = eventId;
            if (mobileNo) data.mobileNo = mobileNo;
            if (content) data.content = content;

            return $http.post('/activity/admin/ajax/sms/records/' + pn, data).success(function(data, status) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        },
        resend: function(id) {
            return $http.get('/activity/admin/ajax/sms/records/resend/' + id).success(function(data) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        },
        deleteRec: function(id) {
            return $http.delete('/activity/admin/ajax/sms/records/' + id).success(function(data) {
                return data;
            }).error(function(data, status) {
                return data;
            });
        }
    };
});