var app = angular.module("editEvent", ['ui.bootstrap', 'events.service', 'utils']);

function viewEvent(eventId) {
    window.location = '/activity/admin/event/' + eventId;
}


app.controller('editEventCtrl', function($scope, $modal, $timeout, eventService) {
    $scope.alerts = [];
    var ajaxError = function(data) {
        $scope.alerts.push({
            msg: data
        });
    };

    $scope.closeAlert = function(idx) {
        $scope.alerts.splice(idx, 1);
    };


    var deleteConfirm = function(ok, cancel) {
        var modalInstance = $modal.open({
            templateUrl: 'confirmModal.html',
            controller: "confirmModalCtrl"
        });

        modalInstance.result.then(function() {
            ok && ok();
        }, function() {
            cancel && cancel();
        });
    };



    $scope.init = function(eventId, editType) {
        $scope.eventId = eventId;
        $scope.editType = editType;

        if (editType == 'baseInfo') {
            eventService.getEvent(eventId).success(function(data) {
                $scope.baseInfo = data;
                $scope.baseInfo.types = {};
                for (var type in data.components) {
                    $scope.baseInfo.types[type] = true;
                }
            }).error(ajaxError);
        } else {
            eventService.getEventComponents(eventId).success(function(data) {
                $scope.components = data;
                if (editType == 'comment') {
                    var comment = data.comment;
                    $scope.comment.estimatedParticipants = comment.estimatedParticipants || 0;

                    angular.forEach($scope.comment.limitTypes, function(type) {
                        if (comment.limitType == type.value)
                            $scope.comment.limitType = type;
                    });

                    if (!$scope.comment.limitType) {
                        $scope.comment.limitType = $scope.comment.limitTypes[0];
                    }

                    angular.forEach($scope.comment.limitPeriods, function(p) {
                        if (comment.limitPeriod == p.value)
                            $scope.comment.limitPeriod = p;
                    });

                    $scope.comment.limitNumber = comment.limitNumber;
                } else if (editType == 'vote') {
                    var vote = data.vote;
                    $scope.vote.candidates = vote.candidates;

                    angular.forEach($scope.vote.limits, function(type) {
                        if (vote.ipLimitType == type.value)
                            $scope.vote.ipLimitType = type;
                        if (vote.idLimitType == type.value)
                            $scope.vote.idLimitType = type;
                    });

                    if (!$scope.vote.ipLimitType) {
                        $scope.vote.ipLimitType = $scope.vote.limits[0];
                    }
                    if (!$scope.vote.idLimitType) {
                        $scope.vote.idLimitType = $scope.vote.limits[0];
                    }

                    $scope.vote.ipLimitNumber = vote.ipLimitNumber;
                    $scope.vote.idLimitNumber = vote.idLimitNumber;
                } else if (editType == 'draw') {
                    var draw = data.draw;

                    if (draw.limitType) {
                        angular.forEach($scope.draw.limitTypes, function(type) {
                            if (draw.limitType == type.value)
                                $scope.draw.limitType = type;
                        });
                    } else {
                        // unlimit
                        $scope.draw.limitType = $scope.draw.limitTypes[0];
                    }


                    if (draw.limitPeriod) {
                        angular.forEach($scope.draw.limitPeriods, function(p) {
                            if (draw.limitPeriod == p.value)
                                $scope.draw.limitPeriod = p;
                        });
                    } else {
                        // unlimit
                        $scope.draw.limitPeriod = $scope.draw.limitPeriods[0];
                    }
                    $scope.draw.limitNumber = draw.limitNumber || 0;
                    $scope.draw.limitWins = draw.limitWins || 0;
                    $scope.draw.prizes = draw.prizes;

                    $scope.draw.estimatedParticipants = parseInt(draw.estimatedParticipants, 10);
                }
            }).error(ajaxError);
        }
    };

    $scope.cancelEdit = function() {
        viewEvent($scope.eventId);
    };


    // ============================
    // Comment Config
    // ============================
    $scope.comment = {
        limitNumber: 0,
        limitTypes: [{
            value: 'unlimited',
            text: '不限制'
        }, {
            value: 'byUser',
            text: '用户ID限制'
        }, {
            value: 'byIP',
            text: '用户IP限制'
        }],
        limitPeriods: [{
            value: 'event',
            text: '整期限制'
        }, {
            value: 'day',
            text: '每日限制'
        }],
        estimatedParticipants: 0
    };

    $scope.comment.limitType = $scope.comment.limitTypes[0];
    $scope.comment.limitPeriod = $scope.comment.limitPeriods[0];

    $scope.saveComment = function() {
        if (!$scope.commentForm.$valid)
            return;

        var configs = $scope.components,
            comment = configs.comment;

        comment.estimatedParticipants = $scope.comment.estimatedParticipants || 0;

        if (typeof $scope.comment.limitType.value == 'string')
            comment.limitType = $scope.comment.limitType.value;

        comment.limitPeriod = $scope.comment.limitPeriod.value;
        comment.limitNumber = $scope.comment.limitNumber;

        eventService.updateEventComponents($scope.eventId, configs).success(function(data) {
            viewEvent($scope.eventId);
        }).error(ajaxError);
    };


    // ============================
    //  Vote Config
    // ============================
    $scope.vote = {
        candidates: [],
        limits: [{
            value: '',
            text: '不限制'
        }, {
            value: 'event',
            text: '整期限制'
        }, {
            value: 'day',
            text: '每日限制'
        }],
        ipLimitNumber: 0,
        idLimitNumber: 0,
        selected: {},
        select: function(index) {
            var selected = $scope.vote.selected;
            if (selected[index])
                delete selected[index];
            else
                selected[index] = true;
        },
        selectAll: function() {
            var selected = $scope.vote.selected,
                value = !$scope.vote.isAllSelected();
            for (var i = 0; i < $scope.vote.candidates.length; ++i)
                selected[i] = value;
        },
        isAllSelected: function() {
            var selected = $scope.vote.selected;
            for (var i = 0; i < $scope.vote.candidates.length; ++i)
                if (!selected[i])
                    return false;
            return true;
        },
        isSelected: function(index) {
            return $scope.vote.selected[index];
        },
        edit: function(cand, index) {
            var modalInstance = $modal.open({
                templateUrl: 'newVoteCandidate.html',
                controller: "newVoteCandidateCtrl",
                resolve: {
                    cand: function() {
                        return cand;
                    }
                }
            });

            modalInstance.result.then(function(cand) {
                $scope.vote.candidates.splice(index, 1, cand);
            }, function() {

            });
        },
        new: function() {
            var modalInstance = $modal.open({
                templateUrl: 'newVoteCandidate.html',
                controller: "newVoteCandidateCtrl",
                resolve: {
                    cand: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(cand) {
                $scope.vote.candidates.push(cand);
            }, function() {

            });
        },
        deleteSelected: function() {
            deleteConfirm(function() {
                for (var idx in $scope.vote.selected) {
                    if ($scope.vote.selected[idx])
                        $scope.vote.candidates.splice(idx, 1);
                }
            });
        },
        delete: function(p, index) {
            deleteConfirm(function() {
                $scope.vote.candidates.splice(index, 1);
            });
        }
    };

    $scope.vote.ipLimitType = $scope.vote.limits[0];
    $scope.vote.idLimitType = $scope.vote.limits[0];

    $scope.saveVote = function() {
        if (!$scope.voteForm.$valid)
            return;

        $scope.vote.$dirty = true;
        if ($scope.vote.candidates.length < 0)
            return;

        var configs = $scope.components,
            vote = configs.vote;

        vote.candidates = $scope.vote.candidates;

        if (typeof $scope.vote.ipLimitType.value == 'string') {
            vote.ipLimitType = $scope.vote.ipLimitType.value;
            vote.ipLimitNumber = $scope.vote.ipLimitNumber;
        }

        if (typeof $scope.vote.idLimitType.value == 'string') {
            vote.idLimitType = $scope.vote.idLimitType.value;
            vote.idLimitNumber = $scope.vote.idLimitNumber;
        }

        eventService.updateEventComponents($scope.eventId, configs).success(function(data) {
            viewEvent($scope.eventId);
        }).error(ajaxError);
    };


    // ============================
    //  Draw Config
    // ============================
    $scope.draw = {
        prizes: [],
        limitTypes: [{
            value: '',
            text: '不限制'
        }, {
            value: 'byUser',
            text: '用户ID限制'
        }, {
            value: 'byIP',
            text: '用户IP限制'
        }],
        limitPeriods: [{
            value: 'event',
            text: '整期限制'
        }, {
            value: 'day',
            text: '每日限制'
        }],
        limitNumber: 0,
        limitWins: 0,
        selected: {},
        select: function(index) {
            var selected = $scope.draw.selected;
            if (selected[index])
                delete selected[index];
            else
                selected[index] = true;
        },
        selectAll: function() {
            var selected = $scope.draw.selected,
                value = !$scope.draw.isAllSelected();
            for (var i = 0; i < $scope.draw.prizes.length; ++i)
                selected[i] = value;
        },
        isAllSelected: function() {
            var selected = $scope.draw.selected;
            for (var i = 0; i < $scope.draw.prizes.length; ++i)
                if (!selected[i])
                    return false;
            return true;
        },
        isSelected: function(index) {
            return $scope.draw.selected[index];
        },
        edit: function(prize, index) {
            var modalInstance = $modal.open({
                templateUrl: 'newDrawPrize.html',
                controller: "newDrawPrizeCtrl",
                resolve: {
                    prize: function() {
                        return prize;
                    }
                }
            });

            modalInstance.result.then(function(prize) {
                $scope.draw.prizes.splice(index, 1, prize);
            }, function() {

            });
        },
        new: function() {
            var modalInstance = $modal.open({
                templateUrl: 'newDrawPrize.html',
                controller: "newDrawPrizeCtrl",
                resolve: {
                    prize: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(prize) {
                $scope.draw.prizes.push(prize);
            }, function() {

            });
        },
        deleteSelected: function() {
            deleteConfirm(function() {
                for (var idx in $scope.draw.selected) {
                    if ($scope.draw.selected[idx])
                        $scope.draw.prizes.splice(idx, 1);
                }
            });
        },
        delete: function(p, index) {
            deleteConfirm(function() {
                $scope.draw.prizes.splice(index, 1);
            });
        }
    };
    $scope.draw.limitType = $scope.draw.limitTypes[2];
    $scope.draw.limitPeriod = $scope.draw.limitPeriods[0];


    $scope.saveDraw = function() {
        if (!$scope.drawForm.$valid)
            return;

        $scope.draw.$dirty = true;

        if ($scope.draw.prizes.length < 1)
            return;

        // parseDrawComponent
        var configs = $scope.components,
            draw = configs.draw;

        if (typeof $scope.draw.limitType.value == 'string') {
            draw.limitType = $scope.draw.limitType.value;
            draw.limitNumber = $scope.draw.limitNumber;
            draw.limitWins = $scope.draw.limitWins;
            draw.limitPeriod = $scope.draw.limitPeriod.value;
        }

        draw.prizes = $scope.draw.prizes;

        draw.estimatedParticipants = $scope.draw.estimatedParticipants;

        eventService.updateEventComponents($scope.eventId, configs).success(function(data) {
            viewEvent($scope.eventId);
        }).error(ajaxError);
    };


    $scope.saveBaseInfo = function() {
        if ($scope.baseInfoForm.$invald)
            return;

        eventService.updateEventBaseInfo($scope.eventId, $scope.baseInfo).success(function(data) {
            viewEvent($scope.eventId);
        }).error(ajaxError);
    };

});


app.controller('newDrawPrizeCtrl', function($scope, $modalInstance, prize) {

    $scope.hardRate = 1;
    $scope.total = 0;

    if (prize) {
        // read prize
        $scope.name = prize.name;
        $scope.level = prize.level;
        $scope.total = prize.total;
        $scope.hardRate = prize.hardRate;
        $scope.message = prize.message;
        $scope.types = {};

        for (var i = 1; i < 3; ++i) {
            if (prize.type & i) {
                $scope.types[i] = true;
            }
        }
    }

    $scope.ok = function() {
        // checkbox but in bootstrap, which override the event make ng-model not work
        var type = 0;

        for (var t in $scope.types) {
            if ($scope.types[t])
                type += parseInt(t, 10);
        }

        var prize = {
            name: $scope.name,
            level: $scope.level,
            total: $scope.total,
            hardRate: $scope.hardRate,
            type: '' + type,
            message: $scope.message
        };
        $modalInstance.close(prize);
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };
});



app.controller('newVoteCandidateCtrl', function($scope, $modalInstance, cand) {
    if (cand) {
        // read prize
        $scope.name = cand.name;
        $scope.desc = cand.desc;
        $scope.votes = cand.votes;
    }

    $scope.ok = function() {
        // checkbox but in bootstrap, which override the event make ng-model not work
        var cand = {
            name: $scope.name,
            votes: $scope.votes || 0,
            desc: $scope.desc
        };

        $modalInstance.close(cand);
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };
});


app.controller('confirmModalCtrl', function($scope, $modalInstance) {
    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});