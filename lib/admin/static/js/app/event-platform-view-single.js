require( ['jquery', 'underscore', 'backbone'], function ( $, _, Backbone ) {
    require( ['bs/bootstrap'] );

    var Event = Backbone.Model.extend({
        defaults : function(){
            return {
                eventId    : null,
                eventState : null,
                name       : null,
                owner      : null,
                cityId     : null,
                url        : null,
                message    : '',
                startTime  : null,
                endTime    : null,
                components : {}
            }
        }
    });

    var EventCollection = Backbone.Collection.extend({
        model: Event
    });

    var eventInfo = new EventCollection;

    var EventInfoView = Backbone.View.extend( {
        el : $( "#event-platform-view-single-info" ),

        template : _.template( $( "#event-info-template" ).html() ),

        events : {
        },
        initialize: function(){
            this.listenTo(eventInfo, "add", this.render);
            this.listenTo(eventInfo, "change", this.render);

            eventInfo.add(window.pageInfo.event);
        },
        render: function(){
            var event = eventInfo.at(0);
            this.$("#event-info").html( this.template( event.toJSON() ) );
            return this;
        }
    } );

    var Prize = Backbone.Model.extend( {
        defaults : function () {
            return {
                "name"     : null,
                "type"     : 0,
                "level"    : 0,
                "hardRate" : 1,
                "total"    : 0,
                "sent"     : 0
            }
        }
    } );

    var PrizeListCollection = Backbone.Collection.extend( {
        model         : Prize
    } );

    var PrizeList = new PrizeListCollection;

    var PrizeListRow = Backbone.View.extend( {
        tagName       : "tr",
        className     : "table-row",
        events        : {
            "click .prize-view"   : "viewPrize"
        },
        template      : _.template( $( '#event-prize-template' ).html() ),
        render        : function () {
            this.$el.html( this.template( this.model.toJSON() ) );
            return this;
        }
    });

    var EventPrizeListView = Backbone.View.extend( {
        el : $( "#event-platform-view-single-draw" ),

        initialize     : function () {
            this.listenTo( PrizeList, 'add', this.addOne );
            this.listenTo( PrizeList, 'reset', this.addAll );

            this.loadPrizeList();
        },
        loadPrizeList: function(){
            var components = eventInfo.at(0).get('components');
            if(components.hasOwnProperty('draw')){
                for(var i=0; i < components.draw.prizes.length; i++){
                    PrizeList.add(components.draw.prizes[i]);
                }
            }
            else{
                this.$el.remove();
            }
        },
        addOne         : function ( prize ) {
            var view = new PrizeListRow( {
                model : prize
            } );
            this.$( "#event-prize-list-body" ).append( view.render().el );
            return this;
        },

        addAll         : function () {
            PrizeList.each( this.addOne, this );
            return this;
        }
    } );

    window.App = new Backbone.View.extend({

        eventInfoView: new EventInfoView,
        eventPrizeListView: new EventPrizeListView
    });
} );