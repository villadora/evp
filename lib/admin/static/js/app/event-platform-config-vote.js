/**
 * Date: 13-9-27
 * Author: Jerry
 * Time: 上午10:19
 */
require(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    require(['bs/bootstrap', 'util/scroll']);

    var Candidate = Backbone.Model.extend({
        defaults: function () {
            return {
                "selected": false,
                "voteId": null,
                "name": null,
                "description": null,
                "ticket": 0
            }
        },

        toggle: function () {
            this.set({
                selected: !this.get("selected")
            });
        }
    });

    var CandidateCollection = Backbone.Collection.extend({
        model: Candidate,

        selected: function () {
            return this.where({
                selected: true
            });
        }
    });

    var Candidates = new CandidateCollection;

    if (window.pageInfo && window.pageInfo.votes) {
        Candidates.add(window.pageInfo.votes);
    }

    var EventCandidatesConfigView = Backbone.View.extend({
        el: $("#vote-config"),

        events: {
            "click #vote-config-ok": "saveCandidate",
            "click #vote-config-cancel": "close"
        },

        open: function (vote) {
            if (vote) {
                this.model = vote;
            } else {
                this.model = null;
            }
            this.render();
        },

        close: function () {
            this.model = null;
            $("#form-vote-config").trigger('reset');
            this.$el.modal('hide');
        },

        render: function () {
            var _this = this,
                model = this.model;
            if (model) {
                _this.$("#vote-target-id").val(model.get('voteId'));
                _this.$("#vote-target-name").val(model.get('name'));
                _this.$("#vote-target-description").val(model.get('description'))
            } else {
                $("#form-vote-config").trigger('reset');
            }
        },

        saveCandidate: function () {
            var obj = {
                'voteId': this.$("#vote-target-id").val(),
                'name': this.$("#vote-target-name").val(),
                'description': this.$("#vote-target-description").val()
            };
            if (this.model) {
                this.model.set(obj);
            } else {
                Candidates.add(obj);
            }

            this.close();
        }
    });

    var CandidatesConfig = new EventCandidatesConfigView;

    var CandidatesDeleteConfirmView = Backbone.View.extend({
        el: $("#vote-confirm"),

        events: {
            'click #vote-confirm-ok': "deleteConfirm"
        },

        initialize: function () {
            this.selected = null;
        },

        open: function (vote) {
            this.$(".modal-body").html("是否确定要删除选中的奖品？");
            //删除一个特定奖品
            if (vote) {
                this.selected = [vote];
            }
            //删除选中的奖品
            else {
                this.selected = Candidates.selected();
                if (!this.selected.length) {
                    this.$(".modal-body").html("你没有选择要删除的奖品！")
                }
            }
        },

        close: function () {
            this.$el.modal('hide');
            this.selected = null;
        },

        deleteConfirm: function () {
            if (this.selected) {
                $.each(this.selected, function (key, val) {
                    val.destroy();
                });
            }
            this.close();
        }
    });

    var CandidatesDeleteConfirm = new CandidatesDeleteConfirmView;

    var CandidatesListRow = Backbone.View.extend({
        tagName: "tr",
        className: "table-row",
        events: {
            "click .vote-edit": "openEdit",
            "click .vote-delete": "deleteConfirm",
            "click .vote-select": "toggle"
        },
        template: _.template($('#vote-template').html()),

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
            this.$(".vote-select").on("click", function (e) {
                e.preventDefault();
            });
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        openEdit: function () {
            CandidatesConfig.open(this.model);
            return this;
        },

        deleteConfirm: function () {
            CandidatesDeleteConfirm.open(this.model);
            return this;
        },

        toggle: function () {
            this.model.set({
                'selected': !this.model.get('selected')
            });
        }
    });

    var CandidatesListView = Backbone.View.extend({
        el: $("#event-vote-list"),

        events: {
            "click #vote-toggle-all": "toggleAll"
        },

        initialize: function () {
            Candidates.each(this.addOne, this);

            this.listenTo(Candidates, 'add', this.addOne);
            this.listenTo(Candidates, 'reset', this.addAll);
            this.listenTo(Candidates, 'add', this.checkToggleAll);
            this.listenTo(Candidates, 'remove', this.checkToggleAll);
            this.listenTo(Candidates, 'change', this.checkToggleAll);
        },
        addOne: function (candidate) {
            var view = new CandidatesListRow({
                model: candidate
            });
            this.$("#event-vote-list-body").append(view.render().el);
            return this;
        },

        addAll: function () {
            Candidates.each(this.addOne, this);
            return this;
        },

        checkToggleAll: function () {
            var checked = Candidates.selected().length === Candidates.length;

            this.$("#vote-toggle-all").attr("checked", checked);

            return checked;
        },

        toggleAll: function () {
            var check = Candidates.selected().length === Candidates.length;
            Candidates.each(function (vote) {
                vote.set({
                    'selected': !check
                });
            });
            return this;
        }
    });

    var CandidatesList = new CandidatesListView;

    $('#save-draft').on('click', function () {
        $("#vote-list-holder").val(JSON.stringify(Candidates.models));
        $('#eventForm-draft').val('true');
        $('#event-platform-config-vote').submit();
    });

    var EventConfigView = Backbone.View.extend({
        el: $("#event-platform-config-vote"),

        events: {
            'click #vote-add': "addCandidate",
            'click #vote-delete-selected': "deleteConfirm",
            'submit': "submitCheck"
        },
        addCandidate: function () {
            CandidatesConfig.open();
        },
        deleteConfirm: function () {
            CandidatesDeleteConfirm.open();
            return this;
        },
        submitCheck: function (event) {
            if ($('#eventForm-draft').val())
                return;

            if (!this._validate()) {
                event.preventDefault();
            }
        },
        _validate: function () {
            var error = 0,
                inputs = {
                    limitNumber: $("#event-vote-limitation-number")
                },
                data = Candidates.models,
                message = [];

            if (!$.trim(inputs.limitNumber.val())) {
                message.push("请输入限制投票次数。");
                error += 1;
            }

            $("#alert-message").remove();

            if (error) {
                var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                    alertMessage = "<h4 class='alert-heading'>错误</h4>";
                $.each(message, function (key, val) {
                    alertMessage += "<li>" + val + "</li>";
                });

                $("<div id='alert-message' class='alert alert-error fade in'></div>").html(closeBtn + alertMessage).appendTo("#alert-window");

                $.scrollTo("#alert-message", 500, {
                    offset: {
                        top: -50
                    }
                });
            }

            return !error;
        }
    });

    var EventConfig = new EventConfigView;

    var AppView = Backbone.View.extend({
        eventConfigView: EventConfig,
        eventCandidatesListView: CandidatesList,
        eventCandidatesConfigView: CandidatesConfig,
        eventCandidatesList: Candidates
    });

    window.App = new AppView;
});