require(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {

    require( ['bs/bootstrap','util/datepick', 'util/scroll'] );

    var AppView = Backbone.View.extend( {
        el         : $( "#event-platform-create" ),
        events     : {
            'submit': "submitForm",
            'click #save-draft': 'saveDraft'
        },
        initialize : function(){
            var event = window.pageInfo && window.pageInfo.event;
            if ( event ) {
                $( "[name=name]" ).val( event.name );
                $( "[name=cityId]" ).val( event.cityId );
                $( "[name=url]" ).val( event.url );
                $( "[name=owner]" ).val( event.owner );
                $( "[name=startTime]" ).val( event.startTime );
                $( "[name=endTime]" ).val( event.endTime );
                $( "[name=message]" ).val( event.message );
                if ( event.components && event.components.hasOwnProperty( 'draw' ) ) {
                    $( "#eventType-draw" ).attr( 'checked', true );
                }
                if ( event.components && event.components.hasOwnProperty( 'vote' ) ) {
                    $( "#eventType-vote" ).attr( 'checked', true );
                }
                if ( event.components && event.components.hasOwnProperty( 'promotion' ) ) {
                    $( "#eventType-promotion" ).attr( 'checked', true );
                }
                if ( event.components && event.components.hasOwnProperty( 'quiz' ) ) {
                    $( "#eventType-quiz" ).attr( 'checked', true );
                }
            }
        },
        _validate  : function () {
            var error = 0,
                inputs = {
                    eventName: $("#event-name"),
                    eventCity: $("#event-city"),
                    eventLink: $("#event-link"),
                    eventOwner: $("#event-owner"),
                    eventStartTime: $("#event-start-time"),
                    eventEndTime: $("#event-end-time"),
                    eventType: $("input[name=types]")
                },
                message = [];

            if (!$.trim(inputs.eventName.val())) {
                message.push("请输入活动名称。");
                error += 1;
            }

            if (!$.isNumeric(inputs.eventCity.val())) {
                message.push("请输入正确的活动城市ID");
                error += 1;
            }

            if (!$.trim(inputs.eventLink.val())) {
                message.push("请输入活动链接。");
                error += 1;
            }

            if (!$.trim(inputs.eventOwner.val())) {
                message.push("请输入活动负责人。");
                error += 1;
            }

            if (!$.trim(inputs.eventStartTime.val())) {
                message.push("请输入开始时间。");
                error += 1;
            } else if (/^d{4}-d{2}-d{2}$/.test(inputs.eventStartTime.val())) {
                message.push("请输入正确的开始时间。");
                error += 1;
            }

            if (!$.trim(inputs.eventEndTime.val())) {
                message.push("请输入结束时间。");
                error += 1;
            } else if (/^d{4}-d{2}-d{2}$/.test(inputs.eventEndTime.val())) {
                message.push("请输入正确的结束时间。");
                error += 1;
            }

            if (Date.parse(inputs.eventStartTime.val()) > Date.parse(inputs.eventEndTime.val())) {
                message.push("开始时间不能大于结束时间。");
                error += 1;
            }

            if (!inputs.eventType.filter(":checked").length) {
                message.push("请至少选择一个活动类型。");
                error += 1;
            }

            $("#alert-message").remove();

            if (error) {
                var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                    alertMessage = "<h4 class='alert-heading'>错误</h4>";
                $.each(message, function(key, val) {
                    alertMessage += "<li>" + val + "</li>";
                });

                $("<div id='alert-message' class='alert alert-error fade in'></div>").html(closeBtn + alertMessage).appendTo("#alert-window");

                $.scrollTo( "#alert-message", 500, {offset: { top:-50 }} );
            }

            return !error;
        },
        submitForm: function(event) {
            if(!$('#eventForm-draft').val()) {
                if (!this._validate()) {
                    event.preventDefault();
                }
            }
        },
        saveDraft: function(event) {
            $('#eventForm-draft').val('true');
            $('#event-platform-create').submit();
        }
    });

    window.app = new AppView();
});