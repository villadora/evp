require( ['jquery', 'underscore', 'backbone'], function ( $, _, Backbone ) {
    require(['bs/bootstrap']);
    require( ['util/pagination', 'util/datepick', 'util/scroll'], function ( Pagination ) {

        var DrawRecord = Backbone.Model.extend( {
            defaults : function () {
                return {
                    '_id'      : null,
                    'eventId'  : null,
                    'prize'    : {
                        'id'   :-1,
                        'rank' :-1,
                        'level':-1,
                        'name' : null
                    },
                    'userInfo' : {
                    },
                    'userId'   : null,
                    'name'     : null,
                    'create'   : null,
                    'phone'    : null,
                    'address'  : null,
                    'cityId'   : null,
                    'userIP'   : null
                };
            }
        } );

        var DrawRecordCollection = Backbone.Collection.extend( {
            model : DrawRecord
        } );

        var DrawRecords = new DrawRecordCollection;

        var DrawRecordQueryView = Backbone.View.extend( {
            el : $( "#event-platform-draw-prize-query" ),

            events           : {
                'click #query-button' : 'queryDrawRecords'
            },
            initialize       : function () {
                this.listenTo( PaginationView.model, 'change:page', this.queryDrawRecords );
                this.queryDrawRecords();
            },
            loadRecords      : function ( records ) {
                DrawRecords.reset( records );
                return this;
            },
            queryDrawRecords : function () {
                var _this = this;

                function error ( message ) {

                    var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                        alertMessage = "<h4 class='alert-heading'>错误</h4>";

                    alertMessage += "<li>" + (message || "未知错误，请重试。") + "</li>";

                    $( "#alert-message" ).remove();

                    $( "<div id='alert-message' class='alert alert-error fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                    $.scrollTo( "#alert-message", 500, {offset: { top:-50 }} );
                }

                $.ajax( {
                    url      : PaginationView.model.get( 'url' ) + PaginationView.model.get( 'page' ),
                    data     : this.$el.serialize(),
                    type     : 'GET',
                    success  : function ( response ) {
                        switch (response.code) {
                            case 200: //success
                                //TODO: 确定活动列表格式
                                _this.loadRecords( response.msg.list );

                                PaginationView.model.set( 'total', response.msg.total );

                                break;
                            default : //error
                                error( response.msg );
                                break;
                        }
                    },
                    error    : function () {
                        error();
                    }
                } );
            }
        } );

        var DrawRecordView = Backbone.View.extend( {
            tagName   : 'tr',
            className : 'table-row',

            template : _.template( $( '#record-template' ).html() ),

            events      : {
                'click .record-delete' : 'deleteEvent'
            },
            initialize  : function () {
                this.listenTo( this.model, 'change', this.render );
                this.listenTo( this.model, 'destroy', this.remove );
            },
            hide        : function () {
                this.$el.addClass( 'hide' );
            },
            show        : function () {
                this.$el.removeClass( 'hide' );
            },
            render      : function () {
                this.$el.html( this.template( this.model.toJSON() ) );
                return this;
            },
            deleteEvent : function () {
                recordDeleteConfirmView.open( this.model );
                return this;
            }
        } );

        var DrawRecordList = Backbone.View.extend( {
            el : $( "#event-platform-record-table" ),

            initialize : function () {
                this.listenTo( DrawRecords, 'add', this.addOne );
                this.listenTo( DrawRecords, 'reset', this.addAll );
                this.listenTo( DrawRecords, 'all', this.render );

            },

            addOne : function ( record ) {
                var view = new DrawRecordView( {
                    model : record
                } );
                this.$( "#event-platform-record-table-body" ).append( view.render().el );
                return this;
            },
            addAll : function ( models, options ) {
                _.each( options.previousModels, function ( model ) {
                    model.destroy();
                } );
                DrawRecords.each( this.addOne, this );
                return this;
            }
        } );

        var PaginationView = new Pagination.View( {
            el    : $( '#event-query-pagination' ),
            model : new Pagination.Model( {
                'page'  : 1,
                'total' : 1,
                'limit' : 20,
                //TODO: 添加地址
                'url'   : '/activity/admin/ajax/event/' + window.pageInfo.event._id + '/results/draw/page/'
            } )
        } );

        var RecordDeleteConfirmView = Backbone.View.extend( {
            el : $( "#window-confirm" ),

            events        : {
                'click #window-confirm-ok' : "deleteConfirm"
            },
            initialize    : function () {
                this.selected = null;
            },
            open          : function ( record ) {
                this.$( ".modal-body" ).html( "是否确定要删除选中的记录？" );
                this.selected = [ record ];
            },
            close         : function () {
                this.$el.modal( 'hide' );
                this.selected = null;
            },
            deleteConfirm : function () {

                function error ( message ) {
                    var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                        alertMessage = "<h4 class='alert-heading'>错误</h4>";
                    alertMessage += "<li>" + ( message || "发生错误，请重试。") + "</li>";
                    $( "#alert-message" ).remove();

                    $( "<div id='alert-message' class='alert alert-error fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                    $.scrollTo( "#alert-message", 500, {offset: { top:-50 }} );
                }

                if ( this.selected ) {
                    $.each( this.selected, function ( key, model ) {
                        $.ajax( {
                            url  : "/activity/admin/ajax/event/" + window.pageInfo.event._id + "/results/" + model.get( '_id' ), //TODO:地址
                            type : "DELETE",

                            success : function ( response ) {
                                if ( response.code == 200 ) {
                                    var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                                        alertMessage = "<h4 class='alert-heading'>删除成功！</h4>";

                                    $( "#alert-message" ).remove();

                                    $( "<div id='alert-message' class='alert alert-success fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                                    $.scrollTo( "#alert-message", 500, {offset: { top:-50 }} );
                                    model.destroy();
                                }
                                else {
                                    error( response.msg );
                                }
                            },
                            error   : function () {
                                error();
                            }
                        } );
                    } );
                }
                this.close();
            }
        } );

        var recordDeleteConfirmView = new RecordDeleteConfirmView;

        var AppView = Backbone.View.extend( {
            el          : $( 'body' ),
            events      : {
                'click #export-to-excel' : 'exportTable'
            },
            exportTable : function () {
                window.open( 'download-url' );
            },
            views       : {
                DrawRecordQueryView : new DrawRecordQueryView,
                DrawRecordList      : new DrawRecordList
            }
        } );

        window.App = new AppView;
    } );

} );