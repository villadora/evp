/**
 * Date: 13-9-27
 * Author: Jerry
 * Time: 上午10:19
 */
require(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    require(['bs/bootstrap', 'util/scroll']);

    var EventConfigView = Backbone.View.extend({
        el: $("#event-platform-config-draw"),

        events: {
            'click #prize-add': "addPrize",
            'click #prize-delete-selected': "deleteConfirm",
            'submit': "submitCheck"
        },
        initialize: function () {
            var draw = window.pageInfo && window.pageInfo.event.components.review;

            if (draw) {
                $("[name=limitType]").val(draw.limitType);
                $("[name=limitPeriod]").val(draw.limitPeriod);
                $("[name=estimatedParticipants]").val(draw.estimatedParticipants);
                $("[name=limitNumber]").val(draw.limitNumber);
                $("[name=limitWins]").val(draw.limitWins);

            }
        },
        submitCheck: function (event) {

            if (!this._validate()) {
                event.preventDefault();
            }
        },
        _validate: function () {
            var error = 0,
                inputs = {
                    limitType: $("[name=limitType]"),
                    limitPeriod: $("[name=limitPeriod]"),
                    limitNumber: $("#event-lottery-limitation-number"),
                    limitWins: $("#event-lottery-limitation-win"),
                    estimatedParticipants: $("#event-lottery-estimated-participants")
                },
                data = PrizeList.models,
                message = [];

            if (!$.trim(inputs.limitNumber.val())) {
                message.push("请输入限制抽奖次数。");
                error += 1;
            }
            if (!$.trim(inputs.limitWins.val())) {
                message.push("请输入限制中奖次数。");
                error += 1;
            }
            if (!$.trim(inputs.estimatedParticipants.val())) {
                message.push("请输入预期参与次数。");
                error += 1;
            }
            if (!data.length) {
                message.push("请填写至少一个奖品。");
                error += 1;
            }
            else {
                $("#prize-list-holder").val(JSON.stringify(data));
            }

            $("#alert-message").remove();

            if (error) {
                var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                    alertMessage = "<h4 class='alert-heading'>错误</h4>";
                $.each(message, function (key, val) {
                    alertMessage += "<li>" + val + "</li>";
                });

                $("<div id='alert-message' class='alert alert-error fade in'></div>").html(closeBtn + alertMessage).appendTo("#alert-window");

                $.scrollTo("#alert-message", 500, {offset: { top: -50 }});
            }

            return !error;
        }
    });

    var eventConfigView = new EventConfigView;

    var AppView = Backbone.View.extend({
        eventConfigView: eventConfigView,
        eventPrizeListView: eventPrizeListView,
        eventPrizeConfigView: eventPrizeConfigView,
        eventPrizeList: PrizeList
    });

    window.App = new AppView;
});