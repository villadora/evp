/**
 * Date: 13-9-27
 * Author: Jerry
 * Time: 上午10:19
 */
require( ['jquery', 'underscore', 'backbone'], function ( $, _, Backbone ) {
    require( ['bs/bootstrap', 'util/scroll'] );

    var Prize = Backbone.Model.extend( {
        defaults : function () {
            return {
                "selected" : false,
                "name"     : null,
                "type"     : 0,
                "level"    : 0,
                "hardRate" : 1,
                "total"    : 0,
                "sent"     : 0,
                'message'  : ''
            }
        },

        toggle : function () {
            this.set( {
                selected : !this.get( "selected" )
            } );
        }
    } );

    var PrizeListCollection = Backbone.Collection.extend( {
        model         : Prize,

        selected      : function () {
            return this.where( {selected : true} );
        }
    } );

    var PrizeList = new PrizeListCollection;

    var EventPrizeConfigView = Backbone.View.extend( {
        el : $( "#prize-config" ),

        events     : {
            "click #prize-config-ok"     : "savePrize",
            "click #prize-config-cancel" : "close"
        },

        open       : function ( prize ) {
            if ( prize ) {
                this.model = prize;
            }
            else {
                this.model = null;
            }
            this.render();
        },

        close      : function () {
            this.model = null;
            $( "#form-prize-config" ).trigger( 'reset' );
            this.$el.modal( 'hide' );
        },

        render     : function () {
            var _this = this;
            if ( this.model ) {
                this.$( "#prize-name" ).val( this.model.get( 'name' ) );
                this.$( "#prize-rank" ).val( this.model.get( 'level' ) );
                this.$( "#prize-difficult-level" ).val( this.model.get( 'hardRate' ) );
                this.$( "#prize-total" ).val( this.model.get( 'total' ) );
                this.$( "#prize-message" ).val( this.model.get( 'message' ) );

                this.$( "[name=prize-type]" ).each( function () {
                    var $this = $( this ),
                        $value = parseInt( $this.val(), 10 ),
                        type = _this.model.get( 'type' );
                    if ( !!($value & type) ) {
                        $this.attr( 'checked', true );
                    }
                    else {
                        $this.attr( 'checked', false );
                    }
                } );
            }
            else {
                $( "#form-prize-config" ).trigger( 'reset' );
                this.$( "[name=prize-type]" ).attr( 'checked', false );
            }
        },

        savePrize  : function () {
            var obj = {
                'name'     : this.$( "#prize-name" ).val(),
                'level'    : this.$( "#prize-rank" ).val(),
                'type'     : (function ( view ) {
                    var val = 0;
                    view.$( "[name=prize-type]:checked" ).each( function () {
                        val += parseInt( $( this ).val(), 10 );
                    } );
                    return val;
                })( this ),
                'hardRate' : this.$( "#prize-difficult-level" ).val() || 1,
                'total'    : this.$( "#prize-total" ).val(),
                'message'  : this.$( "#prize-message" ).val()
            };
            if ( this.model ) {
                this.model.set( obj );
            }
            else {
                PrizeList.add( obj );
            }

            this.close();
        }
    } );

    var eventPrizeConfigView = new EventPrizeConfigView;

    var PrizeDeleteConfirmView = Backbone.View.extend( {
        el : $( "#prize-confirm" ),

        events        : {
            'click #prize-confirm-ok' : "deleteConfirm"
        },

        initialize    : function () {
            this.selected = null;
        },

        open          : function ( prize ) {
            this.$( ".modal-body" ).html( "是否确定要删除选中的奖品？" )
            //删除一个特定奖品
            if ( prize ) {
                this.selected = [ prize ];
            }
            //删除选中的奖品
            else {
                this.selected = PrizeList.selected();
                if ( !this.selected.length ) {
                    this.$( ".modal-body" ).html( "你没有选择要删除的奖品！" )
                }
            }
        },

        close         : function () {
            this.$el.modal( 'hide' );
            this.selected = null;
        },

        deleteConfirm : function () {
            if ( this.selected ) {
                $.each( this.selected, function ( key, val ) {
                    val.destroy();
                } );
            }
            this.close();
        }
    } );

    var prizeDeleteConfirmView = new PrizeDeleteConfirmView;

    var PrizeListRow = Backbone.View.extend( {
        tagName       : "tr",
        className     : "table-row",
        events        : {
            "click .prize-edit"   : "openEdit",
            "click .prize-delete" : "deleteConfirm",
            "click .prize-select" : "toggle"
        },
        template      : _.template( $( '#prize-template' ).html() ),

        initialize    : function () {
            this.listenTo( this.model, 'change', this.render );
            this.listenTo( this.model, 'destroy', this.remove );
            this.$( ".prize-select" ).on( "click", function ( e ) {
                e.preventDefault();
            } );
        },

        render        : function () {
            this.$el.html( this.template( this.model.toJSON() ) );
            return this;
        },

        openEdit      : function () {
            eventPrizeConfigView.open( this.model );
            return this;
        },

        deleteConfirm : function () {
            prizeDeleteConfirmView.open( this.model );
            return this;
        },

        toggle        : function () {
            this.model.set( {
                'selected' : !this.model.get( 'selected' )
            } );
        }
    } );

    var EventPrizeListView = Backbone.View.extend( {
        el : $( "#event-prize-list" ),

        events : {
            "click #prize-toggle-all" : "toggleAll"
        },

        initialize     : function () {
            this.listenTo( PrizeList, 'add', this.addOne );
            this.listenTo( PrizeList, 'reset', this.addAll );
            this.listenTo( PrizeList, 'add', this.checkToggleAll );
            this.listenTo( PrizeList, 'remove', this.checkToggleAll );
            this.listenTo( PrizeList, 'change', this.checkToggleAll );
        },
        addOne         : function ( prize ) {
            var view = new PrizeListRow( {
                model : prize
            } );
            this.$( "#event-prize-list-body" ).append( view.render().el );
            return this;
        },

        addAll         : function () {
            PrizeList.each( this.addOne, this );
            return this;
        },

        checkToggleAll : function () {
            var checked = PrizeList.selected().length === PrizeList.length;

            this.$( "#prize-toggle-all" ).attr( "checked", checked );

            return checked;
        },

        toggleAll      : function () {
            var check = PrizeList.selected().length === PrizeList.length;
            PrizeList.each( function ( prize ) {
                prize.set( {
                    'selected' : !check
                } );
            } );
            return this;
        }
    } );

    var eventPrizeListView = new EventPrizeListView;

    var EventConfigView = Backbone.View.extend( {
        el : $( "#event-platform-config-draw" ),

        events        : {
            'click #prize-add'             : "addPrize",
            'click #prize-delete-selected' : "deleteConfirm",
            'submit'                       : "submitCheck"
        },
        initialize: function(){
            var draw = window.pageInfo && window.pageInfo.event.components.draw;

            if(draw){
                $("[name=limitType]" ).val(draw.limitType);
                $("[name=limitPeriod]" ).val(draw.limitPeriod);
                $("[name=estimatedParticipants]" ).val(draw.estimatedParticipants);
                $("[name=limitNumber]" ).val(draw.limitNumber);
                $("[name=limitWins]" ).val(draw.limitWins);

                for(var i = 0; i < draw.prizes.length; i++){
                    PrizeList.add(draw.prizes[i]);
                }
            }
        },
        addPrize      : function () {
            eventPrizeConfigView.open();
        },
        deleteConfirm : function () {
            prizeDeleteConfirmView.open();
            return this;
        },
        submitCheck   : function ( event ) {

            if ( !this._validate() ) {
                event.preventDefault();
            }
        },
        _validate     : function () {
            var error = 0,
                inputs = {
                    limitType:   $("[name=limitType]" ),
                    limitPeriod:  $("[name=limitPeriod]" ),
                    limitNumber           : $( "#event-lottery-limitation-number" ),
                    limitWins             : $( "#event-lottery-limitation-win" ),
                    estimatedParticipants : $( "#event-lottery-estimated-participants" )
                },
                data = PrizeList.models,
                message = [];

            if ( !$.trim( inputs.limitNumber.val() ) ) {
                message.push( "请输入限制抽奖次数。" );
                error += 1;
            }
            if ( !$.trim( inputs.limitWins.val() ) ) {
                message.push( "请输入限制中奖次数。" );
                error += 1;
            }
            if ( !$.trim( inputs.estimatedParticipants.val() ) ) {
                message.push( "请输入预期参与次数。" );
                error += 1;
            }
            if ( !data.length ) {
                message.push( "请填写至少一个奖品。" );
                error += 1;
            }
            else {
                $( "#prize-list-holder" ).val( JSON.stringify( data ) );
            }

            $( "#alert-message" ).remove();

            if ( error ) {
                var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                    alertMessage = "<h4 class='alert-heading'>错误</h4>";
                $.each( message, function ( key, val ) {
                    alertMessage += "<li>" + val + "</li>";
                } );

                $( "<div id='alert-message' class='alert alert-error fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                $.scrollTo( "#alert-message", 500, {offset: { top:-50 }} );
            }

            return !error;
        }
    } );

    var eventConfigView = new EventConfigView;

    var AppView = Backbone.View.extend( {
        eventConfigView      : eventConfigView,
        eventPrizeListView   : eventPrizeListView,
        eventPrizeConfigView : eventPrizeConfigView,
        eventPrizeList       : PrizeList
    } );

    window.App = new AppView;
} );