/**
 * Date: 13-9-27
 * Author: Jerry
 * Time: 上午10:19
 */
require(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    require(['bs/bootstrap', 'util/scroll']);

    var EventConfigView = Backbone.View.extend({
        el: $("#event-platform-config-review"),

        events: {
            'submit': "submitCheck"
        },
        initialize: function () {
            var review = window.pageInfo && window.pageInfo.event.components.review;

            if (review) {
                $("[name=limitType]").val(review.limitType);
                $("[name=limitPeriod]").val(review.limitPeriod);
                $("[name=limitNumber]").val(review.limitNumber);
                $("[name=estimatedParticipants]").val(review.estimatedParticipants);
            }
        },
        submitCheck: function (event) {

            if (!this._validate()) {
                event.preventDefault();
            }
        },
        _validate: function () {
            var error = 0,
                inputs = {
                    limitNumber: $("#event-review-limitation-number"),
                    estimatedParticipants: $("#event-review-estimated-participants")
                },
                message = [];

            if (!$.trim(inputs.limitNumber.val())) {
                message.push("请输入限制评论次数。");
                error += 1;
            }

            $("#alert-message").remove();

            if (error) {
                var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                    alertMessage = "<h4 class='alert-heading'>错误</h4>";
                $.each(message, function (key, val) {
                    alertMessage += "<li>" + val + "</li>";
                });

                $("<div id='alert-message' class='alert alert-error fade in'></div>").html(closeBtn + alertMessage).appendTo("#alert-window");

                $.scrollTo("#alert-message", 500, {
                    offset: {
                        top: -50
                    }
                });
            }

            return !error;
        }
    });

    var eventConfigView = new EventConfigView;

    var AppView = Backbone.View.extend({
        eventConfigView: eventConfigView
    });

    window.App = new AppView;
});