/**
 * Date: 13-9-26
 * Author: Jerry
 * Time: 下午2:42
 */
require( ['jquery', 'underscore', 'backbone'], function ( $, _, Backbone ) {
    require( ['bs/bootstrap'] );
    require( [ 'util/pagination', 'util/datepick', 'util/scroll'], function ( Pagination ) {
        var Event = Backbone.Model.extend( {
            defaults : function () {
                return {
                    _id    : null,
                    eventState : null,
                    name       : null,
                    owner      : null,
                    cityId     : null,
                    url        : null,
                    message    : null,
                    startTime  : null,
                    endTime    : null,
                    components : {}
                };
            }
        } );
        var EventListCollection = Backbone.Collection.extend( {
            model : Event
        } );

        var EventList = new EventListCollection();

        var EventItemView = Backbone.View.extend( {
            tagName   : "tr",
            className : "table-row",
            events    : {
                "click .event-delete" : "deleteEvent"
            },

            template   : _.template( $( '#event-list-template' ).html() ),
            initialize : function () {
                this.listenTo( this.model, 'change', this.render );
                this.listenTo( this.model, 'destroy', this.remove );
            },
            render     : function () {
                this.$el.html( this.template( this.model.toJSON() ) );
                return this;
            },
            deleteEvent: function(){
                eventDeleteConfirm.open( this.model );
            }
        } );

        var EventListView = Backbone.View.extend( {
            $el : $( "#event-platform-query" ),

            initialize : function () {
                this.listenTo( EventList, 'add', this.addOne );
                this.listenTo( EventList, 'reset', this.addAll );
                this.listenTo( EventList, 'all', this.render );
            },
            addOne     : function ( event ) {
                var view = new EventItemView( {
                    model : event
                } );
                $( "#event-platform-query-body" ).append( view.render().el );
                return this;
            },
            addAll     : function ( models, options ) {
                _.each( options.previousModels, function ( model ) {
                    model.destroy();
                } );
                EventList.each( this.addOne, this );
                return this;
            }
        } );

        var EventDeleteConfirmView = Backbone.View.extend( {
            el : $( "#window-confirm" ),

            events        : {
                'click #window-confirm-ok' : "deleteConfirm"
            },
            initialize    : function () {
                this.selected = null;
            },
            open          : function ( event ) {
                this.$( "#window-confirm-body" ).empty().html( "确实要删除整个活动吗？该操作不可恢复！" );
                this.selected = [ event ];
            },
            close         : function () {
                this.$el.modal( 'hide' );
                this.selected = null;
            },
            deleteConfirm : function () {

                function error ( message ) {
                    var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                        alertMessage = "<h4 class='alert-heading'>错误</h4>";
                    alertMessage += "<li>" + ( message || "发生错误，请重试。") + "</li>";
                    $( "#alert-message" ).remove();

                    $( "<div id='alert-message' class='alert alert-error fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                    $.scrollTo( "#alert-message", 500, {offset: { top:-50 }} );
                }

                if ( this.selected ) {
                    $.each( this.selected, function ( key, model ) {
                        $.ajax( {
                            url  : "/activity/admin/ajax/event/" + model.get( '_id' ),
                            type : "DELETE",

                            success : function ( response ) {
                                if ( response.code == 200 ) {
                                    var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                                        alertMessage = "<h4 class='alert-heading'>删除成功！</h4>";

                                    $( "#alert-message" ).remove();

                                    $( "<div id='alert-message' class='alert alert-success fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                                    $.scrollTo( "#alert-message", 500, {offset: { top:-50 }} );

                                    model.destroy();
                                }
                                else {
                                    error( response.msg );
                                }
                            },
                            error   : function () {
                                error();
                            }
                        } );
                    } );
                }
                this.close();
            }
        } );

        var eventDeleteConfirm = new EventDeleteConfirmView;

        var PaginationView = new Pagination.View( {
            el    : $( '#event-query-pagination' ),
            model : new Pagination.Model( {
                'page'  : 1,
                'total' : 5,
                'limit' : 20,
                //TODO: 添加地址
                'url'   : '/activity/admin/ajax/event/page/'
            } )
        } );

        var EventPlatformQueryView = Backbone.View.extend( {
            el : $( "#event-platform-view-query" ),

            events     : {
                'click #query-button' : 'queryEvents'
            },
            initialize : function () {
                this.queryEvents();
                this.listenTo(PaginationView.model, 'change:page', this.queryEvents);
            },

            _validate      : function () {
                var error = 0,
                    inputs = {
                        startTime : $( "#query-time-start-date" ),
                        endTime   : $( "#query-time-end-date" ),
                        name      : $( "#query-string" ),
                        owner     : $( "#query-owner" )
                    },
                    message = [];

                if ( Date.parse( inputs.startTime.val() ) > Date.parse( inputs.endTime.val() ) ) {
                    message.push( "时间下限不能大于时间上限。" );
                    error += 1;
                }

                $( "#alert-message" ).remove();

                if ( error ) {
                    var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                        alertMessage = "<h4 class='alert-heading'>错误</h4>";
                    $.each( message, function ( key, val ) {
                        alertMessage += "<li>" + val + "</li>";
                    } );

                    $( "<div id='alert-message' class='alert alert-error fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                    $( window ).scrollTop( 0 );
                }

                return !error;
            },
            _loadEventList : function ( eventList ) {
                EventList.reset();
                for ( var i = 0; i < eventList.length; i++ ) {
                    EventList.add( eventList[i] );
                }
            },
            queryEvents    : function () {
                var _this = this;

                function error( message ){

                    var closeBtn = "<button type='button' class='close' data-dismiss='alert'>×</button>",
                        alertMessage = "<h4 class='alert-heading'>错误</h4>";

                    alertMessage += "<li>" + (message || "未知错误，请重试。") + "</li>";

                    $( "<div id='alert-message' class='alert alert-error fade in'></div>" ).html( closeBtn + alertMessage ).appendTo( "#alert-window" );

                    $( window ).scrollTop( 0 );
                }
                if ( !this._validate() ) {
                    return false;
                }

                $.ajax( {
                    url      : PaginationView.model.get('url') + PaginationView.model.get('page'),
                    data     : _this.$el.serialize(),
                    type     : "GET",
                    success  : function ( response ) {
                        switch (response.code) {
                            case 200: //success
                                //TODO: 确定活动列表格式
                                _this._loadEventList( response.msg.list );

                                PaginationView.model.set('total', response.msg.total);

                                break;
                            default : //error
                                error ( response.msg );
                                break;
                        }
                    },
                    error    : function () {
                        error ( );
                    },
                    complete : function () {
                    }
                } );

                return true;
            }
        } );

        var AppView = Backbone.View.extend( {
            eventListView          : new EventListView,
            eventPlatformQueryView : new EventPlatformQueryView
        } );

        window.App = new AppView;
    } );

} );