/**
 * Date: 13-9-27
 * Author: Jerry
 * Time: 下午3:45
 */

requirejs.config({
    baseUrl: '/activity-platform/lib/admin/static/js/lib',
    paths: {
        app: '/activity-platform/lib/admin/static/js/app',
        backbone: 'backbone',
        underscore: 'underscore',
        jquery: 'jquery-1.8.3'
    },
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});