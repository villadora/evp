/**
 * Date: 13-10-14
 * Author: Jerry
 * Time: 下午4:50
 */
define(
    'util/datepick',
    ['jquery', 'bs/bootstrap'], function ( $ ) {
        require( ['bs/bootstrap-datetimepicker'], function () {
            $( "<link>" ).attr( {
                rel  : "stylesheet",
                type : "text/css",
                href : "/activity/admin/static/bootstrap/css/datetimepicker.css"
            } ).appendTo( "head" );

            $.fn.datetimepicker.dates['zh-CN'] = {
                days        : ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
                daysShort   : ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
                daysMin     : ["日", "一", "二", "三", "四", "五", "六", "日"],
                months      : ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthsShort : ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                today       : "今日",
                suffix      : [],
                meridiem    : []
            };
            var date = new Date();
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            date.setMilliseconds(0);

            $.fn.datetimepicker.defaults.language = 'zh-CN';
            $.fn.datetimepicker.defaults.pickerPosition = 'bottom-left';
            $.fn.datetimepicker.defaults.minView = 'hour';
            $.fn.datetimepicker.defaults.initialDate = date;
            $.fn.datetimepicker.defaults.autoclose = true;
            $.fn.datetimepicker.defaults.forceParse = false;

            $( ".date-picker" ).datetimepicker();
        } );
    }
);