/**
 * Date: 13-10-12
 * Author: Jerry
 * Time: 下午5:07
 */

define(
    'util/pagination',
    ['jquery', 'backbone'],
    function ( $, Backbone ) {
        return {
            Model : Backbone.Model.extend( {
                defaults : function () {
                    return {
                        'page'  : 1,
                        'total' : 1,
                        'limit' : 20,
                        'url'   : ''
                    }
                }
            } ),
            View  : Backbone.View.extend( {
                events : {
                    'click a' : 'loadPage'
                },

                initialize : function () {
                    this.render();
                    this.listenTo( this.model, 'change', this.render );
                },
                render     : function () {
                    var _this = this,
                        page = _this.model.get( 'page' ),
                        total = _this.model.get( 'total' ),
                        url = _this.model.get( 'url' );

                    this.$el.empty();

                    //第一页
                    var $firstPage = $( '<li></li>' );
                    if ( page == 1 ) {
                        $firstPage.addClass( 'disabled' );
                        $( '<span>&laquo;</span>' ).appendTo( $firstPage );
                    }
                    else {
                        $( '<a title="第一页">&laquo;</a>' ).attr( {
                            'data-page' : 1,
                            'href'      : '#page-' + 1
                        } ).appendTo( $firstPage );
                    }

                    this.$el.append( $firstPage );

                    var $pages;
                    for ( var i = page - 3; i <= page + 3; i++ ) {
                        if ( i >= 1 && i <= total ) {
                            $pages = $( '<li></li>' );
                            if ( i == page ) {
                                $pages.addClass( 'active' );
                                $( '<span>' + i + '</span>' ).attr( {
                                    'href' : '#page-' + (i)
                                } ).appendTo( $pages );
                            }
                            else {
                                $( '<a>' + i + '</a>' ).attr( {
                                    'data-page' : i,
                                    'href'      : '#page-' + (i)
                                } ).appendTo( $pages );
                            }

                            this.$el.append( $pages );
                        }
                    }

                    //最后一页
                    var $lastPage = $( '<li></li>' );
                    if ( page == total || total == 0 ) {
                        $lastPage.addClass( 'disabled' );
                        $( '<span>&raquo;</span>' ).appendTo( $lastPage );
                    }
                    else {
                        $( '<a title="最后一页">&raquo;</a>' ).attr( {
                            'data-page' : total,
                            'href'      : '#page-' + total
                        } ).appendTo( $lastPage );
                    }
                    this.$el.append( $lastPage );
                    return this;
                },
                loadPage   : function ( event ) {

                    this.model.set( 'page', $( event.target ).data( 'page' ) );

                    event.preventDefault();
                }
            } )
        }
    }
);
