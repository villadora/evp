var express = require('express'),
    entitlement = require('entitlement'),
    app = express(),
    log = require('common').logger.createLogger('admin:console');

// provide admin console
app.set('views', module.__dirname + '/views');

app.engine('jade', require('jade').__express);

app.use(entitlement.roleAdmin);

app.get('/', function(req, res) {
    res.render('index');
});