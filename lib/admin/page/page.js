var validator = require('obj-validator'),
    moment = require('moment'),
    persist = require('persist'),
    eventValidator = require('common').eventValidator,
    sanitize = validator.sanitize;

var page = module.exports = {};

page.index = function(req, res) {

    var db = persist.create();
    db.events.query({}, 0, 10, function(err, events) {
        if (err)
            events = [];

        db.drafts.query({
            "eventObj.create.createdBy": (req.user ? req.user.uid : 'anonymouse')
        }, function(err, drafts) {
            if (err)
                drafts = [];

            events.forEach(function(evt) {
                evt.startTime = moment(evt.startTime).format('YYYY-MM-DD HH:mm:ss');
                evt.endTime = moment(evt.endTime).format('YYYY-MM-DD HH:mm:ss');
            });


            drafts.forEach(function(draft) {
                var date = draft.eventObj.create.createdOn,
                    name = draft.eventObj.name;

                draft.name = name || (res.__('Draft') + (date && (moment(date).format('YYYY-MM-DD HH:mm:ss'))));
            });

            res.render('index.jade', {
                drafts: drafts,
                recents: events
            });
        });
    });
};


page.list = function(req, res) {
    res.render('list.event.jade', {

    });
};