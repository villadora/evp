var entitlement = require('entitlement'),
    objValidator = require('obj-validator'),
    sanitize = objValidator.sanitize,
    page = require('./page'),
    admin = require('./admin'),
    events = require('./event'),
    apis = require('./apis'),
    sms = require('./sms');



function authOwner(req, res, next) {
    var aid = sanitize(req.params.aid).toInt();
    entitlement.roleOwner(aid, req, res, next, true);
}

module.exports.registerPages = function(app) {

    // help fun
    app.getAndPost = function() {
        app.get.apply(app, arguments);
        app.post.apply(app, arguments);
    };


    require('express-params').extend(app);
    app.param('did', /^[0-9a-f]{24}$/);
    app.param('aid', /^\d+$/);

    // index page
    app.get('/', page.index);

    // create events
    app.get('/list', entitlement.roleWriter, page.list);

    // app.get('/event/:aid/edit/draw', entitlement.roleWriter, authOwner, page.editeventdraw);
    // app.get('/event/:aid/results/draw', entitlement.roleReader, page.viewdrawresults);

    // new api
    app.get('/new', entitlement.roleWriter, events.createEvent);
    app.get('/draft/:did', entitlement.roleWriter, events.viewDraft);

    app.get('/event/:aid', entitlement.roleReader, function(req, res, next) {
        var aid = sanitize(req.params.aid).toInt();
        entitlement.roleOwner(aid, req, res, next);
    }, events.viewEvent);

    app.get('/event/:aid/edit', entitlement.roleWriter, authOwner, events.editBaseInfo);
    app.get('/event/:aid/edit/:editType', entitlement.roleWriter, authOwner, events.editEvent);

    app.get('/event/:aid/results/:type', entitlement.roleReader, events.viewResults);

    // sms center
    app.get('/smscenter', entitlement.roleSms, sms.center);
    app.get('/smscenter/send', entitlement.roleSms, sms.groupsend);
    app.post('/smscenter/send', entitlement.roleSms, sms.groupsendsubmit);
    app.get('/smscenter/records', entitlement.roleSms, sms.records);
    app.get('/smscenter/failed', entitlement.roleSms, sms.failed);


    app.get('/apis/test', entitlement.roleWriter, apis.apiTest);

    // administrator
    app.get('/administrator/roles', entitlement.roleAdmin, admin.roles);

};