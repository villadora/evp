var persist = require('persist'),
	objValidator = require('obj-validator'),
	moment = require('moment'),
	components = require('event-components').components,
	sanitize = objValidator.sanitize;

exports.createEvent = function(req, res) {
	res.render('create-event.jade');
};


exports.editEvent = function(req, res) {
	var editType = sanitize(req.params.editType).xss(),
		eventId = sanitize(req.params.aid).toInt();

	if (!components.hasOwnProperty(editType))
		return res.render('info.jade', {
			message: res.__("The page you visited is not exists"),
			level: 'error'
		});

	res.render('edit-event.jade', {
		eventId: eventId,
		editType: editType
	});
};

exports.editBaseInfo = function(req, res) {
	var eventId = sanitize(req.params.aid).toInt();
	res.render('edit-event.jade', {
		eventId: eventId,
		editType: 'baseInfo'
	});
};


exports.viewDraft = function(req, res) {
	var did = sanitize(req.params.did).xss();
	// get draft eventObject
	res.render('create-event.jade', {
		draftId: did
	});

};

exports.viewResults = function(req, res) {
	var eventId = sanitize(req.params.aid).toInt(),
		type = sanitize(req.params.type).xss(),
		candidateId,
		prizeId;

	if (req.param('candidateId'))
		candidateId = sanitize(req.param('candidateId')).toInt();

	if (req.param('prizeId'))
		prizeId = sanitize(req.param('prizeId')).toInt();

	res.render('view-results.jade', {
		eventId: eventId,
		type: type,
		candId: candidateId,
		prizeId: prizeId
	});
};

exports.viewEvent = function(req, res) {
	var aid = sanitize(req.params.aid).toInt();

	persist.create().events.get(aid, function(err, evt) {
		if (err) {
			return res.render('info.jade', {
				message: res.__('The page you visited is not exists'),
				level: 'error'
			});
		}

		evt.startTime = moment(evt.startTime).format('YYYY-MM-DD HH:mm:ss');
		evt.endTime = moment(evt.endTime).format('YYYY-MM-DD HH:mm:ss');

		evt.url = evt.url || '';
		evt.message = evt.message || '';

		var now = moment();
		if (now.isAfter(evt.endTime))
			evt.status = '已结束';
		else if (now.isBefore(evt.startTime) && !req.apiDebug)
			evt.status = '离线';
		else
			evt.status = '在线';

		evt.types = [];
		if (evt.components.draw) {
			evt.types.push('抽奖类活动');

			evt.components.draw.prizes.forEach(function(p) {
				if (p.type == '1') {
					p.typeText = '优惠券';
				} else if (p.type == '2')
					p.typeText = '实物';
				else
					p.typeText = '优惠券和实物';
			});
		}

		if (evt.components.comment) {
			evt.types.push('评论类活动');
			var comment = evt.components.comment;
			if (comment.limitType == 'byIP')
				evt.components.comment.limitTypeText = "用户IP限制";
			else if (comment.limitType == 'byUser')
				evt.components.comment.limitTypeText = "用户ID限制";
			else
				evt.components.comment.limitTypeText = "不限制";

			if (comment.limitPeriod == 'day')
				evt.components.comment.limitPeriodText = "每天限制";
			else
				evt.components.comment.limitPeriodText = "整期限制";
		}

		if (evt.components.vote) {
			evt.types.push('投票类活动');
		}

		res.render('view-event.jade', {
			event: evt
		});
	});
};