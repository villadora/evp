var multiparty = require('multiparty'),
    csv = require('csv'),
    async = require('async'),
    sms = require('sms-service'),
    xlsx = require('node-xlsx'),
    fs = require('fs');


module.exports.center = function(req, res) {
    res.render('smscenter.jade');
};

module.exports.groupsend = function(req, res) {
    res.render('smscenter-send.jade');
};

module.exports.records = function(req, res) {
    res.render('smscenter-records.jade');
};

module.exports.failed = function(req, res) {
    res.render('smscenter-failed.jade');
};

module.exports.groupsendsubmit = function(req, res) {
    async.waterfall([

            function(cb) {
                var sends = [];
                if (req.files && req.files.senddata && req.files.senddata.size) {
                    var path = req.files.senddata.path,
                        ext = /.+\.([^.]+)$/.exec(req.files.senddata.originalFilename)[1];
                    // what kind of forma
                    if (ext == 'csv') { // first column is mobile number, second column is content
                        csv().from.string(fs.readFileSync(path)).to.array(function(rows) {
                            rows.forEach(function(data) {
                                if (data.length < 2 || /\d+/.test(data[0])) {
                                    return cb({
                                        message: res.__('Mobile Number is not valid')
                                    });
                                }

                                sends.push({
                                    mobile: data[0],
                                    content: data[1]
                                });
                            });
                            cb(null, sends);
                        });
                    } else if (ext == 'xlsx') {
                        var obj = xlsx.parse(fs.readFileSync(path)), // parses a buffer
                            rows = obj.worksheets[0].data;
                        rows.forEach(function(data) {
                            if (data.length < 2 || /\d+/.test(data[0].value)) {
                                return cb({
                                    message: res.__('Mobile Number is not valid')
                                });
                            }
                            sends.push({
                                mobile: data[0].value,
                                content: data[1].value
                            });
                        });
                    } else {
                        cb({
                            message: res.__('Only csv, xlsx files are supported now.')
                        });
                    }
                } else if (req.body.mobileNos && req.body.content) {
                    var mobiles = req.body.mobileNos.split(',');
                    cb(null, mobiles.map(function(mobile) {
                        return {
                            mobile: mobile,
                            content: req.body.content
                        };
                    }));

                } else
                    return cb({
                        message: res.__('Please provide the fields that required')
                    });
            },
            function(sends, cb) {
                var db = require('persist').create();
                var sendMessage = function() {
                    async.parallel(sends.splice(0, 50).filter(function(data) {
                        return (/^[1-9]{1}[0-9]{7,11}$/.test(data.mobile));
                    }).map(function(data) {
                        return function(cb_sent) {
                            if (data && data.mobile && data.content) {
                                sms.send(sms.Types.EVENTS, data.mobile, {
                                    'body': data.content
                                }, function(err, code, msg) {
                                    // store to record
                                    db.smses.create({
                                        code: err ? 500 : code,
                                        err: err,
                                        msg: msg,
                                        mobileNo: data.mobile,
                                        content: data.content,
                                        userInfo: {},
                                        eventId: -1
                                    });
                                    cb_sent();
                                });
                            } else
                                cb_sent();
                        };
                    }), function(err) {
                        if (sends.length) {
                            setTimeout(sendMessage, 30000);
                        }
                    });
                };

                sendMessage();

                cb();
            }
        ],
        function(err, rs) {
            if (err) {
                return res.render('smscenter-send.jade', {
                    error: res.__(err.message)
                });
            } else
                return res.render('smscenter-send.jade');
        });
};