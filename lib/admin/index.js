"use strict";

var express = require('express'),
    configure = require('configure'),
    validator = require('obj-validator'),
    sanitize = validator.sanitize,
    persist = require('persist'),
    app = express(),
    path = require('path'),
    ejs = require('ejs'),
    entitlement = require('entitlement'),
    configAdmin = entitlement.configAdmin,
    log = require('common').logger.createLogger('admin');

ejs.open = '{{';
ejs.close = '}}';
app.engine('ejs', ejs.renderFile);
app.set('view engine', 'ejs');


app.engine('jade', require('jade').__express);

// set view folder
app.set('views', __dirname + '/views');


app.locals.pretty = (configure.template ? (configure.template.pretty || false) : false);

app.use(express.session({
    secret: "event-admin"
}));

// configure

configure.auth.loginTmpl = "login.jade";

var entitled = configAdmin(app, configure.auth);
// router is already added in entitlement
app.use(function(req, res, next) {
    res.locals.user = req.user;

    if (req.user)
        res.locals.user.isAdmin = !! (req.user.role & entitlement.Roles.ADMIN);

    res.locals.__ = res.__;
    next();
});

// move router to the end
for (var i = 0; i < app.stack.length; ++i) {
    if (app.stack[i].handle === app.router) {
        app.stack.splice(i, 1);
        break;
    }
}
app.use(app.router);

// log mounted apps
app.on('mount', function(parent) {
    log.debug(this.path() + ' is mounted to ' + parent.path());
});


// static files service
app.use('/static', express.static(path.join(__dirname, 'static')));


// ajax service
var ajaxsApi = require('./ajaxs');

// TODO: for compatiable for the old
app.use('/ajax/event/', function(req, res, next) {
    // if (!app.get('avoid-rewrite')) return next();
    // modify res, so the http status is not send but with a code object instead
    // to avoid the nginx rewrite/redirect for specific http status code
    var oSend = res.send,
        nested = 0;

    res.send = function(body) {
        nested++;
        if (nested === 1 && res.get('Content-Type') === undefined && (typeof body === 'object' || (typeof body === 'string' && this.statusCode != 200))) {
            // res.send will invoke res.json, which recursively invoke res.send
            var resp = {};
            resp.code = this.statusCode;
            this.statusCode = 200;
            if (typeof body === 'string')
                resp.msg = {
                    message: body
                };
            else
                resp.msg = body;

            oSend.call(this, resp);
        } else {
            oSend.apply(this, arguments);
        }
        nested--;
    };

    next();
});

// old api
app.use('/ajax/event', entitlement.roleReader);
app.use('/ajax/event', ajaxsApi.event);


app.use('/ajax/events', entitlement.roleReader);
app.use('/ajax/events', ajaxsApi.event);
app.use('/ajax/sms', entitlement.roleSms);
app.use('/ajax/sms', ajaxsApi.sms);
app.use('/ajax/admin', entitlement.roleAdmin);
app.use('/ajax/admin', ajaxsApi.admin);


// TODO: console service
app.use('/console', entitlement.roleAdmin);
app.use('/console', require('./console'));


// 404 handle
app.use(function(req, res, next) {
    res.render('info.jade', {
        message: 'The page you visited is not exists',
        level: 'error'
    });
});



// ======================
// registry web pages
// ======================
var page = require('./page');

page.registerPages(app);


module.exports = app;