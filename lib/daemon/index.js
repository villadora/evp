"use strict";


var scheduler = module.exports.scheduler = require('./scheduler'),
    tasks = {
        scheduler: scheduler
    },
    running = false;


module.exports.isStarted = function() {
    return running;
};


module.exports.start = function(options) {
    this.options = options || {};

    if (running) return;

    for (var key in tasks) {
        var task = tasks[key];
        if (!task.start)
            throw new Error('The task is not a daemon task');
        task.start(this.options[key]);
    }
    running = true;
};

module.exports.stop = function() {
    if (!running) return;

    for (var key in tasks) {
        var task = tasks[key];
        if (!task.stop)
            throw new Error('The task is unstoppable');
        task.stop();
    }
    running = false;
};

module.exports.restart = function() {
    this.stop();
    this.start(this.options);
};