"use strict";

var moment = require('moment'),
    persist = require('persist'),
    ajax = require('../ajax'),
    log = require('common').logger.createLogger('daemon:scheduler');


function online(event) {
    var id = event._id,
        hook = ajax.findHook(id);
    log.info({
        eventId: id,
        event: event
    }, 'try to online event [' + id + ']');
    if (!hook) {
        hook = ajax.hook(id, event);
    }
}

function offline(event) {
    if (typeof event === 'number')
        event = {
            _id: event
        };

    log.info({
        eventId: event._id,
        event: event
    }, 'try to offline event [' + event._id + ']');

    // remove debug data when offline
    persist.create().results_debug.remove({
        eventId: event._id
    });

    ajax.unhook(event._id);
}

/**
 * Make sure that one invocation will happen in the midnight: 12:00 am
 * @param {number} rate
 */

function getNexttime(rate) {
    var next = (moment().add('days', 1).startOf('day').valueOf() - Date.now()) % rate;
    if (next < 1000)
        next = rate;
    return next;
}
/**
 *
 */
var scheduler = module.exports = {
    options: {}
};

// package private variables
var eventJobs = {}, // format: id => { online: job, offline: job }
    onOffJob;



/**
 * Pull from database to get events that are ready for online/offline
 */


function pullOffline(retry) {
    retry = retry || 0;
    // get activities will occurs in 2 days after, and schedule the online offline job
    var now = moment().startOf("minute"),
        end = now.clone().add(scheduler.options.pullRange || {
            'days': 2
        });


    // add offline events
    persist.create().events.query({
        endTime: {
            $gte: new Date(now.format()),
            $lte: new Date(end.format())
        }
    }, function(err, events) {
        if (err) {
            if (retry--)
                setTimeout(function() {
                    pullOffline(retry);
                }, (scheduler.options.retryTime || 5 * 60) * 1000);

            return log.error({
                err: err
            });
        }

        events.forEach(function(event) {
            var id = event._id,
                job = eventJobs[id];

            job && clearTimeout(job);
            eventJobs[id] = setTimeout(function() {
                offline(event);
                delete eventJobs[id];
            }, event.endTime.getTime() - Date.now());
        });
    });
}


scheduler.start = function(options) {
    var self = this;
    this.options = options || {};

    if (!onOffJob) {
        var rate = (this.options.onOffRate || 24 * 60 * 60) * 1000,
            onOffTimer = function() {
                pullOffline(scheduler.options.retry || 5);
                onOffJob = setTimeout(onOffTimer, getNexttime(rate));
            };

        onOffJob = setTimeout(onOffTimer, getNexttime(rate)); // first time to execute
    }

    persist.create().events.query({
        'endTime': {
            $gte: new Date()
        }
    }, function(err, events) {
        if (err) return;

        events.forEach(function(event) {
            log.info('hook event %s on starting...', event._id);
            online(event);
        });
    });

};

scheduler.stop = function() {
    for (var id in eventJobs) {
        if (eventJobs.hasOwnProperty(id)) {
            var job = eventJobs[id];
            job && clearTimeout(job);
        }
    }

    onOffJob && (clearTimeout(onOffJob), onOffJob = null);
};

scheduler.restart = function() {
    this.stop();
    this.start(this.options);
};