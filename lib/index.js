var http = require('http');

var ServerType = {
    ADMIN: 1,
    AJAX: 2,
    BOTH: 3
};

module.exports.server = function(config, callback, serverType) {
    serverType = serverType || ServerType.BOTH;
    var configure = require('configure');

    if (arguments.length === 1 && typeof config === "function") {
        callback = config;
        config = undefined;
    }

    // load global config first
    configure.init(config, function(err) {
        if (err) return callback(err);

        var express = require('express'),
            path = require('path'),
            bunyanLogger = require('express-bunyan-logger'),
            logger = require('common').logger,
            daemon = require('./daemon'),
            app = express();

        app.set('name', configure.app);

        // set middlewares
        app.use(bunyanLogger(configure.bunyanOptions)); // init logger

        app.use(require('common').i18n.init); // initialize i18n
        app.use(express.bodyParser());
        // app.use(express.json());
        // app.use(express.urlencoded());
        app.use(express.methodOverride());
        app.use(express.cookieParser());


        // mock data injection
        if (config.test) {
            app.set('test.mockUser', config.test.mockUser);
        }

        app.use(function(req, res, next) {
            if (!app.get('avoid-rewrite')) return next();
            // modify res, so the http status is not send but with a code object instead
            // to avoid the nginx rewrite/redirect for specific http status code
            var oSend = res.send,
                nested = 0;

            res.send = function(body) {
                nested++;
                if (nested === 1 && res.get('Content-Type') === undefined && (typeof body === 'object' || (typeof body === 'string' && this.statusCode != 200))) {
                    // res.send will invoke res.json, which recursively invoke res.send
                    var resp = {};
                    resp.code = this.statusCode;
                    this.statusCode = 200;
                    if (typeof body === 'string')
                        resp.msg = {
                            message: body
                        };
                    else
                        resp.msg = body;

                    oSend.call(this, resp);
                } else {
                    oSend.apply(this, arguments);
                }
                nested--;
            };

            next();
        });


        app.use(app.router); // use app router

        if (serverType & ServerType.ADMIN)
            app.use('/activity/admin', require('./admin'));

        if (serverType & ServerType.AJAX)
            app.use('/activity/ajax', require('./ajax'));

        // allow events staging areas
        if (config.staging)
            app.use('/activity/events', require('./staging'));

        app.get('/activity', function(req, res) {
            // index
            res.send(res.__('OK'));
        });

        // 404 handle
        app.use(function(req, res, next) {
            res.status(404).send();
        });

        // replace listen
        var log = logger.createLogger("server"),
            oldHandle = app.handle;


        app.handle = function() {
            oldHandle.apply(this, arguments);
        };

        // err logger
        app.use(require('common').onerror);

        var server = http.createServer(app),
            oldListen = server.listen;

        server.listen = function() {
            daemon.start(configure.daemon);
            log.info(app.get('name') + ' is listening to port ' + (arguments[0] || 8010) + '...');
            oldListen.apply(this, arguments);
        };

        server.set = function() {
            app.set.apply(app, arguments);
        };

        callback(null, server);
    });
};