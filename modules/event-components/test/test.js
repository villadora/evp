var assert = require('chai').assert,
    async = require('async'),
    components = require('../lib');

describe('event-components', function() {
    var cObj = {
        "draw": {
            "limitType": "byUser",
            "limitPeriod": "event",
            "limitNumber": "0",
            "limitWins": "1",
            "estimatedParticipants": "1333",
            "prizes": [{
                "name": "fadsfads",
                "level": "2",
                "type": 1,
                "hardRate": 1,
                "total": "1",
                "message": "fadsf",
                "selected": false,
                "sent": 0
            }]
        }
    };

    it('verify', function() {
        components.verifyConfigs(cObj);
        cObj = components.parseConfigs(cObj);
        console.log(JSON.stringify(cObj));
    });
});