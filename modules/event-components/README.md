# 活动后台Ajax接口文档
======================

## 抽奖
------------------

##### 用户抽奖(用户必须登录)

* Path: _'/activity/ajax/#{eventId}/draw'_
* Method: _'GET'_

```
// jquery
$.ajax('/activity/ajax/1/draw').done(function(data) {
    data.chanceNum; // 如果有针对userId的抽奖限制,则返回剩余抽奖记录
    data.recordId; // 新建记录的id, recordId == -1 未中奖
    data.prize; // 奖品信息, id == -1未中奖
    data.prize.id; //奖品id
    data.prize.rank; data.prize.level; //奖品等级
    data.prize.name; // 奖品名称
}).fail(function(xhr) {
    switch(xhr.status) {
        case 500: // 系统错误
        case 401: // 未登录或者无权限
        case 403: // 抽检机会用完
        case 400: // 请求错误
    }
});
```

##### 用户信息
* Path: _'/activity/ajax/#{eventId}/draw/userInfo'_
* Method: _'POST'_

```
// jquery
$.ajax({
    url:'/activity/ajax/1/draw/userInfo',
    type: 'POST',
    data: {
        mobileNo: '用户手机号',
        recordId: '之前返回的记录ID',
        userName: '用户姓名',
        address: '用户地址，实物奖品需要',
        memo: '其他备注'
    }
}).done(function(data) {
    // 服务正常返回
}).fail(function(xhr) {
    switch(xhr.status) {  
	   	case 500: // 系统错误
	   	case 401: // 未登录
	   	case 400: // 请求错误
    }
});
```


## 投票
-------------------

##### 查看投票列表

* Path: _'/activity/ajax/#{eventId}/vote/votelist'_
* Method: _'GET'_


```
// jquery
$.ajax('/activity/ajax/1/vote/votelist').done(function(data) {
    // 返回投票候选对璇列表
    data[0] = {
    	id: // 投票候选对象id
    	name: // 投票候选对象名称
    	desc: // 投票候选对象描述
    	vote: // 投票计数, number
    };
}).fail(function(xhr) {  
    // 访问出错
});
```


##### 投票
* Path: _'/activity/ajax/#{eventId}/vote/voteFor/#{candidateId}'_
* Method: _'GET'_

```
// jquery
$.ajax('/activity/ajax/1/vote/voteFor/0').done(function(data) {
	data.userId; // 投票用户ID
	data.candidate = {
    	id: // 投票候选对象id
    	name: // 投票候选对象名称
    	desc: // 投票候选对象描述
    	vote: // 投票计数, number
    };
}).fail(function(xhr) {
	switch(xhr.status) {  
	    case 500: // 系统错误
	    case 404: // 没有该候选对象
	    case 403: // 已经没有投票机会
	    case 401: // 未登录或没有权限
	    
	}
});

```


## 评论
--------------------


##### 添加评论

* Path: _'/activity/ajax/#{eventId}/comment/'_
* Method: _'GET'_ or _'POST'_

```
// jquery 
$.ajax({
	url: '/activity/ajax/1/comment',
	type: 'POST',
	data: {
		content: // 评论内容
		pic: // 附带的图片路径
	}
}).done(function(data) {
	data.userId; // 用户Id
	data.remainTimes; // 剩余评论次数
	data.userInfo.face; // 用户头像
	data.userInfo.nickName; // 用户昵称
	data.likes; // 评论点赞数
	data.comment.content; // 评论内容
	data.comment.pic; // 评论附加图片
}).fail(function(xhr) {
	switch(xhr.status) {
		case 500: // 系统错误
		case 403: // 该用户评论受限制
	}
});
```

##### 获取分页评论

* Path: _'/activity/ajax/#{eventId}/comment/page/#{pageNo}'_
* Method: _'GET'_

```
// jquery
$.ajax('/activity/ajax/1/comment/page/1?limit=40').done(function(data) {
    data.total; // 总页数
    data.list; // 当前页面数据
	data.list[0]._id; // 记录ID, recordId
    data.list[0].userId; // 用户Id
	data.list[0].remainTimes; // 剩余评论次数
	data.list[0].userInfo.face; // 用户头像
	data.list[0].userInfo.nickName; // 用户昵称
	data.list[0].likes; // 评论点赞数
	data.list[0].comment.content; // 评论内容
	data.list[0].comment.pic; // 评论附加图片
}).fail(function(xhr) {  
	// 访问出错
});
```


##### 评论点赞

* Path: _'/acitivity/ajax/#{eventId}/comment/likes/#{recordId}'_
* Method: _'GET'_

```
// jquery
$.ajax('/activity/ajax/1/comment/likes/53ad34f344fd324f4fadcc44').done(function(data) {
	data._id; // 记录ID, recordId
    data.userId; // 用户Id
	data.remainTimes; // 剩余评论次数
	data.userInfo.face; // 用户头像
	data.userInfo.nickName; // 用户昵称
	data.likes; // 评论点赞数
	data.comment.content; // 评论内容
	data.comment.pic; // 评论附加图片
}).fail(function(xhr) {
    case 500: // 系统错误
    case 403: // 该用户已经在该评论上点过赞
    case 400: // 该评论记录不存在
});
```

