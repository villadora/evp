"use strict";


var log = require('./logger').createLogger('components'),
    components = {
        'draw': require('./draw'),
        'vote': require('./vote'),
        'comment': require('./comment')
    };

module.exports.components = components;

module.exports.setLogger = function(logger) {
    require('./logger').logger = logger;
    log = require('./logger').createLogger('components');
};

module.exports.verifyConfigs = function(configs) {
    var valid = true;
    Object.keys(configs).map(function(name) {
        if (valid) {
            if (!components[name]) {
                valid = false;
            } else
                valid = (valid && components[name].verifyConfig(configs[name]));

            if (!valid) {
                log.error({
                    phase: name,
                    config: configs[name]
                }, "Verify '" + name + "' config failed");
                module.exports.lastError = {
                    component: name,
                    message: "Verify '" + name + "' config failed"
                };
            }
        }
    });

    return valid;
};

module.exports.lastError = null;

module.exports.parseConfigs = function(configs) {
    var r = {};

    Object.keys(configs).map(function(name) {
        try {
            if (components[name])
                r[name] = components[name].parseConfig(configs[name]);
        } catch (e) {
            log.error({
                err: e
            }, e.message);
        }
    });

    return r;
};

module.exports.generate = function(app, event) {
    var comps = event.components;
    log.debug('generate hook for event', JSON.stringify(event));

    Object.keys(event.components).forEach(function(type) {
        if (components.hasOwnProperty(type))
            components[type].generate(app, comps[type], event);
        else
            log.error('Event Type: ' + type + ' is not supported!');
    });

    // move router to the end
    for (var i = 0; i < app.stack.length; ++i) {
        if (app.stack[i].handle === app.router) {
            app.stack.splice(i, 1);
            break;
        }
    }

    app.use(app.router);
};