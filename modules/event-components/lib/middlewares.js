var userService = require('user-service').service();

module.exports.mustlogin = function(req, res, next) {
    // logged is mandatory
    if (req.dper && req.dper.userId) {
        return userService.loadUser(req.dper.userId, function(err, userInfo) {
            if (!err && userInfo)
                req.dper.userName = userInfo.nickName;

            return next();
        });
    }

    return res.status(401).send(res.__('You must login first'));
};