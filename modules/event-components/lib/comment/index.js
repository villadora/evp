var assert = require('assert'),
    _ = require('underscore'),
    validator = require('obj-validator'),
    Checker = validator.Checker,
    sanitize = validator.sanitize,
    async = require('async'),
    check = validator.check,
    util = require('./util'),
    express = require('express'),
    params = require('express-params'),
    mw = require('../middlewares'),
    userService = require('user-service').service(),
    log = require('../logger').createLogger('vote');

exports = module.exports = {
    // generating ajax api hook
    generate: function(app, config, event) {
        var eventId = event._id,
            commentApp = express();

        // add params support
        params.extend(commentApp);

        commentApp.param('page', /^\d+$/);
        commentApp.param('rid', /^[a-z0-9A-Z]{24}$/);
        app.use('/comment', commentApp);

        commentApp.get('/', mw.mustlogin, comment);
        commentApp.post('/', mw.mustlogin, comment);

        commentApp.get('/likes/:rid', mw.mustlogin, function(req, res) {
            var debug = req.apiDebug,
                config = req.event.components.comment,
                db = req.conn,
                results = debug ? db.results_debug : db.results,
                rid = sanitize(req.params.rid).xss(),
                userId = req.dper.userId,
                cityId = req.info.cityId,
                ip = req.ip;


            results.get(eventId, rid, function(err, rec) {
                if (err) return res.status(500).send(res.__(err.message));
                if (!rec) return res.status(400).send(res.__('The record does not exist'));

                rec.likeUsers = rec.likeUsers || {};
                if (rec.likeUsers.hasOwnProperty(userId)) {
                    return res.status(403).send(res.__('You had already liked this comment'));
                }

                rec.likeUsers[userId] = true;
                results.modify(eventId, rid, {
                    $inc: {
                        likes: 1
                    },
                    $set: {
                        likeUsers: rec.likeUsers
                    }
                }, function(err, updated) {
                    if (err) return res.status(500).send(res.__(err.message));
                    delete updated.userIp;
                    delete updated.likeUsers;
                    res.send(updated);
                    log.info({
                        userId: userId,
                        userIp: ip,
                        cityId: cityId,
                        like: {
                            comment: rec.comment
                        }
                    }, 'User ' + userId + ' likes ' + rid);
                });
            });

        });

        commentApp.get('/page/:page', function(req, res) {
            var debug = req.apiDebug,
                config = req.event.components.comment,
                db = req.conn,
                results = debug ? db.results_debug : db.results,
                pn = sanitize(req.params.page).toInt(),
                limit = sanitize(req.param('limit')).toInt() || 20,
                cityId = req.info.cityId,
                ip = req.ip;

            var query = {
                type: 'comment'
            };

            results.numOf(eventId, query, function(err, num) {
                if (err) return res.status(500).send(res.__(err.message));
                results.paginate(eventId, query, pn - 1, limit, function(err, recs) {
                    if (err) return res.status(500).send(res.__(err.message));

                    // delete meta data
                    recs.forEach(function(rec) {
                        delete rec.likeUsers;
                        delete rec.userIp;
                    });


                    res.send({
                        total: Math.ceil(num / limit),
                        list: recs
                    });
                });
            });
        });

        function comment(req, res) {
            var debug = req.apiDebug,
                config = req.event.components.comment,
                comment = {},
                db = req.conn,
                results = debug ? db.results_debug : db.results,
                userId = req.dper.userId,
                cityId = req.info.cityId,
                ip = req.ip;

            ['content', 'pic'].forEach(function(prop) {
                    var value = sanitize(req.param(prop)).xss();
                    if (value) {
                        comment[prop] = value;
                    }
                });

            // comment.likes = 0;

            async.waterfall([

                    function(cb) {

                        util.limit(db, eventId, userId, ip, config, function(err, num) {
                            if (num) {
                                cb(err, num);
                            } else {
                                res.status(403).send(res.__('You can not comment on this ' + config.limitPeriod));
                                cb('on comment limitation', num);
                            }
                        });
                    },
                    function(num, cb) {
                        // load user info
                        userService.loadUser(userId, function(err, user) {
                            cb(err, user, num);
                        });
                    },
                    function(user, num, cb) {

                        results.create(eventId, {
                            userId: userId,
                            userIp: ip,
                            userInfo: user,
                            cityId: cityId,
                            type: 'comment',
                            likes: 0,
                            comment: comment
                        }, function(err, doc) {
                            if (err) {
                                res.status(500).send(res.__(err.message));
                                return cb(err);
                            }

                            delete doc[0].userIp;
                            doc[0].remainTimes = num - 1;
                            res.send(doc[0]);
                            cb();
                        });
                    }
                ],
                function(err) {
                    var logFn = err ? log.error : log.info;
                    logFn.call(log, {
                        err: err,
                        userId: userId,
                        userIp: ip,
                        cityId: cityId,
                        comment: comment
                    });
                });

            // new comment
            // meta-data:  text, userInfo, createdOn, deleted(审核)
        }

    },
    // verify comment config object
    verifyConfig: function(config) {
        if (config.hasOwnProperty('limitType')) {
            // if has limit conditions
            if (config.limitType && config.limitPeriod &&
                (config.limitType === 'byUser' || config.limitType === 'byIP') && (config.limitPeriod === 'day' || config.limitPeriod === 'event')) {

                try {
                    if ('limitNumber' in config) {
                        check(config.limitNumber).isInt();
                    }
                } catch (e) {
                    return false;
                }
            } else if (config.limitType && config.limitType !== 'unlimited')
                return false;
        }

        try {
            if (config.estimatedParticipants)
                check(config.estimatedParticipants).isInt();
        } catch (e) {
            return false;
        }

        return true;
    },
    // convert comment config object
    parseConfig: function(config) {
        var rs = {
            estimatedParticipants: 0
        };

        assert(this.verifyConfig(config), 'config object is not valid');

        if (config.hasOwnProperty('limitType')) {

            ['limitType', 'limitPeriod'].forEach(function(prop) {
                rs[prop] = sanitize(config[prop]).xss();
            });


            ['limitNumber'].forEach(function(prop) {
                rs[prop] = sanitize(config[prop]).toInt() || 0;
            });
        } else {
            rs.limitType = 'unlimited';
        }

        if (config.estimatedParticipants)
            rs.estimatedParticipants = sanitize(config.estimatedParticipants).toInt();

        return rs;
    }
};