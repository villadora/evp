"use strict";

var async = require('async'),
    moment = require('moment');


module.exports = {
    limit: function(db, eventId, userId, userIp, config, callback) {
        if (config.limitType === 'unlimited' || config.limitNumber === 0)
            return callback(null, 2);

        var countQ = {
            type: 'comment'
        };

        if (config.limitType == 'byUser')
            countQ.userId = userId;
        else if (config.limitType == 'byIP') {
            countQ.userIp = userIp;
        } else {
            return callback(null, 2);
        }

        if (config.limitPeriod == 'day') {
            countQ.create = {
                $gte: new Date(moment().startOf('day').format()),
                $lt: new Date(moment().endOf('day').format())
            };
        }

        db.results.numOf(eventId, countQ, function(err, num) {
            if (err) return callback(err);
            callback(null, config.limitNumber - num);
        });
    }
};