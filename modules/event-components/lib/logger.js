module.exports.logger = require('bunyan').createLogger({
    name: 'event-components'
});

module.exports.createLogger = function(name) {
    return this.logger.child({
        widget_type: 'event-components:' + name
    });
};