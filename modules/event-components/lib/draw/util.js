"use strict";

var async = require('async'),
    moment = require('moment');

module.exports = {
    chanceNum: function(db, id, userId, userIp, config, callback) {
        /* jshint eqeqeq: false */
        if (config.limitType === 'unlimited' || config.limitNumber == 0)
            return callback(null, 2);

        var countQ = {
            type: 'draw'
        };

        if (config.limitType == 'byUser')
            countQ.userId = userId;
        else {
            // check ip in could win
            callback(null, 2);
        }

        if (config.limitPeriod == 'day') {
            countQ.create = {
                $gte: new Date(moment().startOf('day').format()),
                $lt: new Date(moment().endOf('day').format())
            };
        }


        db.results.numOf(id, countQ, function(err, num) {
            if (err) return callback(err);
            callback(null, config.limitNumber - num);
        });
    },
    couldWin: function(db, id, userId, userIp, config, callback) {
        async.waterfall([

            function(cb) {
                if (config.limitType === 'byIp') {
                    db.results.numOf(id, {
                        userIp: userIp,
                        type: 'draw'
                    }, function(err, num) {
                        if (err) return cb(err);
                        cb(null, num < config.limitNumber);
                    });
                } else
                    cb(null, true);
            },
            function(could, cb) {
                if (!could) return cb(null, could);

                if (config.limitWins === 0)
                    return cb(null, true);

                db.results.numOf(id, {
                    'userId': userId,
                    'type': 'draw',
                    'prize': {
                        $ne: -1 // no win any prize
                    }
                }, function(err, num) {
                    if (err) return callback(err);
                    cb(null, num < config.limitWins);
                });
            }
        ], function(err, could) {
            callback(err, could);
        });
    }
};