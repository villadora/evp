var assert = require('assert'),
    _ = require('underscore'),
    validator = require('obj-validator'),
    Checker = validator.Checker,
    sanitize = validator.sanitize,
    async = require('async'),
    express = require('express'),
    check = validator.check,
    mw = require('../middlewares'),
    util = require('./util'),
    log = require('../logger').createLogger('draw');

var PrizeType = {
    SMS_GOODS: 1,
    GOODS: 2,
    BOTH: 3
};

// check whether prize object is valid
var prizeCheck = Checker({
    'name': Checker.notEmpty(),
    'level': Checker.isInt(),
    'drawn': Checker.isInt().isOptional(),
    'total': Checker.isInt(),
    'hardRate': Checker.isFloat(),
    'type': Checker.isAlphanumeric()
}).error(function(msg, key, err) {
    log.error({
        err: err,
        key: key
    }, msg);
    throw err;
});

exports = module.exports = {
    // generating ajax api hook
    generate: function(app, config, event) {
        var eventId = event._id,
            drawApp = express();

        app.use('/draw', drawApp);

        drawApp.use('/', mw.mustlogin);

        // implement draw interface
        drawApp.get('/', drawPrize);
        drawApp.get('/userInfo', userInfo);
        drawApp.post('/userInfo', userInfo);

        function userInfo(req, res) {
            var config = req.event.components.draw,
                params = {};
            ['mobileNo', 'recordId', 'userName', 'address', 'memo'].forEach(function(prop) {
                    var value = sanitize(req.param(prop)).xss();
                    if (value) {
                        params[prop] = value;
                    }
                });


            if (!params.recordId)
                return res.status(400).send(res.__('Please provide recordId'));

            if (!/^[0-9a-zA-Z]{24}$/.test(params.recordId))
                return res.status(400).send(res.__('Please provide correct recordId'));

            var debug = req.apiDebug,
                db = req.conn,
                sms = req.sms,
                results = debug ? db.results_debug : db.results;

            results.get(eventId, params.recordId, function(err, record) {
                if (!record) return res.status(400).send(res.__('Can not find the record'));

                var prize = record.prize;
                if (prize.id != -1 && params.mobileNo) {
                    sms.send(sms.Types.EVENTS, params.mobileNo, {
                        'body': prize.message
                    }, function(err, code, msg) {
                        // store to record
                        db.smses.create({
                            code: err ? 500 : code,
                            err: err,
                            msg: msg,
                            mobileNo: params.mobileNo,
                            userInfo: _.omit(params, 'recordId'),
                            recordId: params.recordId,
                            content: prize.message,
                            eventId: eventId,
                            prize: prize
                        });
                    });
                }

                results.modify(eventId, params.recordId, {
                    userInfo: _.omit(params, 'recordId')
                }, function(err, updated) {
                    // success
                    res.send(updated.userInfo);
                });
            });
        }


        function drawPrize(req, res) {
            var debug = req.apiDebug,
                config = req.event.components.draw,
                db = req.conn,
                results = debug ? db.results_debug : db.results,
                userId = req.dper.userId,
                cityId = req.info.cityId,
                ip = req.ip;

            log.debug({
                userId: userId,
                userIp: ip
            }, 'draw event api');

            async.waterfall([

                function(cb) {
                    util.chanceNum(db, eventId, userId, ip, config, function(err, chanceNum) {
                        if (err) {
                            res.status(500).send(res.__(err.message));
                            return cb(err);
                        }

                        if (chanceNum <= 0) {
                            res.status(403).send(res.__('You can not draw anymore in this ' + config.limitPeriod));
                            return cb('no win chance');
                        }

                        cb(null, chanceNum);
                    });
                },
                function(chanceNum, cb) {
                    util.couldWin(db, eventId, userId, ip, config, function(err, could) {

                        log.info({
                            userId: userId,
                            userIp: ip,
                            cityId: cityId
                        }, userId + ' try to draw prize and ' + could ? 'may win' : 'can not win');
                        if (err) {
                            res.status(500).send(res.__(err.message));
                            return cb(err);
                        }

                        cb(null, chanceNum, could);
                    });
                },
                function(chanceNum, could, cb) {
                    if (could) {

                        var prize = {
                            id: -1,
                            rank: 0,
                            level: 0,
                            name: res.__('Thank you for attend')
                        };

                        var prizes = config.prizes,
                            baseline = 0;
                        if (config.estimatedParticipants) {
                            baseline = config.estimatedParticipants;
                            prizes.forEach(function(prize) {
                                baseline -= prize.drawn;
                            });

                        } else {
                            prizes.forEach(function(prize) {
                                // TODO: this is drawn
                                baseline += prize.hardRate * (prize.total - prize.drawn);
                            });
                        }


                        if (prizes.length) {
                            // draw prize
                            var drawNumber = Math.random() * baseline,
                                prize, bar = 0;

                            baseline--;

                            for (var i = 0, len = prizes.length; i < len; ++i) {
                                var p = prizes[i];
                                bar += p.hardRate * (p.total - p.drawn);
                                log.info({
                                    userId: userId,
                                    drawNumber: drawNumber,
                                    bar: bar,
                                    couldWin: could,
                                    prize: p
                                }, userId + ' have drawNumber: ' + drawNumber + ' bar: ' + bar + ' could win: ' + could);

                                if (drawNumber < bar) {
                                    prize = p;
                                    break;
                                }
                            }
                        }


                        log.info({
                            userId: userId,
                            userIp: ip,
                            cityId: cityId
                        }, userId + ' get prize: ' + JSON.stringify(prize));

                        cb(null, chanceNum, prize);
                    } else
                        cb(null, chanceNum, {
                            id: -1,
                            rank: 1,
                            level: 0,
                            name: res.__('Thank you for attend')
                        });
                },
                function(chanceNum, prize, cb) {
                    if (prize.id >= 0) {
                        results.create(eventId, {
                            userId: userId,
                            userIp: ip,
                            userName: req.dper ? req.dper.userName : '',
                            cityId: cityId,
                            type: 'draw',
                            prize: prize
                        }, function(err, doc) {
                            if (err) {
                                res.send({
                                    recordId: -1,
                                    prize: prize,
                                    chanceNum: chanceNum - 1
                                });
                                return cb(err);
                            }

                            var recordId = doc[0]._id;
                            // update information
                            if (!debug) {
                                db.events.update({
                                    _id: eventId,
                                    'components.draw.prizes.id': prize.id,
                                    'components.draw.prizes.name': prize.name,
                                    'components.draw.prizes.type': prize.type,
                                    'components.draw.prizes.total': prize.total,
                                    'components.draw.prizes.level': prize.level
                                }, {
                                    $inc: {
                                        'components.draw.prizes.$.drawn': 1
                                    }
                                });
                            }


                            // backwards compatability
                            prize.rank = prize.level;

                            res.send({
                                recordId: recordId,
                                prize: prize,
                                chanceNum: chanceNum - 1
                            }); // win prize
                            cb(null, prize);
                        });
                    } else {
                        // won no prize
                        results.create(eventId, {
                            userId: userId,
                            userIp: ip,
                            userName: req.dper ? req.dper.userName : '',
                            cityId: cityId,
                            type: 'draw',
                            prize: prize
                        });

                        res.send({
                            recordId: -1,
                            prize: prize,
                            chanceNum: chanceNum - 1
                        });
                        cb(null, prize);
                    }
                }
            ], function(err, prize) {

                var logFn = err ? log.error : log.info;
                logFn.call(log, {
                    err: err,
                    userId: userId,
                    userIp: ip,
                    cityId: cityId,
                    prize: prize
                });
            });
        }
    },
    // verify draw config object
    verifyConfig: function(config) {
        if (config.hasOwnProperty('limitType')) {
            // if has limit conditions
            if (config.limitType && config.limitPeriod &&
                (config.limitType === 'byUser' || config.limitType === 'byIP') && (config.limitPeriod === 'day' || config.limitPeriod === 'event')) {

                try {
                    if ('limitNumber' in config) {
                        check(config.limitNumber).isInt();
                    }

                    if ('limitWins' in config) {
                        check(config.limitWins).isInt();
                    }
                } catch (e) {
                    return false;
                }
            } else if (config.limitType && config.limitType != 'unlimited')
                return false;
        }



        try {
            if (config.estimatedParticipants)
                check(config.estimatedParticipants).isInt();
        } catch (e) {
            return false;
        }

        try {
            var prizes = config.prizes;
            if (typeof prizes == 'string') {
                try {
                    prizes = JSON.parse(prizes);
                } catch (e) {
                    return false;
                }
            }

            if (prizes && prizes.length) {
                prizes.forEach(function(prize) {
                    prizeCheck(prize);
                });
            }
        } catch (e) {
            return false;
        }

        return true;
    },
    // convert draw config object
    parseConfig: function(config) {
        var rs = {
            prizes: []
        };

        assert(this.verifyConfig(config), 'config object is not valid');

        if (config.hasOwnProperty('limitType')) {

            ['limitType', 'limitPeriod'].forEach(function(prop) {
                rs[prop] = sanitize(config[prop]).xss();
            });

            ['limitWins', 'limitNumber'].forEach(function(prop) {
                rs[prop] = sanitize(config[prop]).toInt() || 0;
            });
        } else {
            rs.limitType = 'unlimited';
        }

        if (typeof config.prizes == 'string') {
            try {
                config.prizes = JSON.parse(config.prizes);
            } catch (e) {
                log.error({
                    err: e
                }, e.message);
                config.prizes = [];
            }
        }


        if (config.estimatedParticipants)
            rs.estimatedParticipants = sanitize(config.estimatedParticipants).toInt();

        var prizeId = config.pid || 0;
        config.prizes.forEach(function(prize) {
            var p = {};
            if (prize.id)
                p.id = prize.id;
            else
                p.id = prizeId++;
            p.name = sanitize(prize.name).xss();
            p.type = sanitize(prize.type).xss();
            p.level = sanitize(prize.level).toInt();
            p.rank = p.level; // backwards-compatability
            p.total = sanitize(prize.total).toInt();
            p.drawn = prize.drawn ? sanitize(prize.drawn).toInt() : 0;
            p.hardRate = sanitize(prize.hardRate).toFloat();
            if (prize.message) {
                p.message = sanitize(prize.message).xss();
            }

            rs.prizes.push(p);
        });

        rs.pid = prizeId;
        return rs;
    }
};