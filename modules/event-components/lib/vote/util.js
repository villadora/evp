"use strict";

var async = require('async'),
    moment = require('moment');

module.exports = {
    /**
     */
    limitId: function(db, eventId, userId, userIp, config, cb) {
        if (config.idLimitType == 'unlimited' || config.idLimitNumber === 0)
            return cb(null, true);

        var countQ = {
            type: 'vote',
            userId: userId
        };

        if (config.idLimitType == 'day') {
            countQ.create = {
                $gte: new Date(moment().startOf('day').format()),
                $lt: new Date(moment().endOf('day').format())
            };
        } else if (config.idLimitType == 'event') {

        } else {
            return cb(null, true);
        }

        db.results.numOf(eventId, countQ, function(err, num) {
            if (err) return cb(err);
            cb(null, config.idLimitNumber - num > 0);
        });
    },
    limitIp: function(db, eventId, userId, userIp, config, cb) {
        if (config.ipLimitType == 'unlimited' || config.ipLimitNumber === 0) {
            return cb(null, true);
        }

        var countQ = {
            type: 'vote',
            userIp: userIp
        };

        if (config.ipLimitType == 'day') {
            countQ.create = {
                $gte: new Date(moment().startOf('day').format()),
                $lt: new Date(moment().endOf('day').format())
            };
        } else if (config.ipLimitType == 'event') {

        } else {
            return cb(null, true);
        }

        db.results.numOf(eventId, countQ, function(err, num) {
            console.log('number', num);
            if (err) return cb(err);
            cb(null, (config.ipLimitNumber - num > 0));
        });
    }
};