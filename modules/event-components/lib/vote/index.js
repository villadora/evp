"use strict";


var assert = require('assert'),
    _ = require('underscore'),
    validator = require('obj-validator'),
    Checker = validator.Checker,
    sanitize = validator.sanitize,
    async = require('async'),
    express = require('express'),
    params = require('express-params'),
    mw = require('../middlewares'),
    check = validator.check,
    util = require('./util'),
    log = require('../logger').createLogger('vote');

// check whether candidate object is valid
var voteCheck = Checker({
    'name': Checker.notEmpty(),
    'votes': Checker.isInt().isOptional()
}).error(function(msg, key, err) {
    log.error({
        err: err,
        key: key
    }, msg);
    throw err;
});


module.exports = {

    // generating ajax api hook
    generate: function(app, config, event) {
        var eventId = event._id;

        // vote
        app.get('/vote/voteFor/:vid', mw.mustlogin, function(req, res) {
            var debug = req.apiDebug,
                config = req.event.components.vote,
                db = req.conn,
                results = debug ? db.results_debug : db.results,
                userId = req.dper.userId,
                vid = sanitize(req.params.vid).toInt(),
                cityId = req.info.cityId,
                ip = req.ip;

            async.waterfall([

                    function(cb) {
                        util.limitId(db, eventId, userId, ip, config, function(err, limit) {
                            if (!limit) {
                                res.status(403).send(res.__('You account can not vote on anymore'));
                                cb('on vote limitation');
                            } else
                                cb(err);
                        });
                    },
                    function(cb) {
                        util.limitIp(db, eventId, userId, ip, config, function(err, limit) {
                            if (!limit) {
                                res.status(403).send(res.__('You have vote too many times from your ip address'));
                                cb('on vote limitation');
                            } else
                                cb(err);
                        });
                    },
                    function(cb) {
                        var candidates = config.candidates,
                            cand;

                        for (var i = 0, len = candidates.length; i < len; ++i) {
                            if (candidates[i].id == vid) {
                                cand = candidates[i];
                                break;
                            }
                        }

                        if (!cand) cand = candidates[vid];

                        if (!cand) {
                            res.status(404).send(res.__('Can not find such a candidate for id:' + vid));
                            return cb("no such a candidate for id: " + vid);
                        }

                        results.numOf(eventId, {
                            userId: userId,
                            type: 'vote',
                            vid: vid
                        }, function(err, num) {
                            if (err) {
                                res.status(500).send(res.__(err.message));
                                return cb(err);
                            }

                            // no duplicate vote?
                            if (num > 0) {
                                log.warn({
                                    num: num,
                                    userId: userId,
                                    vid: vid,
                                    candidate: cand
                                }, 'Duplicate votes from the same user');
                            }

                            results.create(eventId, {
                                userId: userId,
                                userName: req.dper ? req.dper.userName : '',
                                userIp: ip,
                                cityId: cityId,
                                type: 'vote',
                                vid: vid,
                                candidate: cand
                            }, function(err, doc) {
                                if (err) return res.status(500).send(err.__(err.message));

                                if (!debug) {
                                    db.events.update({
                                        _id: eventId,
                                        'components.vote.candidates.id': vid,
                                        'components.vote.candidates.name': cand.name,
                                        'components.vote.candidates.desc': cand.desc
                                    }, {
                                        $inc: {
                                            'components.vote.candidates.$.votes': 1
                                        }
                                    });
                                }

                                // incr votes
                                cand.votes++;

                                res.send({
                                    userId: userId,
                                    candidate: cand
                                });

                                cb();
                            });
                        });
                    }
                ],
                function(err) {
                    var logFn = err ? log.error : log.info;
                    logFn.call(log, {
                        err: err,
                        userId: userId,
                        userIp: ip,
                        cityId: cityId
                    });
                });
        });


        app.get('/vote/votes/:page', function(req, res) {
            res.send(req.event.components.vote.candidates);
        });

        app.get('/vote/votelist', function(req, res) {
            res.send(req.event.components.vote.candidates);
        });
    },
    // verify vote config object
    verifyConfig: function(config) {
        ['ip', 'id'].forEach(function(li) {
            var limitType = li + 'LimitType';
            if (config.hasOwnProperty(limitType)) {
                // if has limit conditions
                if (config[limitType] && (config[limitType] === 'day' || config[limitType] === 'event')) {
                    try {
                        if ((li + 'LimitNumber') in config) {
                            check(config[li + 'limitNumber']).isInt();
                        }
                    } catch (e) {
                        return false;
                    }
                } else if (config[limitType] && config[limitType] != 'unlimited')
                    return false;
            }
        });

        try {
            var candidates = config.candidates;
            if (typeof candidates == 'string') {
                try {
                    candidates = JSON.parse(candidates);
                } catch (e) {
                    return false;
                }
            }

            if (candidates && candidates.length) {
                candidates.forEach(function(cand) {
                    voteCheck(cand);
                });
            }
        } catch (e) {
            return false;
        }

        return true;
    },
    // convert draw config object
    parseConfig: function(config) {
        var rs = {
            candidates: []
        };

        assert(this.verifyConfig(config), 'config object is not valid');

        ['ipLimitType', 'idLimitType'].forEach(function(prop) {
            if (config.hasOwnProperty(prop)) {
                rs[prop] = sanitize(config[prop]).xss();
            } else
                rs[prop] = 'unlimited';
        });

        ['ipLimitNumber', 'idLimitNumber'].forEach(function(prop) {
            if (config.hasOwnProperty(prop))
                rs[prop] = sanitize(config[prop]).toInt() || 0;
        });

        if (typeof config.candidates == 'string') {
            try {
                config.candidates = JSON.parse(config.candidates);
            } catch (e) {
                log.error({
                    err: e
                }, e.message);
                config.candidates = [];
            }
        }

        var id = config.cid || 0;
        config.candidates.forEach(function(cand) {
            var v = {};
            if (cand.id)
                v.id = cand.id;
            else
                v.id = id++;
            v.name = sanitize(cand.name).xss();
            v.votes = sanitize(cand.votes).toInt() || 0;


            if (cand.desc) {
                v.desc = sanitize(cand.desc).xss();
            }
            rs.candidates.push(v);
        });

        rs.cid = id;

        return rs;
    }
};