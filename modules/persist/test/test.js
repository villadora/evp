var assert = require('chai').assert,
    async = require('async'),
    persist = require('../lib/');

// init
persist.init({
    "address": "mongo://localhost:27017",
    "database": "DP_Event",
    "auto_reconnect": true
});

describe('persist', function() {
    it('persist.create', function() {
        assert.isFunction(persist.create);
        var db = persist.create();
        assert(db);
        assert(db === persist.create());
        db.users.indexInformation(function(err, index) {
            assert.isNull(err);
            assert(index);
        });
    });


    it('persist.counter', function(done) {
        var counter = persist.create().counter;
        counter.remove();
        counter.nextSeq('events', function(err, seq) {
            done(err);
        });
    });


    it('persist.changes', function(done) {
        var changes = persist.create().changes;
        changes.remove();

        changes.new(0, {
            name: 'test'
        });
        changes.del(1);
        changes.upt(1, {
            name: 'test1'
        });
        setTimeout(function() {
            changes.fetch(function(err, logs) {
                assert.lengthOf(logs, 3);
                done(err);
            });
        }, 200);
    });



    it('persist.events', function(done) {
        var events = persist.create().events;
        events.remove();

        async.waterfall([

            function(cb) {
                events.create({
                    _id: 0,
                    name: 'test_event',
                    create: {
                        createOn: new Date(2013, 9, 13)
                    }
                }, function(err) {
                    cb(err, 0);
                });
            },
            function(id, cb) {
                events.get(id, function(err, event) {
                    assert.equal(event.name, 'test_event');
                    assert.equal(id, event._id);
                    cb(err, event._id);
                });
            },
            function(id, cb) {
                events.get(-1, function(err, event) {
                    assert.isNull(event);
                    cb(err, id);
                });
            },
            function(id, cb) {
                events.modify(id, {
                    _id: 'x',
                    cityId: 5
                }, function(err, updated) {
                    if (err) return cb(err);
                    assert.equal(updated.name, 'test_event');
                    assert.equal(updated._id, 0);
                    assert.equal(updated.cityId, 5);
                    cb(err, id);
                });
            },
            function(id, cb) {
                events.query({
                    _id: id
                }, function(err, results) {
                    if (err) return cb(err);
                    assert.lengthOf(results, 1);
                    cb(err, id);
                });
            },
            function(id, cb) {
                events.create([{
                    _id: 2,
                    name: 'test event2',
                    create: {
                        createOn: new Date(2011, 3, 4)
                    }
                }, {
                    _id: 3,
                    name: 'test event3',
                    create: {
                        createOn: new Date(2013, 5, 4)
                    }
                }], function(err) {
                    if (err) return cb(err);
                    events.query({}, 1, 2, function(err, rs) {
                        cb(err, id);
                    });
                });
            },
            function(id, cb) {
                events.destroy({
                    _id: id
                }, function(err, doc) {
                    cb(err);
                });
            }
        ], function(err, results) {
            events.remove();
            done(err);
        });

    });


    it('persist.events.results', function(done) {
        var results = persist.create().results;
        results.remove();

        async.waterfall([

                function(cb) {
                    results.create(0, [{
                        cnt: 1
                    }, {
                        cnt: 3
                    }, {
                        cnt: 4
                    }, {
                        cnt: 12
                    }, {
                        cnt: 43
                    }], function(err, num) {
                        setTimeout(function() {
                            if (err) return cb(err);
                            results.query(0, {}, function(err, recs) {
                                assert.lengthOf(recs, 5);
                                cb(err, 0, recs.map(function(r) {
                                    return r._id;
                                }));
                            });
                        }, 200);
                    });
                },
                function(id, rids, cb) {
                    results.destroy(id, rids.slice(0, 2), function(err, num) {
                        if (err) return cb(err);
                        results.query(0, function(err, recs) {
                            assert.lengthOf(recs, 3);
                            cb(err, id, recs.map(function(r) {
                                return r._id;
                            }));
                        });
                    });
                },
                function(id, rids, cb) {
                    results.get(id, rids[0], function(err, rec) {
                        if (err) return cb(err);
                        assert.equal(rec._id.toHexString(), rids[0]);
                        cb(err, id, rids);
                    });
                },
                function(id, rids, cb) {
                    results.paginate(id, 1, 2, function(err, recs) {
                        if (err) return cb(err);
                        assert.lengthOf(recs, 1);
                        cb(err, id, rids);
                    });
                },
                function(id, rids, cb) {
                    results.numOf(id, function(err, number) {
                        if (err) return cb(err);
                        assert.equal(number, 3);
                        cb(err, id, rids);
                    });
                },
                function(id, rids, cb) {
                    results.numOf('not exists eent', function(err, number) {
                        if (err) return cb(err);
                        assert.equal(number, 0);
                        cb(err, id, rids);
                    });
                }
            ],
            function(err, rs) {
                results.remove();
                done(err);
            });
    });


    it('persist.users', function(done) {
        var users = persist.create().users;
        users.get({
            uid: '123'
        }, function(err, user) {
            if (err) done(err);
            assert.isNull(user);
            users.addOrUpdate({
                    uid: '100',
                    provider: 'google',
                    email: 'test_account@dianping.com'
                },
                function(err, user) {
                    if (err) return done(err);
                    assert.equal(null, err);
                    assert.equal(user.uid, '100');
                    assert.equal(user.provider, 'google');
                    users.get({
                        uid: '100',
                        provider: 'google',
                        email: 'test_account@dianping.com'
                    }, function(err, user) {
                        if (err) return done(err);
                        assert.equal(user.uid, '100');
                        assert.equal(user.provider, 'google');
                        users.find().toArray(function(err, doc) {
                            assert(doc.length);
                            users.remove();
                            done(err);
                        });
                    });
                });
        });

    });


    it('persist.drafts', function(done) {
        var drafts = persist.create().drafts;
        drafts.remove();
        drafts.create({
            name: 'afdfa',
            type: 1
        }, function(err, doc) {
            if (err) return done(err);

            var id = doc._id.toString();
            drafts.get(id, function(err, item) {
                if (err) return done(err);

                drafts.numOf(function(err, num) {
                    if (err) return done(err);
                    assert.equal(num, 1);

                    drafts.modify(id, {
                        name: 'x'
                    }, function(err, updated) {
                        if (err) return done(err);
                        assert.equal(updated.name, 'x');

                        drafts.destroy({
                            _id: id
                        }, function(err, changes) {
                            if (err) return done(err);
                            assert.equal(changes, 1);
                            drafts.get(id, function(err, item) {
                                assert.isNull(item);
                                // drafts.remove();
                                done(err);
                            });
                        });
                    });
                });
            });
        });

    });

});