"use strict";

module.exports.logger = require('bunyan').createLogger({
    name: 'persist'
});

module.exports.createLogger = function(name) {
    return this.logger.child({
        widget_type: name
    });
};