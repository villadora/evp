"use strict";

var _ = require('underscore'),
    mongo = require('mongoskin'),
    log = require('./logger').createLogger('persist');

var _dbCached = {};

module.exports.init = function(options) {
    this.options = options || {};
};

module.exports.create = function(address, options) {
    if (arguments.length < 2) {
        options = address || {};
        address = null;
    }

    _.defaults(options, this.options);
    address = address || options.address;


    var key = address + JSON.stringify(options);
    // cache
    if (_dbCached.hasOwnProperty(key))
        return _dbCached[key];


    var db = mongo.db(address, {
        "database": options.database || "",
        "poolSize": options.poolSize || 5,
        "auto_reconnect": options.auto_reconnect || false,
        "w": options.writeconcern || 0,
        "journal": options.journal || false
    });


    // bind helper methods
    db.bind('counter', require('./counter'));
    db.bind('users', require('./users'));
    db.bind('events', require('./events'));
    db.bind('drafts', require('./drafts'));
    db.bind('results', require('./results'));
    db.bind('results_debug', require('./results'));
    // db.bind('changes', require('./changes'));
    db.bind('smses', require('./smses'));

    // create index for collections
    db.events.ensureIndex({
        name: 1,
        startTime: 1,
        endTime: 1
    });

    db.drafts.ensureIndex({
        _id: 1,
        "eventObj.create.createBy": 1,
        "eventObj.create.createOn": 1
    });

    db.results.ensureIndex({
        create: 1
    });

    db.users.ensureIndex({
        uid: 1,
        'email': 1
    });

    db.smses.ensureIndex({
        create: 1
    });

    _dbCached[key] = db;

    // add router
    return db;
};


module.exports.mysql = require('./mysql');