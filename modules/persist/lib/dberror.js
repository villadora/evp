"use strict";


module.exports = function(err, logger) {
    if (err && logger) logger.error({
        err: err
    });

    if (err) {
        err.status = 500;
        err.originMessage = err.message;
        err.message = 'Database operation failed';
    }

    return err;
};