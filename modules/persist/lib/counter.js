"use strict";

var _ = require('underscore'),
    dberror = require('./dberror'),
    log = require('./logger').createLogger('counter');

module.exports = {
    nextSeq: function(name, callback) {
        this.findAndModify({
            _id: name
        }, [], {
            $inc: {
                seq: 1
            }
        }, {
            upsert: true,
            'new': true
        }, function(err, counter) {
            return callback(dberror(err, log), counter && counter.seq);
        });
    }
};