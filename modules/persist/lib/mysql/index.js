"use strict";

var mysql = require('mysql'),
    url = require('url'),
    log = require('../logger').createLogger('mysql');


// create
var poolCluster;

//var connection = mysql.createConnection('mysql://user:pass@host/db?debug=true&charset=BIG5_CHINESE_CI&timezone=-0700');
module.exports.init = function(config) {
    if (poolCluster)
        poolCluster.end();

    poolCluster = mysql.createPoolCluster({
        canRetry: true,
        removeNodeErrorCount: Infinity, // Remove the node immediately when connection fails.
        defaultSelector: 'RR', // choose from RR, ORDER, RADMON
    });


    var masterConfig = config.master,
        slaveConfig = config.slave;

    // normalize and parse host/port/database from jdbc url
    [masterConfig, slaveConfig].forEach(function(dbconfig) {
        var jdbc = dbconfig.jdbc;

        if (/^jdbc\:/.test(jdbc))
            jdbc = jdbc.split('jdbc:')[1];

        var dburl = url.parse(jdbc);
        dbconfig.host = dburl.hostname;
        dbconfig.port = dburl.port;
        dbconfig.database = dburl.pathname.substr(1);
    });

    poolCluster.add('MASTER', masterConfig);
    poolCluster.add('SLAVE', slaveConfig);
};


// Target Group : ALL(anonymous, MASTER, SLAVE1-2), Selector : round-robin(default)
// poolCluster.getConnection(function(err, connection) {});

module.exports.readConnection = function(callback) {
    // only read from slave
    poolCluster.getConnection('SLAVE', function(err, connection) {
        callback(err, connection);
    });
};

module.exports.writeConnection = function(callback) {
    poolCluster.getConnection('MASTER', function(err, connection) {
        callback(err, connection);
    });
};


// default get read connection
module.exports.getConnection = module.exports.readConnection;