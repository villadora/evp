/**
 * @fileOverview this collection is used to record canges on events, better to be replace with a center pub/sub system like redis
 */
"use strict";

var dberror = require('./dberror'),
    log = require('./logger').createLogger('chagnes');

module.exports = {
    'new': function(id, event) {
        this.insert({
            id: id,
            event: event,
            timestamp: new Date(),
            action: 'new'
        });
    },
    'del': function(id) {
        if (!(id instanceof Array))
            id = [id];

        this.insert(id.map(function(i) {
            return {
                id: i,
                timestamp: new Date(),
                action: 'delete'
            };
        }));
    },
    upt: function(id, update) {
        this.insert({
            id: id,
            update: update,
            timestamp: new Date(),
            action: 'update'
        });
    },
    fetch: function(callback) {
        var self = this;
        self.findItems({}, {
            _id: 0
        }, {
            sort: [
                ['timestamp', 1]
            ],
            w: 1
        }, function(err, rs) {
            self.remove();
            callback(dberror(err, log), rs);
        });
    }
};