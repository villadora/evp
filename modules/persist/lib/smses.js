"use strict";

var _ = require('underscore'),
    dberror = require('./dberror'),
    log = require('./logger').createLogger('persist.events'),
    ObjectID = require('mongoskin').ObjectID;


module.exports = {
    create: function(sms, callback) {
        sms.create = new Date();

        if (callback)
            this.insert(sms, {
                w: 1
            }, function(err, docs) {
                callback(dberror(err, log), docs);
            });
        else
            this.insert(sms);

        return this;
    },
    get: function(id, callback) {
        if (typeof id === 'string')
            id = ObjectID.createFromHexString(id);

        this.findOne({
            _id: id
        }, function(err, item) {
            // if (item) Object.freeze(item);
            callback(dberror(err, log), item);
        });
        return this;
    },
    numOf: function(query, callback) {
        this.count(query, function(err, num) {
            callback(dberror(err, log), num);
        });
        return this;
    },
    query: function(query, pageNum, perPage, callback) {
        if (arguments.length < 4) {
            callback = arguments[arguments.length - 1];
            pageNum = perPage = 0;
        }

        var limit = perPage,
            skip = pageNum * perPage;

        this.find(query).sort({
            'create': -1
        }).limit(limit).skip(skip).toArray(function(err, results) {
            callback(dberror(err, log), results);
        });
        return this;
    },
    destroy: function(query, callback) {
        var self = this,
            db = self.skinDb;

        if (typeof query === 'string' || query instanceof ObjectID) {
            var id = query;
            if (typeof query === 'string')
                id = ObjectID.createFromHexString(query);
            self.remove({
                _id: id
            }, {
                w: 1
            }, function(err, num) {
                callback(dberror(err, log), num);
            });
        } else {
            self.findItems(query, {
                w: 1
            }, function(err, items) {
                var ids = items.map(function(item) {
                    return item._id;
                });
                self.remove({
                    _id: {
                        $in: ids
                    }
                }, {
                    w: 1
                }, function(err, num) {
                    callback(dberror(err, log), num);
                });
            });
        }
        return this;
    },
    modify: function(id, update, callback) {
        var db = this.skinDb;
        if (!callback && typeof update === 'function') {
            callback = update;
            update = id;
            id = update._id;
        }

        update.modifiedOn = new Date();

        this.findAndModify({
                _id: id
            }, [], {
                $set: _.omit(update, '_id')
            }, {
                w: 1,
                'new': true
            },
            function(err, updated) {
                callback && callback(dberror(err, log), updated);
            });

        return this;
    }
};