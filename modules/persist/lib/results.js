"use strict";

var _ = require('underscore'),
	moment = require('moment'),
	dberror = require('./dberror'),
	log = require('./logger').createLogger('results'),
	ObjectID = require('mongoskin').ObjectID;


module.exports = {
	/**
	 * Return the number of records for this events
	 * @param {number} id
	 * @param {function} callback
	 */
	numOf: function(id, query, callback) {
		if (!callback && typeof query == 'function') {
			callback = query;
			query = {};
		}

		query.eventId = id;
		this.count(query, function(err, num) {
			callback(dberror(err, log), num);
		});
		return this;
	},
	/**
	 * @param {number} id
	 * @param {Array} records
	 * @param {function} callback
	 */
	create: function(id, records, callback) {
		if (!(records instanceof Array)) records = [records];

		records.forEach(function(rec) {
			delete rec._id;
			rec.eventId = id;
			rec.create = new Date();
		});

		this.insert(records, function(err, docs) {
			if (callback) callback(dberror(err, log), docs);
		});
		return this;
	},
	destroy: function(id, query, callback) {
		if (_.isArray(query)) {
			query = {
				_id: {
					$in: query.map(function(id) {
						if (id instanceof ObjectID)
							return id;
						else
							return ObjectID.createFromHexString(id);
					})
				}
			};
		}

		query.eventId = id;
		this.remove(query, {
			w: 1
		}, function(err, num) {
			if (callback) callback(dberror(err, log), num);
		});
		return this;
	},
	get: function(id, rid, callback) {
		if (typeof rid === 'string')
			rid = ObjectID.createFromHexString(rid);

		this.findOne({
			eventId: id,
			_id: rid
		}, function(err, rec) {
			callback(dberror(err, log), rec);
		});
		return this;
	},
	query: function(id, query, callback) {
		if (!callback && typeof query === 'function') {
			callback = query;
			query = {};
		}

		query.eventId = id;
		this.find(query).sort({
			'create': -1
		}).toArray(function(err, recs) {
			recs.forEach(function(rec) {
				rec.create = moment(rec.create).format('YYYY-MM-DD HH:mm:ss');
			});

			callback(dberror(err, log), recs);
		});
		return this;
	},
	modify: function(id, rid, update, callback) {
		if (typeof rid === 'string') rid = ObjectID.createFromHexString(rid);
		this.findAndModify({
				_id: rid,
				eventId: id
			}, [], {
				$set: _.omit(update, '_id', 'eventid')
			}, {
				w: 1,
				'new': true
			},
			function(err, updated) {
				if (callback) callback(dberror(err, log), updated);
			});
		return this;
	},
	set: function(id, rid, update, callback) {
		if (!callback && typeof update === 'function' && arguments.length === 3) {
			callback = update;
			update = rid;
			rid = update._id;
		}

		if (typeof rid === 'string') rid = ObjectID.createFromHexString(rid);

		this.findAndModify({
				_id: rid,
				eventId: id
			}, [], {
				$set: _.omit(update, '_id', 'eventId')
			}, {
				w: 1,
				'new': true
			},
			function(err, updated) {
				if (callback) callback(dberror(err, log), updated);
			});
		return this;
	},
	/**
	 * @param {number} id
	 * @param {number} pageNum start with 0/zero, and can be negative
	 * @param {number} perPage
	 * @param {function} callback
	 */
	paginate: function(id, query, pageNum, limit, callback) {
		if (arguments.length < 5) {
			callback = arguments[arguments.length - 1];
			limit = pageNum;
			pageNum = query;
			query = {};
			if (arguments.length !== 4) {
				pageNum = limit = 0;
			}
		}

		query.eventId = id;
		this.find(query).sort({
			'create': -1
		}).limit(limit).skip(pageNum * limit).toArray(function(err, results) {
			results.forEach(function(rec) {
				rec.create = moment(rec.create).format('YYYY-MM-DD HH:mm:ss');
			});
			callback(dberror(err, log), results);
		});
		return this;
	}
};