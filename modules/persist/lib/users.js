"use strict";

var _ = require('underscore'),
    dberror = require('./dberror'),
    ObjectID = require('mongoskin').ObjectID,
    log = require('./logger').createLogger('users');

module.exports = {
    get: function(user, callback) {
        if (typeof user == 'string') {
            user = {
                _id: ObjectID.createFromHexString(user)
            };
        } else if (user instanceof ObjectID) {
            user = {
                _id: user
            };
        }

        this.findOne(user, function(err, u) {
            callback(dberror(err, log), u);
        });
    },
    addOrUpdate: function(user, callback) {
        this.findAndModify(user, [], {
                $set: _.omit(user, '_id')
            }, {
                upsert: true,
                'new': true
            },
            function(err, user) {
                callback && callback(dberror(err, log), user);
            });
    },
    changeRole: function(user, role, callback) {
        if (typeof user == 'string') {
            user = {
                _id: ObjectID.createFromHexString(user)
            };
        } else if (user instanceof ObjectID) {
            user = {
                _id: user
            };
        } else
            callback('Valid id must be provided');

        this.findAndModify(user, [], {
                $set: {
                    role: role
                }
            }, {
                w: 1,
                'new': true
            },
            function(err, updated) {
                callback && callback(dberror(err, log), updated);
            });
        return this;
    },
    destroy: function(user, callback) {
        if (typeof user == 'string') {
            user = {
                _id: ObjectID.createFromHexString(user)
            };
        } else if (user instanceof ObjectID) {
            user = {
                _id: user
            };
        }

        this.remove(user, {
            w: 1
        }, function(err, u) {
            callback(dberror(err, log), u);
        });
    },
    numOf: function(query, callback) {
        this.count(query, function(err, num) {
            callback(dberror(err, log), num);
        });
        return this;
    },
    query: function(query, page, perPage, callback) {
        if (arguments.length < 4) {
            callback = arguments[arguments.length - 1];
            page = perPage = 0;
        }

        var limit = perPage,
            skip = page * perPage;

        this.find(query).sort({
            'create': -1
        }).limit(limit).skip(skip).toArray(function(err, results) {
            callback(dberror(err, log), results);
        });
        return this;
    }
};