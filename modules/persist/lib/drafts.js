"use strict";

var _ = require('underscore'),
    dberror = require('./dberror'),
    log = require('./logger').createLogger('persist.events'),
    ObjectID = require('mongoskin').ObjectID;


module.exports = {
    create: function(draft, callback) {
        this.insert(draft, {
            w: 1
        }, function(err, docs) {
            /* jshint es5:true */
            callback(dberror(err, log), docs.length ? docs[0] : null);
        });
        return this;
    },
    get: function(id, callback) {
        if (typeof id === 'string')
            id = ObjectID.createFromHexString(id);

        this.findOne({
            _id: id
        }, function(err, item) {
            // if (item) Object.freeze(item);
            callback(dberror(err, log), item);
        });
        return this;
    },
    numOf: function(query, callback) {
        if (arguments.length < 2) {
            callback = query;
            query = {};
        }

        this.count(query, function(err, num) {
            callback(dberror(err, log), num);
        });
        return this;
    },
    query: function(query, pageNum, perPage, callback) {
        if (arguments.length < 4) {
            callback = arguments[arguments.length - 1];
            pageNum = perPage = 0;
        }

        if (typeof query === "string") {
            query = {
                _id: ObjectID.createFromHexString(query)
            };
        }


        if (typeof query._id === 'string')
            query._id = ObjectID.createFromHexString(query._id);

        var limit = perPage,
            skip = pageNum * perPage;

        this.find(query).sort({
            'eventObj.create.createOn': -1
        }).limit(limit).skip(skip).toArray(function(err, results) {
            callback(dberror(err, log), results);
        });
        return this;
    },
    destroy: function(query, callback) {
        var self = this;

        if (typeof query._id === 'string')
            query._id = ObjectID.createFromHexString(query._id);

        self.findItems(query, {
            w: 1
        }, function(err, items) {
            var ids = items.map(function(item) {
                return item._id;
            });
            self.remove({
                _id: {
                    $in: ids
                }
            }, {
                w: 1
            }, function(err, num) {
                callback && callback(dberror(err, log), num);
            });
        });
        return this;
    },
    modify: function(id, update, callback) {
        if (!callback && typeof update === 'function') {
            callback = update;
            update = id;
            id = update._id;
        }

        if (typeof id === 'string')
            id = ObjectID.createFromHexString(id);

        this.findAndModify({
                _id: id
            }, [], {
                $set: _.omit(update, '_id')
            }, {
                w: 1,
                'new': true
            },
            function(err, updated) {
                callback && callback(dberror(err, log), updated);
            });

        return this;
    }
};


module.exports.paginate = module.exports.query;