#!/usr/bin/env node

var persist = require('../lib'),
    async = require('async'),
    path = require('path'),
    args = process.argv,
    users, config;



if (args.length > 2)
    users = require(path.resolve(process.cwd(), args[2]));

if (args.length > 3)
    config = require(path.resolve(process.cwd(), args[3]));

// init
persist.init(config || {
    "address": "mongo://localhost:27017",
    "database": "DP_Event",
    "auto_reconnect": true
});

var db = persist.create();

async.parallel(users.map(function(user) {
    return function(cb) {
        db.users.addOrUpdate({
            uid: user.uid || 'none',
            provider: 'google',
            email: user.email,
            name: user.name || 'name',
            role: user.role || 4 // default role
        }, function(err, user) {
            cb(err, user);
        });
    };
}), function(err) {
    if (err) return console.error(err);
    console.log('done');
    process.exit(err ? 1 : 0);
});