"use strict";

module.exports = (function() {
    var defaultConfig = {
        "app": "acitivty-platform:local",
        "auth": {},
        "i18n": {
            "locales": ['en', "zh"],
            "defaultLocale": "zh",
            "directory": "./locales",
            "updateFiles": true,
            "extension": ".js"
        },
        "deamon": {
            "scheduler": {
                "retry": 5,
                "retryTime": 5 * 60,
                "pullRange": {
                    "days": 2
                },
                "pullRate": 5 * 60,
                "onOffRate": 24 * 60 * 60
            }
        },
        "services": {},
        "persist": {
            "address": "mongo://127.0.0.1:27017",
            "database": "DP_Event",
            "writeconcern": 0
        },
        "mysql": {
            "master": {
                "jdbc": "mysql://127.0.0.1:3306/dpcomm"
            },
            "slave": {
                "jdbc": "mysql://127.0.0.1:3306/dpcomm"
            }
        },
        "bunyanOptions": {
            "level": "debug"
        },
        "template": {

        }
    },
        config = {
            config: defaultConfig,
            init: function(c, cb) {
                this.config = c || defaultConfig;
                require('./initialize')(this.config, function(err) {
                    cb(err);
                });
            }
        };

    Object.keys(defaultConfig).forEach(function(key) {
        Object.defineProperty(config, key, {
            enumerable: true,
            get: function() {
                return this.config[key];
            }
        });
    });

    return config;
})();