/**
 * @fileOverview initalize packages and other components before application started
 */

var _ = require('underscore'),
    async = require('async'),
    encryption = require('common-encryption'),
    lionClient = require('lion-client'),
    persist = require('persist'),
    sms = require('sms-service'),
    log = require('common').logger.createLogger('configure');

module.exports = function(config, finish) {
    lionClient.create(function(err, client) {
        if (err) return finish(err);

        async.parallel([

            function(cb) {
                client.get('avatar-biz.Environment', function(err, env) {
                    config.lionEnv = env;
                    log.info('lionEnv: ' + env);
                    cb(err);
                });
            },
            function(cb) {
                encryption.init(client, function(err) {
                    cb(err);
                });
            },
            function(cb) {
                var lionProps = [],
                    lionTasks = {};

                parseConfig(config);

                lionProps.forEach(function(prop) {
                    lionTasks[prop] = function(cb1) {
                        client.get(prop, function(err, val) {
                            cb1(err, val);
                        });
                    };
                });

                async.parallel(lionTasks, function(err, results) {
                    setConfig(config, results);
                    cb(err);
                });

                function setConfig(obj, values) {
                    if (_.isObject(obj) && !_.isEmpty(obj) && !_.isArray(obj) && !_.isFunction(obj)) {
                        _.keys(obj).forEach(function(key) {
                            var val = obj[key],
                                newVal = obj[key];
                            if (_.isString(val)) {
                                var matches = val.match(/\$\{([\w\.\-]+)\}/g);
                                if (matches) {
                                    for (var i = 0, len = matches.length; i < len; ++i) {
                                        var prop = matches[i].substring(2, matches[i].length - 1);
                                        newVal = newVal.replace(matches[i], values[prop]);
                                    }
                                    obj[key] = newVal;
                                }
                            } else if (_.isObject(val) && !_.isEmpty(obj) && !_.isArray(obj) && !_.isFunction(obj)) {
                                setConfig(val, values);
                            }
                        });
                    }
                }

                function parseConfig(obj) {
                    if (_.isObject(obj) && !_.isEmpty(obj) && !_.isArray(obj) && !_.isFunction(obj)) {
                        _.keys(obj).forEach(function(key) {
                            var val = obj[key];
                            if (_.isString(val)) {
                                var captures = val.match(/\$\{([\w\.\-]+)\}/g);
                                if (captures) {
                                    for (var i = 0, len = captures.length; i < len; ++i) {
                                        lionProps.push(captures[i].substring(2, captures[i].length - 1));
                                    }
                                }
                            } else if (_.isObject(val) && !_.isEmpty(obj) && !_.isArray(obj) && !_.isFunction(obj)) {
                                parseConfig(val);
                            }
                        });
                    }
                }
            }

        ], function(err, results) {
            // init persist
            sms.init(config.services.sms);
            persist.init(config.persist);
            persist.mysql.init(config.mysql);
            client.close(function() {
                finish(err);
            });
        });
    });
};