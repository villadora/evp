var cacheService = require('../lib'),
    assert = require('chai').assert,
    async = require('async');



describe('cacheService', function() {
    before(function() {
        cacheService.setDefaultCache('local');
    });

    it('local-cache', function() {
        assert(cacheService.cache);
        var cache = cacheService.getCache('local');

        assert.equal(cache, cacheService.cache);
        cache.add('a', 1);
        assert.equal(cacheService.get('a'), 1);


        cacheService.mAdd({
            'a': 2,
            'b': 3
        });

        var rs = cache.mGet(['a', 'b'], 1000);
        assert.equal(rs[0], 2);
        assert.equal(rs[1], 3);

        rs = cache.mGet('a', 'b', 1000);
        assert.lengthOf(rs, 2);

        cache.remove('a');
        assert.isUndefined(cache.get('a'));
    });
});