# Lion Client for NodeJS


Environment is setting by _'NODE_LION_ENV'_ env, available value is _local_, _dev_, _alpha_, _qa_, _prerelease_, _product_.

and properties path is setting in _'NODE_LION_PTS_PATH'_, default to "/data/webapps/appenv".


## Usage

     var lion = require('lion-client');
     lion.create(function(err, client) {
          if(err)  {
              // ... handle error
              return;
          }
          
          // do stuff with client
          
     });


## API

client API


* get: 

    client.get(key, function(err, val) {
       ...
    });
    
    
* getInt
* getFloat
* getBoolean
* getByte
* reset:

    client.reset();
