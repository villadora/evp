/**
 * @param {string} category
 * @param {...} params
 */

function CacheKey(category, params) {
    this.category = category;
    this.params = Array.prototype.slice.call(arguments, 1);
}

CacheKey.prototype.getKey = function() {
    return "";
};

module.exports = CacheKey;