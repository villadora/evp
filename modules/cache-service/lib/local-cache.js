"use strict";

var _ = require('underscore'),
    events = require('events'),
    LRU = require('lru-cache'),
    CacheKey = require('./cachekey'),
    debug = require('debug')('cache-service:local-cache');


function Cache(config) {
    config = config || {};

    _.defaults(config, {
        max: 1000,
        maxAge: 24 * 60 * 60 * 1000,
        length: function(n) {
            return 1;
        }
    });

    this.cache = LRU(config);
}


_.extend(Cache.prototype, {
    add: function(key, val, expired) {
        if (key instanceof CacheKey) {
            key = key.getKey();
        }
        return this.cache.set(key, val);
    },
    mAdd: function(keyVals) {
        for (var key in keyVals) {
            if (keyVals.hasOwnProperty(key)) {
                this.cache.set(key, keyVals[key]);
            }
        }
    },
    get: function(key, timeout) {
        return this.cache.get(key);
    },
    mGet: function(keys, timeout) {
        var cache = this.cache;
        timeout = arguments[arguments.length - 1];
        if (_.isArray(keys))
            return keys.map(function(key) {
                return cache.get(key);
            });
        else {
            var keys = Array.prototype.slice.call(arguments);
            if (_.isNumber(timeout))
                keys.pop();

            return keys.map(function(key) {
                return cache.get(key);
            });
        }
    },
    remove: function(key) {
        return this.cache.del(key);
    },
    reset: function() {
        this.cache.reset();
    }
});

module.exports = Cache;