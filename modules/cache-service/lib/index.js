"use strict";

var async = require('async'),
    events = require('events'),
    EventEmitter = events.EventEmitter,
    debug = require('debug')('cache-service');

var caches = {},
    cacheOpts = {
        'local': {
            max: 2000,
            maxAge: 24 * 60 * 60 * 1000
        }
    };

module.exports.getCache = function(cacheType) {
    if (!caches[cacheType]) {
        var Cache = require('./' + cacheType + '-cache');
        caches[cacheType] = new Cache(cacheOpts[cacheType] || {});
    }

    return caches[cacheType];
};

module.exports.setDefaultCache = function(cacheType) {
    this.cache = this.getCache(cacheType);
};

module.exports.CacheKey = require('./cachekey');

// make itself as default cache
['add', 'mAdd', 'get', 'mGet', 'remove'].forEach(function(method) {
    module.exports[method] = function() {
        return this.cache[method].apply(this.cache, arguments);
    };
});



// default cache
module.exports.cache = module.exports.getCache('local');