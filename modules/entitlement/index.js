var _ = require("underscore"),
    log = require('common').logger.createLogger('auth'),
    persist = require('persist'),
    entitled = false;


var Roles = module.exports.Roles = {
    DEFAULT: 0,
    READER: 1,
    WRITER: 2,
    USER: 3,
    SMS: 4,
    ADU: 7,
    ADMIN: 8,
    SUPER: 15,
    ROOT: 31
};

module.exports.roleDesc = function(role) {
    if (role >= 31) {
        return "Root";
    } else if (role >= 8) {
        return "Admin";
    } else if (role >= 7)
        return "Advanced User";
    else if (role >= 4)
        return "SMS";
    else if (role >= 3)
        return "User";
    else if (role >= 2) {
        return "Writer";
    } else if (role >= 1)
        return "Reader";

    return "Default";
};

function roleMw(role) {
    return function(req, res, next) {
        if (req.app.get('noauth')) return next();

        if (!entitled || (req.user && (req.user.role & role)))
            next();
        else
            res.status(401).send(res.__('You have no permission to perform this action'));
    };
}



module.exports.roleSms = roleMw(Roles.SMS);
module.exports.roleDefault = roleMw(Roles.DEFAULT);
module.exports.roleReader = roleMw(Roles.READER);
module.exports.roleWriter = roleMw(Roles.WRITER);
module.exports.roleAdmin = roleMw(Roles.ADMIN);

module.exports.roleOwner = function(eventId, req, res, next, verifyOwner) {
    persist.create().events.query({
        _id: eventId
    }, function(err, events) {
        // not found this event
        if (events.length === 0)
            return res.render('info.jade', {
                message: res.__('The event %s does not exist', eventId),
                level: 'info'
            });

        var data = events[0];

        // CHeck whetehr use can change this event
        if (verifyOwner && req.user && req.user.role < Roles.ADMIN) {
            if (data.create.createdBy !== "anonymouse" && data.create.createdBy != req.user.uid) {
                return res.render('info', {
                    level: 'warning',
                    message: res.__("You have no permission to perform this action")
                });
            }
        }
        next();
    });
};


module.exports.configAdmin = function(app, opts) {
    if (opts.google) {

        require('./passport-config').config(opts.google, function(profile, done) {
            // only @dianping.com is allowed
            if (profile.hd != 'dianping.com')
                return done('Only account with @dianping.com is allowed');
            var db = persist.create();
            db.users.get({
                email: profile.email
            }, function(err, user) {
                if (err) done(err);

                if (user) {
                    if (!user.uid && !user.provider) {
                        db.users.update({
                                email: profile.email
                            }, {
                                $set: {
                                    uid: profile.uid,
                                    provider: profile.provider,
                                    name: profile.name,
                                    role: user.role || Roles.READER
                                }
                            },
                            function(err, user) {
                                log.info({
                                    err: err,
                                    user: user
                                }, 'create user');
                                done(err, user);
                            });
                    } else
                        done(err, user);
                } else {
                    db.users.addOrUpdate({
                        uid: profile.uid,
                        provider: profile.provider,
                        email: profile.email,
                        name: profile.name,
                        role: Roles.READER // default role
                    }, function(err, user) {
                        log.info({
                            err: err,
                            user: user
                        }, 'create user');
                        done(err, user);
                    });
                }
            });
        });


        var passport = require('passport');
        app.use(passport.initialize());
        app.use(passport.session());

        // make sure authencated
        app.use(function(req, res, next) {

            if (req.app.get('noauth')) return next();

            if (req.url === '/' || /^\/static\//.test(req.url) || /\/login$/.test(req.url) || /\/auth\//.test(req.url) || req.isAuthenticated()) {
                return next();
            }

            req.session.redirectUrlAfterLogin = req.url;

            res.render(opts.loginTmpl || "login", {
                message: res.__('Please login first'),
                user: null
            });
            // res.redirect((app.path() + '/login').replace(/\/\//g, '/'));
        });

        app.get('/auth/google',
            passport.authenticate('google', {
                failureRedirect: (app.path() + '/login').replace(/\/\//, '/')
            }),
            function(req, res) {
                log.info(req.user);
                if (req.session.redirectUrlAfterLogin) {
                    res.redirect(app.path() + req.session.redirectUrlAfterLogin);
                    delete req.session.redirectUrlAfterLogin;
                } else
                    res.redirect(app.path());
            });

        app.get(/\/auth\/google\/return(\??).*/, passport.authenticate('google', {
            failureRedirect: app.path() + '/login'
        }), function(req, res) {
            log.info(req.user);
            if (req.session.redirectUrlAfterLogin) {
                res.redirect(app.path() + req.session.redirectUrlAfterLogin);
                delete req.session.redirectUrlAfterLogin;
            } else
                res.redirect(app.path());
        });


        app.get('/login',
            function(req, res) {
                if (req.isAuthenticated())
                    return res.redirect((app.path()));
                console.log(opts.loginTmpl);
                res.render(opts.loginTmpl || "login");
                // res.redirect(app.path() + '/auth/google');
            });

        app.get('/logout', function(req, res) {
            req.logout();
            res.redirect(app.path());
        });

        entitled = true;
        return true;
    }
};