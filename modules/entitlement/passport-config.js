var passport = require('passport'),
    persist = require('persist'),
    GoogleStrategy = require('passport-google').Strategy;
// GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

module.exports.config = function(opts, getUser) {
    passport.serializeUser(function(user, done) {
        done(null, user.uid);
    });

    passport.deserializeUser(function(obj, done) {
        persist.create().users.findOne({
            uid: obj
        }, function(err, user) {
            // delete user._id;
            done(err, user);
        });
    });

    // use google strategy
    passport.use(new GoogleStrategy({
            returnURL: opts.callbackURL,
            realm: opts.realm
        },
        function(identifier, profile, done) {
            var up = {
                uid: identifier,
                name: profile.name.familyName + profile.name.givenName,
                email: profile.emails[0].value,
                provider: 'google',
                hd: profile.emails[0].value.split('@')[1]
            };

            if (getUser)
                getUser(up, done);
            else
                done(null, up);
        }
    ));
};