# Common Encryption

Utils for encryption in common biz

## Usage

```
var encryption = require('common-encryption');

encryption.init(key, iv);

var text = encryption.decryptDefault(encryptedText);

var encrypted = encryption.encryptDefault(text);
```