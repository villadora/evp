"use strict";

var crypto = require('crypto'),
    assert = require('assert');

function isBlank(str) {
    return !str || str.length === 0 || /^\s*$/.test(str);
}

/** @const **/
var ENCODING_KEY = "ascii",
    ENCODING_TEXT = "utf8";


var LION_DEFAULT_KEY = "encryption.key_default",
    LION_DEFAULT_IV = "encryption.iv_default",
    defaultKey = '',
    defaultIv = '';

module.exports = {
    /**
     * Initialize encryption
     *     init(key, iv)
     *     init(lionClient, finish)
     */
    init: function(lionClient, finish) {
        var self = this;
        if (typeof finish === 'function') {
            lionClient.get(LION_DEFAULT_KEY, function(err, key) {
                if (err) return finish(err);
                lionClient.get(LION_DEFAULT_IV, function(err, iv) {
                    if (err) return finish(err);
                    self.init(key, iv);
                    finish();
                });
            });

            lionClient.removeAllListeners('change:' + LION_DEFAULT_KEY);
            lionClient.removeAllListeners('change:' + LION_DEFAULT_IV);
            lionClient.on('change:' + LION_DEFAULT_KEY, function(val) {
                defaultKey = val;
            });

            lionClient.on('change:' + LION_DEFAULT_IV, function(val) {
                defaultIv = val;
            });
        } else if (arguments.length == 2) {
            defaultKey = lionClient;
            defaultIv = finish;
        } else if (arguments.length == 1 && typeof lionClient === "function") {
            finish = lionClient;
            require('lion-client').create(function(err, client) {
                if (err) return finish(err);
                self.init(client, finish);
            });
        }
    },
    /**
     * 使用AES算法，使用指定key和IV对指定字节数组进行解密
     */
    decrypt: function(bytes, key, iv) {
        var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
        decipher.setAutoPadding(false);
        var decrypted = decipher.update(bytes);
        Buffer.concat([decrypted, decipher.final()]);
        return decrypted;
    },
    decryptDefault: function(cipherText) {
        if (isBlank(cipherText))
            return '';

        assert(!isBlank(defaultKey) && !isBlank(defaultIv), 'key and iv is not initialized yet');

        var cipherBytes = parseHex2Byte(cipherText),
            decryptedBytes = this.decrypt(cipherBytes, new Buffer(defaultKey, ENCODING_KEY),
                new Buffer(defaultIv, ENCODING_KEY));

        if (decryptedBytes && decryptedBytes.length) {
            var decrypted = decryptedBytes.toString(ENCODING_TEXT);
            decrypted = decrypted.replace(/^[ \0]+|[ \0]+$/g, '').replace(/\~\$#!\^/g, ' ').replace(/\^#\~\$!/g, "\0");
            return decrypted;
        }

        return '';
    },
    encrypt: function(bytes, key, iv) {
        var cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
        cipher.setAutoPadding(false);
        var crypted = cipher.update(padWithZeros(bytes));
        Buffer.concat([crypted, cipher.final()]);
        return crypted;
    },
    encryptDefault: function(text) {
        if (isBlank(text)) return '';

        var encrypted = this.encrypt(new Buffer(text, ENCODING_TEXT), new Buffer(defaultKey, ENCODING_KEY), new Buffer(defaultIv, ENCODING_KEY));
        if (encrypted && encrypted.length) {
            return parseByte2Hex(encrypted);
        }
        return '';
    }
};

function parseByte2Hex(bytes) {
    var hex = '';
    for (var i = 0, len = bytes.length; i < len; ++i) {
        var h = (bytes[i] & 0xff).toString(16);
        if (h.length === 1)
            h = '0' + h;
        hex += h;
    }
    return hex;
}

function parseHex2Byte(hexText) {
    if (isBlank(hexText))
        return null;

    var rs = new Buffer(hexText.length / 2);
    for (var i = 0, len = hexText.length / 2; i < len; ++i) {
        var h = parseInt(hexText.substring(i * 2, i * 2 + 1), 16),
            l = parseInt(hexText.substring(i * 2 + 1, i * 2 + 2), 16);
        rs[i] = h * 16 + l;
    }
    return rs;
}

function padWithZeros(input) {
    var rest = input.length % 16,
        padd;
    if (rest > 0) {
        var padding = new Buffer(16 - rest);
        padding.fill(0);
        input = Buffer.concat([input, padding]);
    }
    return input;
}