"use strict";

var _ = require('underscore'),
    validator = require('obj-validator'),
    components = require('event-components'),
    Checker = validator.Checker,
    check = validator.check,
    sanitize = validator.sanitize,
    log = require('./logger').createLogger('common:event-validator');

// validate event object
module.exports = function(data) { // create/activity
    var obj = {};
    ['name', 'cityId', 'url', 'owner', 'startTime', 'message', 'endTime', 'components'].forEach(function(key) {
        if (data.hasOwnProperty(key)) {
            if (typeof obj[key] === 'string')
                obj[key] = sanitize(data[key]).xss();
            else
                obj[key] = data[key];
        }
    });


    if (obj.components) {
        var originComp = obj.components;

        if (typeof obj.components === 'string') {
            try {
                obj.components = JSON.parse(sanitize(obj.components).xss());
            } catch (e) {
                log.error({
                    err: e
                });
                e.status = 400, e.message = 'The property is not valid: [%s] %s', e.args = ['components', JSON.stringify(originComp)];
                throw e;
            }
        } else {
            obj.components = obj.components || {};
        }


        if (!components.verifyConfigs(obj.components)) {
            log.error('Verify failed');
            var err = new Error("The property is not valid: [%s] %s");
            err.status = 400, err.args = ['components', JSON.stringify(originComp)];
            throw err;
        }

        try {
            obj.components = components.parseConfigs(obj.components);
        } catch (e) {
            log.error('parse failed');
            log.error({
                err: e
            });
            e.message = "The property is not valid: [%s] %s";
            e.status = 400, e.args = ['components', JSON.stringfy(originComp)];
            throw e;
        }

    }

    // validate event object
    var eventValidate = Checker({
        'url': Checker.isUrl().isOptional(true),
        'cityId': Checker.isInt().isOptional(),
        'startTime': Checker.isDate().isOptional(),
        'endTime': Checker.isDate().isAfter(obj.startTime).isOptional()
    }).error(function(msg, key, err) {
        log.error({
            err: err
        });
        err.originalMessage = err.message;
        err.message = 'The property is not valid: [%s] %s';
        err.status = 400,
        err.args = [key, obj[key]];
        throw err;
    });

    eventValidate(obj);

    if ('cityId' in obj) obj.cityId = parseInt(obj.cityId, 10);
    else obj.cityId = 0;

    if ('url' in obj) {
        if (!obj.url) obj.url = "";
    }

    if (obj.startTime)(!(obj.startTime instanceof Date)) && (obj.startTime = new Date(obj.startTime));
    if (obj.endTime)(!(obj.endTime instanceof Date)) && (obj.endTime = new Date(obj.endTime));

    return obj;
};


module.exports.checkComponents = function(comps) {
    if (comps) {
        var originComp = comps;

        if (typeof comps === 'string') {
            try {
                comps = JSON.parse(comps);
            } catch (e) {
                log.error({
                    err: e
                });
                e.status = 400, e.message = 'The property is not valid: [%s] %s', e.args = ['components', originComp];
                throw e;
            }
        } else {
            comps = comps || {};
        }


        if (!components.verifyConfigs(comps)) {
            if (components.lastError) {
                var err = new Error(components.lastError.message);
                err.status = 400;
                throw err;
            } else {
                var err = new Error("The property is not valid: [%s] %s");
                err.status = 400, err.args = ['components', JSON.stringify(originComp)];
                throw err;
            }
        }

        try {
            comps = components.parseConfigs(comps);
        } catch (e) {
            log.error({
                err: e
            });
            e.message = "The property is not valid: [%s] %s";
            e.status = 400, e.args = ['components', originComp];
            throw e;
        }
        return comps;
    }
};