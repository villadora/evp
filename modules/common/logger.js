var _ = require('underscore'),
	bunyan = require('bunyan'),
	config = require('../configure');


module.exports = {
	createLogger: function(name) {
		if (!this.__logger) {
			return bunyan.createLogger({
				name: name
			});
		}

		return this.__logger.child({
			widget_type: name
		});
	}
};

var bunyanOptions = _.chain(config.bunyanOptions).clone().defaults({
	name: "activity-platform"
}).value();


if (bunyanOptions.streams) {
	bunyanOptions.streams.push({
		'level': 'info',
		'stream': process.stdout
	});
}

module.exports.__logger = bunyan.createLogger(bunyanOptions);

require('event-components').setLogger(module.exports.__logger);