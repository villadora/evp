module.exports = {
    i18n: require('./i18n'),
    lion: require('./lion'),
    logger: require('./logger'),
    onerror: require('./onerror'),
    eventValidator: require('./eventValidator')
};