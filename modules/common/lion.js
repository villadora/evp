var lion = require('lion-client'),
    cache = require('lru-cache')({
        max: 200,
        length: function(n) {
            return 1;
        },
        maxAge: 60 * 60 * 1000
    }),
    log = require('./logger').createLogger('common:lion');

module.exports = {
    get: function(key, callback) {
        var self = this;
        lion.create(function(err, client) {
            if (err) return callback(err);

            // if (cache.has(key))
            // return callback(null, cache.get(key));

            client.get(key, function(err, val) {
                if (err) {
                    log.error({
                        err: err
                    }, err.message);
                    return callback(null, null);
                }
                log.debug({
                    key: key,
                    val: val
                }, 'get key from lion');
                cache.set(key, val);
                callback(err, val);
            });
        });
    },
    getBoolean: function(key, callback) {
        var self = this;
        lion.create(function(err, client) {
            if (err) return callback(err);

            if (cache.has(key))
                return callback(null, cache.get(key));
            client.getBoolean(key, function(err, val) {
                if (err) {
                    log.error({
                        err: err
                    }, err.message);
                    return callback(null, null);
                }
                cache.set(key, val);
                callback(err, val);
            });
        });
    }
};