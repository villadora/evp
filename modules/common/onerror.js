var i18n = require('i18n'),
    log = require('./logger').createLogger('common:onerror');

module.exports = function(err, req, res, next) {
    console.error(err);
    log.error({
        err: err,
        url: req.url
    }, 'Request on error');

    var msg = err.message || err.toString();

    res.status(err.status || 500).send(res.__.apply(res, [msg].concat(err.args || [])));
};