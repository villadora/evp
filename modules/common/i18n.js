var i18n = require('i18n'),
    _ = require('underscore'),
    configure = require('../configure');

i18n.configure(_.defaults(configure.i18n, {
    "cookie": configure.app
}));

module.exports = i18n;