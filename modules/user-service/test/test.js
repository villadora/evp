var userService = require('../lib');

describe('user-service', function() {
    this.timeout(5000);
    
    var u;
    before(function() {
        u = userService.service();
    });

    it('userService.loadUser', function(done) {
        u.loadUser(4086794, function(err, user) {
            console.log(user);
            done(err);
        });
    });
});