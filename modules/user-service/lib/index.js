var util = require('util'),
    select = require('soupselect').select,
    htmlparser = require("htmlparser"),
    request = require('request'),
    cache = require('lru-cache')({
        max: 1000,
        length: function(n) {
            return 1;
        },
        maxAge: 6 * 60 * 60 * 1000 // one day
    });


var baseUrl = "http://www.dianping.com/member/jsonp/userCarte?memberId=%s&callback=userServiceCallback";

module.exports.service = function(options) {
    return {
        loadUser: function(userId, callback) {
            if (cache.has(userId))
                return callback(null, cache.get(userId));

            var url = util.format(baseUrl, userId);
            request.get(url, function(err, res, body) {
                if (err) return callback(err);
                var mc = /^userServiceCallback\((.*)\);$/.exec(body);

                if (mc.length < 2) return callback(new TypeError("http service return wrong format"));

                var rs, profileHtml;
                try {
                    rs = JSON.parse(mc[1]);
                } catch (e) {}

                if (!rs) return callback(new TypeError("Can not parse return result"));

                profileHtml = rs.msg;

                if (!profileHtml) return callback(new TypeError("Return result does not contain 'msg'"));

                var handler = new htmlparser.DefaultHandler(function(err, dom) {
                    if (err) {
                        callback(err);
                    } else {
                        try {
                            var img = select(dom, '.photo a img')[0],
                                face = img.attribs.src,
                                nickName = img.attribs.title,
                                userInfo = {
                                    face: face,
                                    nickName: nickName
                                };

                            cache.set(userId, userInfo);

                            callback(err, userInfo);
                        } catch (e) {
                            callback(e);
                        }
                    }
                });

                var parser = new htmlparser.Parser(handler);
                parser.parseComplete(profileHtml);
            });
        }
    };
};