#!/bin/bash

MONGOD_PID=`ps ax | grep "\([0-9]\+\).*\/mongod.*--config \.\/mongodb\.conf" | cut -d' ' -f1`
MONGOD_PORT=27017

echo "$MONGOD_PID"

mkdir -p ./db/mongodb

if [ ! -z "$MONGOD_PID" ]; then
    echo "kill -2 ${MONGOD_PID}"
    kill -2 $MONGOD_PID
fi

MONGOD_LOCK="./db/mongodb/mongod.lock"
if [ -e "$MONGOD_LOCK" ]; then
    rm -rf "$MONGOD_LOCK"
fi

# start mongodb
mongod --port ${MONGOD_PORT}  --config ./mongodb.conf &
