#!/bin/bash


if [ -z ${1} ]; then
    echo "Please provide packaging target directory!"
    exit 1
fi

if [ ! -d ${1} ]; then
    echo "Directory does not exists!"
    exit 1
fi



rm -rf ${1}/activity-platform/
cd ${1}

git clone git@code.dianpingoa.com:f2e/activity-platform.git || {
    exit 1
}
cd ${1}/activity-platform/
rm -rf ${1}/activity-platform/.git

npm install lion-client sms-service || {
    exit 1
}

node <<EOF
var fs = require('fs');
var package = require('./package.json');
delete package.dependencies['lion-client'];
delete package.dependencies['sms-service'];

fs.writeFileSync('./package.json', JSON.stringify(package, null, 4));

EOF


cd ${1}
tar czvf activity-platform.tar.gz activity-platform/

