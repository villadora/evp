{
	"Welcome": "Welcome",
	"OK": "OK",
	"Database operation failed": "Database operation failed",
	"You have no permission to perform this action": "You have no permission to perform this action",
	"The event %s does not exist": "The event %s does not exist",
	"The event you visited was offline": "The event you visited was offline",
	"The property is not valid: [%s] %s": "The property is not valid: [%s] %s",
	"The event you visited does not start yet": "The event you visited does not start yet",
	"Only account with @dianping.com is allowed": "Only account with @dianping.com is allowed",
	"Draft": "Draft",
	"Please login first": "Please login first"
}