{
	"Welcome": "欢迎",
	"OK": "确认",
	"Database operation failed": "服务器操作失败",
	"You have no permission to perform this action": "你没有权限进行此操作",
	"The event %s does not exist": "对不起，活动 %s 不存在",
	"The event you visited was offline": "谢谢您的关注,活动已关闭",
	"The property is not valid: [%s] %s": "活动对象属性错误: [%s] %s",
	"The event you visited does not start yet": "对不起，您访问的活动还没有开始",
	"The property is required: [%s] %s": "请提供活动对象属性: [%s] %s",
	"You must login first": "请先登陆点评帐户",
	"Thank you for attend": "感谢参与",
	"You can not draw anymore in this event": "对不起，您已经用完抽奖机会",
	"You can not draw anymore in this day": "对不起，您今天的抽奖机会已用完",
	"Please provide recordId": "请提供纪录recordId",
	"Please login first": "请先登陆帐户",
	"The page you visited is not exists": "你访问的页面不存在",
	"The event is not opened in your city": "对不起，该活动没有在您所在的城市开展",
	"Draft": "草稿",
	"Only csv files are supported now.": "目前只支持csv文件格式",
	"Invalid phone number": "手机号码格式错误",
	"Writer": "只写用户",
	"Root": "管理员",
	"Reader": "只读用户",
	"SMS": "短信用户",
	"Advanced User": "高级用户",
	"Admin": "管理员",
	"Default": "默认(无权限)",
	"User": "普通用户",
	"Can not delete your own profile": "不能删除自己的账号",
	"Can not change own roles": "不能修改自己的权限",
	"Can not delete the user with this privilege": "不能删除该用户",
	"Can not change the user's privilege": "不能修改该用户的权限",
	"You had already liked this comment": "你已经赞过该项",
	"Object #<Object> has no method 'get'": "Object #<Object> has no method 'get'",
	"You can not comment on this event": "You can not comment on this event",
	"You can not comment on this day": "You can not comment on this day",
	"You account can not vote on anymore": "You account can not vote on anymore",
	"You have vote too many times from your ip address": "You have vote too many times from your ip address"
}