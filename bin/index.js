#!/usr/bin/env node

var optimist = require('optimist'),
    fs = require('fs'),
    path = require('path'),
    platform = require('../lib'),
    env = process.env.NODE_ENV || 'development';

// parse arguments
var argv = optimist.usage('Usage $0 [-p|--port <port>]')
    .alias('p', 'port')
    .describe('running in port')
    .alias('c', 'config')
    .describe('load config file')
    .boolean('admin')
    .describe('start admin server')
    .boolean('ajax')
    .describe('start ajax interface server')
    .boolean('norewrite')
    .boolean('noauth')
    .boolean('cluster')
    .describe('start server with cluster')
    .argv;



var config = argv.config ? path.join(process.cwd(), argv.config) : ('./config.' + env);


// load config, the config should be set first
platform.server(require(config), function(err, app) {
    if (err) throw err;

    app.set('noauth', argv.noauth);
    app.set('avoid-rewrite', argv.norewrite);

    var cluster = require('cluster'),
        numCPUs = require('os').cpus().length;

    if (config.cluster) {
        if (cluster.isMaster) {
            // Fork workers.
            for (var i = 0; i < numCPUs; i++)
                cluster.fork();

            cluster.on('exit', function(worker, code, signal) {
                console.error('worker ' + worker.process.pid + ' crashed');
            });
        } else {
            app.listen(argv.port || config.port || 8010);

            app.on('close', function() {
                console.error('platform server crashed, it will be restart in 5 seconds...');
                var i = 5;
                setTimeout(function() {
                    --i;
                    console.error(i + '...');
                    if (i === 0)
                        app.listen(argv.port || config.port || 8010);
                }, 1000);
            });
        }
    } else {
        app.listen(argv.port || config.port || 8010);
    }

}, (argv.admin ? 1 : 0) + (argv.ajax ? 2 : 0));