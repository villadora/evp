module.exports = {
	"app": "activity-platform-local",
	"auth": {
		"google": {
			"realm": "http://localhost:8010/",
			"callbackURL": "http://localhost:8010/activity/admin/auth/google/return"
		}
	},
	"i18n": {
		"locales": ['en', 'zh'],
		"defaultLocale": "zh",
		"directory": "./locales",
		"updateFiles": true,
		"extension": ".js"
	},
	"deamon": {
		"scheduler": {
			"retry": 5, // 5 times
			"retryTime": 5 * 60, // 5 minutes
			"pullRange": {
				"days": 2 // gethering 2 days so leaving time to handle failures
			},
			"pullRate": 10, // 10 seconds
			"onOffRate": 6 * 60 * 60
		}
	},
	"mysql": {
		"master": {
			"jdbc": "mysql://127.0.0.1:3306/dpcomm"
		},
		"slave": {
			"jdbc": "mysql://127.0.0.1:3306/dpcomm"
		}
	},
	"persist": {
		"address": "mongo://127.0.0.1:27017",
		"database": "DP_Event",
		"auto_reconnect": true
	},
	"services": {
		"sms": "http://192.168.214.119:8080/sms-server/sms/send/json"
	},
	"bunyanOptions": {
		"level": "debug"
	},
	"template": {
		"pretty": true
	},
	"test": {
		"mockUser": 300400
	}
};