module.exports = {
    "app": "activity-platform-alpha",
    "staging": true,
    "cluster": true,
    "auth": {
        "google": {
            "realm": "http://event-platform.alpha.dp:8010/",
            "callbackURL": "http://event-platform.alpha.dp:8010/activity/admin/auth/google/return"
        }
    },
    "i18n": {
        "locales": ['en', 'zh'],
        "defaultLocale": "zh",
        "directory": "./locales",
        "updateFiles": true,
        "extension": ".js"
    },
    "deamon": {
        "scheduler": {
            "retry": 5, // 5 times
            "retryTime": 5 * 60, // 5 minutes
            "pullRange": {
                "days": 2 // gethering 2 days so leaving time to handle failures
            },
            "pullRate": 30, // 30 seconds
            "onOffRate": 6 * 60 * 60
        }
    },
    "mysql": {
        "master": {
            "jdbc": "${dp-common-service.common.master.jdbc.url}",
            "user": "${dp-common-service.common.master.jdbc.username}",
            "password": "${dp-common-service.common.master.jdbc.password}",
            "connectionLimit": 5
        },
        "slave": {
            "jdbc": "${dp-common-service.common.slave.jdbc.url}",
            "user": "${dp-common-service.common.slave.jdbc.username}",
            "password": "${dp-common-service.common.slave.jdbc.password}",
            "connectionLimit": 5
        }
    },
    "persist": {
        "address": "mongo://192.168.8.21:27017",
        "database": "${event-platform-biz.eventMongo.server.dbName}",
        "auto_reconnect": true
    },
    "services": {
        "sms": "http://192.168.214.119:8080/sms-server/sms/send/json"
    },
    "bunyanOptions": {
        "level": "debug",
        "streams": [{
            "type": 'rotating-file',
            "path": "/tmp/activity-platform/activity-platform.log",
            "period": '3d',
            "count": 30
        }]
    },
    "template": {
        "pretty": true
    },
    "test": {
        "mockUser": 300400
    }
};