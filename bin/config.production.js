module.exports = {
    "app": "activity-platform",
    "cluster": true,
    "auth": {
        "google": {
            "realm": "http://event-platform.dp/",
            "callbackURL": "http://event-platform.dp/activity/admin/auth/google/return"
        }
    },
    "i18n": {
        "locales": ['en', 'zh'],
        "defaultLocale": "zh",
        "directory": "./locales",
        "updateFiles": false,
        "extension": ".js"
    },
    "deamon": {
        "scheduler": {
            "retry": 5, // 5 times
            "retryTime": 5 * 60, // 5 minutes
            "pullRange": {
                "days": 2 // gethering 2 days so leaving time to handle failures
            },
            "pullRate": 60, // 1 minutes
            "onOffRate": 24 * 60 * 60 // 1 day
        }
    },
    "mysql": {
        "master": {
            "jdbc": "${dp-common-service.common.master.jdbc.url}",
            "user": "${dp-common-service.common.master.jdbc.username}",
            "password": "${dp-common-service.common.master.jdbc.password}",
            "connectionLimit": 5
        },
        "slave": {
            "jdbc": "${dp-common-service.common.slave.jdbc.url}",
            "user": "${dp-common-service.common.slave.jdbc.username}",
            "password": "${dp-common-service.common.slave.jdbc.password}",
            "connectionLimit": 5
        }
    },
    "persist": {
        "address": "mongo://${event-platform-biz.eventMongo.server.url}",
        "database": "${event-platform-biz.eventMongo.server.dbName}",
        "auto_reconnect": true,
        "poolSize": 8,
        "journal": true
    },
    "services": {
        "sms": "http://10.1.1.84/sms/send/json"
    },
    "bunyanOptions": {
        "level": "info",
        "streams": [{
            "type": 'rotating-file',
            "path": "/data/applogs/activity-platform/activity-platform.log",
            "period": '7d',
            "count": 50
        }]
    }
};