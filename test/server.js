require('./pre-test');
var request = require('supertest');


describe('server.js', function() {
   
    describe('server start', function() {
        it('/activity', function(done) {
            require('../lib').server(function(err, app) {
                if (err) return done(err);
                    request(app)
                    .get('/activity')
                    .expect(200)
                    .end(done);
                });
            });
        });
    });