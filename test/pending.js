module.exports = function(n, fn) {
    if (n) {
        return function(err) {
            if (err) throw err;
            --n || fn.apply(this, arguments);
        };
    }

    fn();
    return function() {};
};