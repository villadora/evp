require('./pre-test');

require('../lib/ajax').generator = function(eventObj) {
    // generate hooks
    var app = require('express')();
    app.all('/:action', function(req, res) {
        res.status(200).send({
            action: req.params.action,
            name: eventObj.name,
            debug: moment().isBefore(eventObj.startTime) || moment().isAfter(eventObj.endTime)
        });
    });
    return app;
};


var async = require('async'),
    moment = require('moment'),
    request = require('supertest'),
    i18n = require('i18n'),
    persist = require('persist'),
    assert = require('chai').assert,
    ajax = require('../lib/ajax'),
    configure = require('../lib/configure'),
    scheduler = require('../lib/daemon/scheduler'),
    daemon = require('../lib/daemon'),
    schedulerOpts = {
        "pullRange": {
            "days": 5
        },
        "retry": 0,
        "pullRate": 3,
        "onOffRate": 3
    };


require('./test-wrapper')(ajax);


describe('daemon.js', function() {
    require('./pre-test').presuit();

    beforeEach(function(done) {
        persist.create().events.remove();
        persist.create().changes.remove();
        ajax.reset();
        setTimeout(done, 200);
    });

    afterEach(function(done) {
        // watiting for data clean
        setTimeout(done, 200);
    });


    it('scheduler test', function(done) {
        this.timeout(15000);
        var hook = false,
            unhook = false;
        async.waterfall([
            function(cb) {
                persist.create().events.create({
                    _id: 0,
                    name: 'test_event',
                    startTime: new Date(moment().add('seconds', 5).format()),
                    endTime: new Date(moment().add('seconds', 8).format())
                }, function(err) {
                    cb(err);
                });
            },
            function(cb) {
                ajax.once('hooked', function(id) {
                    assert(id === 0, 'id === 0');
                    hook = true;
                });

                ajax.once('unhooked', function(id) {
                    unhook = true;
                });

                scheduler.start(schedulerOpts);
                setTimeout(cb, 9000);
            },
            function(cb) {
                assert(hook && unhook, 'hook&unhook must be performed already');
                scheduler.stop();
                cb();
            }
        ], done);
    });

    it('daemon start', function(done) {
        this.timeout(20000);
        async.waterfall([
            function(cb) {
                daemon.start({
                    scheduler: schedulerOpts
                });
                cb();
            },
            function(cb) {
                ajax.once('hooked', function(id) {
                    assert(id === 0);
                    cb();
                });
                persist.create().events.create({
                    _id: 0,
                    name: 'test_event',
                    startTime: new Date(moment().add('seconds', 5).format()),
                    endTime: new Date(moment().add('seconds', 10).format())
                }, function(err) {
                    if (err) throw err;
                });
            },
            function(cb) {
                request(ajax).get('/0/draw').expect(200)
                    .end(function(err, res) {
                        assert.equal(res.body.debug, true, 'event is on debug yet');
                        assert.equal(res.body.action, 'draw');
                        cb(err);
                    });
            },
            function(cb) {
                setTimeout(function() {
                    request(ajax).get('/0/vote').expect(200)
                        .end(function(err, res) {
                            assert.equal(res.body.debug, false, 'event now is active');
                            assert.equal(res.body.action, 'vote');
                            cb(err);
                        });
                }, 5000);
            },
            function(cb) {
                setTimeout(function() {
                    request(ajax).get('/0/vote').expect(400)
                        .end(function(err, res) {
                            assert.equal(res.text, i18n.__('The event you visited was offline'));
                            cb(err);
                        });
                }, 5000);
            },
            function(cb) {
                daemon.stop();
                cb();
            }
        ], done);
    });
});