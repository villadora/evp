require('./pre-test');

var request = require('supertest'),
    async = require('async'),
    moment = require('moment'),
    express = require('express'),
    assert = require('chai').assert,
    persist = require('persist'),
    configure = require('../lib/configure'),
    ajax = require('../lib/ajax'),
    i18n = require('i18n');


require('./test-wrapper')(ajax);

describe('ajax.generator.js', function() {
    require('./pre-test').presuit();

    var oldGen;
    beforeEach(function() {
        oldGen = ajax.generator;

        ajax.generator = function(event) {
            // generate hooks
            var app = express();
            app.get('/draw', function(req, res) {
                res.send(event);
            });

            return app;
        };
    });

    afterEach(function() {
        persist.create().events.remove();
        ajax.generator = oldGen;
        ajax.reset();
    });


    it('draw generator', function(done) {
        ajax.hook(0, {
            _id: 0,
            name: 'draw event',
            startTime: new Date(moment().startOf('day').subtract('days', 1).format()),
            endTime: new Date(moment().startOf('day').add('days', 2).format()),
            components: {
                draw: {

                }
            }
        });

        makeReq('get', '/0/draw').expect(200).end(function(err, res) {
            assert.equal(res.body._id, 0);
            assert.equal(res.body.name, 'draw event');
            done(err);
        });
    });
});

function makeReq(method, path) {
    var req = request(ajax)[method](path);
    return req;
}