require('./pre-test');

var request = require('supertest'),
    async = require('async'),
    assert = require('chai').assert,
    configure = require('../lib/configure'),
    persist = require('persist'),
    app = require('../lib/admin');

require('./test-wrapper')(app);

describe('admin.results.js', function() {

    require('./pre-test').presuit();

    var now = new Date(),
        y = 1900 + now.getYear(),
        m = now.getMonth(),
        d = now.getDate(),
        testEvent = {
            name: 'event test',
            type: 'draw',
            startTime: new Date(y, m, d + 1),
            endTime: new Date(y, m, d + 4)
        }, id;

    beforeEach(function(done) {
        persist.create().events.insert({
            _id: 1
        });
        id = 1;
        setTimeout(done, 200);
    });

    afterEach(function(done) {
        persist.create().counter.remove();
        persist.create().events.remove();
        persist.create().results.remove();
        persist.create().results_debug.remove();
        setTimeout(done, 200);
    });



    it('event not found', function(done) {
        request(app)
            .post('/ajax/event/4/results/create')
            .expect(404)
            .end(done);
    });

    it('query 404', function(done) {
        async.waterfall([
            function(cb) {
                request(app).get('/ajax/event/' + id + '/results/notexistsid')
                    .expect(404).end(function(err) {
                        cb(err);
                    });
            }
        ], function(err, results) {
            done(err);
        });
    });


    it('debug results', function(done) {
        async.waterfall([
            function(cb) {
                request(app)
                    .post('/ajax/event/' + id + '/results/create')
                    .send({
                        debug: true,
                        userid: 'debug_user',
                        mobile: '123456'
                    }).expect(202)
                    .end(function(err) {
                        setTimeout(function() {
                            cb(err);
                        }, 100);
                    });
            },
            function(cb) {
                persist.create().results_debug.query(id, {
                    userid: 'debug_user'
                }, function(err, recs) {
                    if (err) return cb(err);
                    assert.lengthOf(recs, 1);
                    cb(err);
                });
            },
            function(cb) {
                persist.create().results.query(id, {
                    userid: 'debug_user'
                }, function(err, recs) {
                    assert.lengthOf(recs, 0);
                    cb(err);
                });
            },
            function(cb) {
                // test count
                request(app).get('/ajax/event/' + id + '/results/count?debug=true')
                    .expect(200, '1').end(function(err) {
                        cb(err);
                    });
            }
        ], done);
    });

    it('create results', function(done) {
        async.waterfall([
            function(cb) {
                request(app).post('/ajax/event/' + id + '/results/create')
                    .send({
                        userid: 'test_user',
                        mobile: '123456'
                    }).expect(202)
                    .end(function(err) {
                        setTimeout(function() {
                            cb(err);
                        }, 200);
                    });
            },
            function(cb) {
                persist.create().results.query(id, {}, function(err, recs) {
                    if (err) return cb(err);
                    assert.lengthOf(recs, 1);
                    cb(err, recs);
                });
            }
        ], function(err) {
            done(err);
        });
    });

    it('query resuls', function(done) {
        async.waterfall([
                function(cb) {
                    request(app).post('/ajax/event/' + id + '/results/create')
                        .send({
                            userid: 'test_user',
                            mobile: '123456'
                        }).expect(202)
                        .end(function(err) {
                            setTimeout(function() {
                                cb(err);
                            }, 200);
                        });
                },
                function(cb) {
                    persist.create().results.query(id, function(err, recs) {
                        cb(err, recs);
                    });
                },
                function(recs, cb) {
                    var rid = recs[0]._id.toHexString();
                    request(app).get('/ajax/event/' + id + '/results/' + rid)
                        .expect(200).end(function(err, res) {
                            if (err) return cb(err);
                            assert.equal(res.body.userid, 'test_user');
                            assert.equal(res.body._id, rid);
                            assert.equal(res.body.mobile, '123456');
                            cb(err, recs);
                        });
                },
                function(recs, cb) {
                    // test count
                    request(app).get('/ajax/event/' + id + '/results/count')
                        .expect(200, '1').end(function(err) {
                            cb(err, recs);
                        });
                },

                function(recs, cb) {
                    // test delete
                    request(app).del('/ajax/event/' + id + '/results/' + recs[0]._id)
                        .expect(200, '1').end(function(err) {
                            cb(err);
                        });
                },
                function(cb) {
                    persist.create().results.create(id, [{
                        userid: 'test'
                    }, {
                        userid: 'test1'
                    }, {
                        userid: 'test2'
                    }], function(err) {
                        persist.create().results.query(id, function(err, recs) {
                            if (err) return cb(err);
                            assert.lengthOf(recs, 3);
                            cb(err);
                        });
                    });
                },
                function(cb) {
                    request(app).get('/ajax/event/' + id + '/results/count')
                        .expect(200, '3')
                        .end(function(err) {
                            if (err) return cb(err);
                            // test page
                            request(app).get('/ajax/event/' + id + '/results/page/0?limit=2')
                                .expect(200).end(function(err, res) {
                                    if (err) return cb(err);
                                    assert.lengthOf(res.body, 2);
                                    cb(err);
                                });
                        });
                }
            ],
            function(err, rs) {
                done(err);
            });
    });
});