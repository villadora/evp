var config = require('../lib/configure'),
    assert = require('chai').assert;


describe('configure.js', function() {
    require('./pre-test').presuit();

    it('configure.init', function(done) {
        assert.isFunction(config.init);
        config.init(require('../bin/config.' + (process.env.NODE_ENV || "development")), function(err) {
            done(err);
        });
    });

    it('configure[properties]', function(done) {
        config.init(require('../bin/config.' + (process.env.NODE_ENV || "development")), function(err) {
            assert(config.app);
            assert(config.auth);
            assert(config.i18n);
            assert(config.persist);
            assert(config.mysql);
            assert(config.bunyanOptions);
            done(err);
        });
    });
});