require('./pre-test');

var request = require('supertest'),
    async = require('async'),
    moment = require('moment'),
    express = require('express'),
    assert = require('chai').assert,
    persist = require('persist'),
    configure = require('../lib/configure'),
    app = require('../lib/ajax'),
    i18n = require('i18n');

require('./test-wrapper')(app);

describe('ajax.js', function() {
    require('./pre-test').presuit();

    var oldGen;
    beforeEach(function() {
        oldGen = app.generator;
        app.generator = function(event) {
            var app = express();
            app.all('/:action', function(req, res) {
                return res.send(req.params.action);
            });
            return app;
        };
    });

    afterEach(function() {
        persist.create().events.remove();
        app.generator = oldGen;
        app.reset();
    });

    it('unmatch', function(done) {
        persist.create().events.remove();
        async.waterfall([
                function(cb) {
                    persist.create().events.insert({
                        _id: 0,
                        startTime: new Date(moment().startOf('day').add('days', 1).format()),
                        endTime: new Date(moment().startOf('day').add('days', 2).format())
                    });
                    setTimeout(function() {
                        request(app).get('/0/draw').expect(400)
                            .end(function(err, res) {
                                // for apiDebug, will not do automatcially hook
                                assert.equal(res.text, i18n.__('The event you visited does not start yet'));
                                cb(err);
                            });
                    }, 200);
                },
                function(cb) {
                    persist.create().events.findAndModify({
                            _id: 0
                        }, [], {
                            $set: {
                                startTime: new Date(moment().startOf('day').subtract('days', 5).format()),
                                endTime: new Date(moment().startOf('day').subtract('days', 3).format())
                            }
                        },
                        function(err) {
                            request(app).get('/0/draw').expect(400)
                                .end(function(err, res) {
                                    assert.equal(res.text, i18n.__('The event you visited was offline'));
                                    cb(err);
                                });
                        });
                },
                function(cb) {
                    persist.create().events.findAndModify({
                            _id: 0
                        }, [], {
                            $set: {
                                startTime: new Date(moment().startOf('day').subtract('days', 1).format()),
                                endTime: new Date(moment().startOf('day').add('days', 1).format())
                            }
                        },
                        function(err) {
                            request(app).get('/0/vote').expect(200, 'vote')
                                .end(function(err, res) {
                                    request(app).get('/0/draw').expect(200, 'draw')
                                        .end(function(err, res) {
                                            cb(err);
                                        });
                                });
                        });
                }
            ],
            function(err) {
                persist.create().events.remove();
                done(err);
            });
    });

    it('unmount', function(done) {
        app.use('/event', function(req, res) {
            res.send('OK');
        });
        app.unmount('/event');
        request(app).get('/event').expect(404).end(done);
    });

    it('reset', function() {
        var ok = false;
        app.once('hooked', function(id) {
            ok = typeof id == 'number';
        });
        assert(!app.hook(1, function(req, res, next) {}), 'hook will fail');
        assert(app.hook(1, function(req, res) {}), 'hook must be success');
        assert.ok(ok, 'ok should be true');
        assert(app.findHook(1), 'hooker 1 should be found');
        app.reset();
        assert(!app.findHook(1), 'now no hooker');
    });

    it('hook event with simple handler', function(done) {
        var ok = false,
            hook = function(req, res) {
                res.send('OK');
            };
        assert(!app.hook(1, function() {}), 'can not hook empty function');
        assert(app.hook(3, hook), 'hook failed');
        assert(app.findHook(3) === hook, 'can not find hook');

        app.once('unhooked', function(id) {
            ok = typeof id == 'number';
        });

        request(app).get('/3').expect(200, 'OK').end(function(err) {
            app.unhook(3);
            assert.ok(ok);
            assert(!app.findHook(3));
            done(err);
        });
    });

    it('hook event with express app', function() {
        var hook = express();
        assert(app.hook(2, hook));
        assert(app.findHook(2) === hook, 'can not find hook');
    });
});