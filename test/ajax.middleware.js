require('./pre-test');

var request = require('supertest'),
    async = require('async'),
    express = require('express'),
    assert = require('chai').assert,
    configure = require('../lib/configure'),
    dao = require('../lib/ajax/dao'),
    middleware = require('../lib/ajax/middleware'),
    encryption = require('common-encryption');


var app = express();
app.use(express.cookieParser());
app.use(middleware.dper);

app.get('/cookie/set', function(req, res) {
    res.cookie('dper', '00508301af0d74c87151d4388ef61a1ae08b81b56d84965c6df87c2f34b7df90');
    res.send();
});

app.get('/', function(req, res) {
    console.log(req.ip, req.dper);
    assert(req.dper.userId);
    res.send();
});

app.use(function(err, req, res, next) {
    next(err);
});

describe('ajax.middleware.js', function() {
    require('./pre-test').presuit();

    it('dper', function(done) {
        request(app).get('/cookie/set').expect(200).end(function(err, res) {
            var cookies = res.headers['set-cookie'].pop().split(';')[0];
            var req = request(app).get('/');
            // Set cookie to get saved user session
            req.set('Cookie', cookies);
            req.expect(200).end(function(err, res) {
                console.log(err, res.body);
                done(err);
            });
        });
    });

    it('encryption', function() {
        var text = encryption.encryptDefault('hello world');
        assert.equal(encryption.decryptDefault(text), 'hello world');
    });
});