require('./pre-test');

var request = require('supertest'),
    async = require('async'),
    moment = require('moment'),
    express = require('express'),
    assert = require('chai').assert,
    persist = require('persist'),
    configure = require('../lib/configure'),
    components = require('event-components'),
    ajax = require('../lib/ajax'),
    i18n = require('i18n');


require('./test-wrapper')(ajax);

var cookie = 'dper=00508301af0d74c87151d4388ef61a1ae08b81b56d84965c6df87c2f34b7df90';

describe('ajax.generator.js', function() {
    require('./pre-test').presuit();

    afterEach(function() {
        persist.create().events.remove();
        persist.create().counter.remove();
        persist.create().results.remove();
        ajax.reset();
    });


    it('draw generator', function(done) {
        ajax.hook(0, {
            _id: 0,
            name: 'draw event',
            startTime: new Date(moment().startOf('day').subtract('days', 1).format()),
            endTime: new Date(moment().startOf('day').add('days', 2).format()),
            components: components.parseConfigs({
                draw: {
                    "limitType": "byUser",
                    "limitPeriod": "event",
                    "limitNumber": "2",
                    "limitWins": "1",
                    "estimatedParticipants": "100",
                    "prizes": [{
                        "name": "prizeA",
                        "level": "2",
                        "type": 1,
                        "hardRate": 1,
                        "total": "100",
                        "message": "win a",
                        "sent": 0
                    }]

                }
            })
        });

        makeReq('get', '/0/draw').expect(200).end(function(err, res) {
            if (err) return done(err);
            console.log(res.body);
            var recordId = res.body.recordId;
            makeReq('get', '/0/userInfo').send({
                mobileNo: '192323232',
                recordId: recordId
            }).expect(200).end(function(err, res) {
                if (err) return done(err);
                console.log(res.body);
                makeReq('get', '/0/draw').expect(200).end(function(err, res) {
                    if (err) return done(err);
                    console.log(res.body);
                    makeReq('get', '/0/draw').expect(405).end(function(err, res) {
                        console.log(res.text);
                        done(err);
                    });
                });
            });
        });
    });
});

function makeReq(method, path) {
    var req = request(ajax)[method](path);
    req.set('Cookie', cookie);
    return req;
}