require('./pre-test');

var request = require('supertest'),
    async = require('async'),
    pending = require('./pending'),
    assert = require('chai').assert,
    configure = require('../lib/configure'),
    app = require('../lib/admin'),
    persist = require('persist');

require('./test-wrapper')(app);

describe('admin.draft.js', function() {
    require('./pre-test').presuit();

    var now = new Date(),
        y = now.getYear() + 1900,
        m = now.getMonth(),
        d = now.getDate();


    afterEach(function(done) {
        persist.create().events.remove();
        persist.create().drafts.remove();
        persist.create().counter.remove();
        setTimeout(done, 100);
    });

    it('save with array: wrong parameter', function(done) {
        request(app).post('/ajax/event/draft')
            .send([{
                name: 'xxx'
            }])
            .expect(400).end(done);
    });



    it('save/list/remove draft', function(done) {
        async.waterfall([
            function(cb) {
                // test create
                request(app)
                    .post('/ajax/event/draft')
                    .send({
                        name: 'xxx',
                        type: 'draw',
                        cityId: '3',
                        owner: 'tester',
                        url: 'http://event.dianping.com/event1',
                        startTime: new Date(y, m, d + 2),
                        endTime: new Date(y, m, d + 6)
                    }).expect(200).end(function(err, res) {
                        var id = res.body._id;
                        cb(err, id);
                    });
            },
            function(id, cb) {
                request(app)
                    .put('/ajax/event/draft')
                    .send({
                        _id: id,
                        cityId: '4'
                    })
                    .expect(200).end(function(err, res) {
                        assert.equal(res.body._id, id, 'id not change');
                        assert.equal(res.body.cityId, '4', 'cityId has changed');
                        assert.equal(res.body.url, 'http://event.dianping.com/event1');
                        assert.equal(res.body.owner, 'tester');
                        cb(err, id);
                    });
            },
            function(id, cb) {
                request(app)
                    .get('/ajax/event/draft/list')
                    .expect(200)
                    .end(function(err, res) {
                        assert.lengthOf(res.body, 0);
                        cb(err, id);
                    });
            },
            function(id, cb) {
                // test delete
                request(app)
                    .del('/ajax/event/draft/' + id)
                    .expect(200, {
                        id: id
                    }).end(function(err) {
                        cb(err);
                    });
            }
        ], done);
    });

});