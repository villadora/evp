var i18n = require('i18n'),
    bunyan = require('bunyan'),
    express = require('express');

i18n.configure({
    "locales": ['en', 'zh'],
    "defaultLocale": "en",
    "directory": "./locales",
    "extension": ".js",
    "updateFiles": true,
    "cookie": 'test-app'
});


module.exports = function(app) {

    app.stack.unshift({
        route: '',
        handle: express.cookieParser()
    });


    app.stack.unshift({
        route: '',
        handle: express.methodOverride()
    });

    app.stack.unshift({
        route: '',
        handle: express.bodyParser()
    });

    app.stack.unshift({
        route: '',
        handle: require('../lib/common/i18n').init // initialize i18n
    });

    app.use(function(err, req, res, next) {
        var msg = err.message || err.toString();
        console.log('Error', res.__.apply(res, [msg].concat(err.args), err.status));
        next(err);
    });

    app.use(require('../lib/common/onerror'));
};