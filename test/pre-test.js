var bunyan = require('bunyan'),
    ringbuffer = new bunyan.RingBuffer({
        limit: 100
    });

require('../lib/common/logger').__logger = bunyan.createLogger({
    "name": "test",
    streams: [{
        type: 'raw',
        stream: ringbuffer
    }]
});


module.exports.presuit = function() {
    before(function(done) {
        require('../lib/configure').init(require('../bin/config.' + (process.env.NODE_ENV || "development")), function(err) {
            done(err);
        });
    });

    after(function(done) {
        done();
    });
};