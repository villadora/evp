var assert = require('chai').assert,
    mysql = require('persist').mysql,
    dao = require('../lib/ajax/dao'),
    configure = require('../lib/configure');

describe('ajax.dao.js', function() {

    require('./pre-test').presuit();

    it('userAuthLevel', function(done) {
        dao.userAuthLevel.find(293249, function(err, level) {
            done(err);
        });
    });

    it('userBlackList', function(done) {
        dao.userBlackList.contains(223233, function(err, blocked) {
            assert(!blocked);
            done(err);
        });
    });


    it('configuration', function(done) {
        dao.configuration.find('UserBlackListOpen', function(err, settings) {
            assert.isString(settings);
            done(err);
        });
    });
});