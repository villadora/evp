require('./pre-test');

var request = require('supertest'),
    async = require('async'),
    pending = require('./pending'),
    assert = require('chai').assert,
    persist = require('persist'),
    configure = require('../lib/configure'),
    app = require('../lib/admin');

require('./test-wrapper')(app);

describe('admin.js', function() {

    require('./pre-test').presuit();

    var now = new Date(),
        y = now.getYear() + 1900,
        m = now.getMonth(),
        d = now.getDate();


    afterEach(function() {
        persist.create().events.remove();
        persist.create().counter.remove();
        setTimeout(done, 100);
    });


    it('root', function(done) {
        request(app)
            .get('/')
            .expect(200)
            .end(done);
    });


    it('operators on components', function(done) {
        async.waterfall([
            function(cb) {
                request(app).post('/ajax/event')
                    .send({
                        name: 'xxx',
                        components: JSON.stringify({
                            'draw': {
                                'limitType': 'byUser',
                                'limitPeriod': 'day',
                                'prizes': [{
                                    name: 'win1',
                                    type: 'level1',
                                    level: 1,
                                    total: 10,
                                    hardRate: 1.0
                                }]
                            }
                        }),
                        cityId: '5',
                        owner: 'tester',
                        url: 'http://event.dianping.com/event1',
                        startTime: new Date(y, m, d + 2),
                        endTime: new Date(y, m, d + 6)
                    }).expect(200).end(function(err, res) {
                        if (err) return cb(err);
                        cb(err, res.body._id);
                    });
            },
            function(id, cb) {
                request(app).put('/ajax/event/' + id + '/components').send({
                    'draw': {
                        'prizes': JSON.stringify([{
                            name: 'win1',
                            type: 2,
                            level: 2,
                            total: 100,
                            hardRate: 1.0
                        }, {
                            name: 'prize1',
                            type: 1,
                            level: 3,
                            total: 1000,
                            hardRate: 10
                        }])
                    }
                }).expect(200).end(function(err, res) {
                    cb(err, id);
                });
            },
            function(id, cb) {
                request(app).get('/ajax/event/' + id + '/components')
                    .send().expect(200).end(function(err, res) {
                        assert.lengthOf(res.body.draw.prizes, 2);
                        cb(err, id);
                    });
            }
        ], done);
    });

    it('create with array: wrong parameter', function(done) {
        request(app).post('/ajax/event')
            .send([{
                name: 'xxx',
                components: {
                    'draw': {}
                }
            }])
            .expect(400).end(done);
    });


    it('mget/mdel query', function(done) {
        async.waterfall([
                function(cb) {
                    request(app)
                        .post('/ajax/event')
                        .send({
                            name: 'xxx',
                            components: JSON.stringify({
                                'draw': {
                                    'limitType': 'byUser',
                                    'limitPeriod': 'day',
                                    'prizes': [{
                                        name: 'win1',
                                        type: 'level1',
                                        level: 1,
                                        total: 10,
                                        hardRate: 1.0
                                    }]
                                }
                            }),
                            cityId: '5',
                            owner: 'tester',
                            url: 'http://event.dianping.com/event1',
                            startTime: new Date(y, m, d + 2),
                            endTime: new Date(y, m, d + 6)
                        }).expect(200).end(function(err, res) {
                            if (err) return cb(err);
                            cb(err);
                        });
                },
                function(cb) {
                    request(app)
                        .post('/ajax/event')
                        .send({
                            name: 'test1',
                            components: JSON.stringify({
                                'draw': {
                                    'limitType': 'byUser',
                                    'limitPeriod': 'day',
                                    'prizes': [{
                                        name: 'win1',
                                        type: 'level1',
                                        level: 1,
                                        total: 10,
                                        hardRate: 1.0
                                    }]
                                }
                            }),
                            cityId: '6',
                            owner: 'tester',
                            url: 'http://event.dianping.com/event2',
                            startTime: new Date(y, m, d + 2),
                            endTime: new Date(y, m, d + 6)
                        }).expect(200).end(function(err) {
                            if (err) return cb(err);
                            cb(err);
                        });
                },
                function(cb) {
                    request(app).get('/ajax/event/page/0?limit=2')
                        .end(function(err, res) {
                            assert.lengthOf(res.body, 2);
                            cb(err);
                        });
                },
                function(cb) {
                    request(app).post('/ajax/event/page/0')
                        .send({
                            name: 'not exist',
                            limit: 2
                        })
                        .end(function(err, res) {
                            assert.lengthOf(res.body, 0, 'page for name not exist');
                            cb(err);
                        });
                },
                function(cb) {
                    request(app)
                        .get('/ajax/event/query?name=x')
                        .end(function(err, res) {
                            if (err) return cb(err);
                            assert(res.body.length > 0, 'query with name x');
                            cb(err);
                        });
                },
                function(cb) {
                    request(app)
                        .post('/ajax/event/query')
                        .send({
                            name: 'x',
                            components: ['draw'],
                            range_start: new Date(),
                            range_end: new Date(y, m, d + 8)
                        }).expect(200).end(function(err, res) {
                            if (err) return cb(err);
                            assert(res.body.length > 0, 'query with range');
                            cb(err);
                        });
                },
                function(cb) {
                    request(app)
                        .del('/ajax/event/query?components=draw')
                        .send({
                            components: 'draw'
                        })
                        .expect(200, '2').end(function(err, res) {
                            cb(err);
                        });
                }
            ],
            function(err) {
                done(err);
            });
    });


    it('POST|DELETE|PUT|PATCH /event/(create|destroy|update)', function(done) {
        async.waterfall([
            function(cb) {
                // test create
                request(app)
                    .post('/ajax/event')
                    .send({
                        name: 'xxx',
                        cityId: '3',
                        owner: 'tester',
                        url: 'http://event.dianping.com/event1',
                        startTime: new Date(y, m, d + 2),
                        endTime: new Date(y, m, d + 6),
                        components: JSON.stringify({
                            'draw': {
                                'limitType': 'byUser',
                                'limitPeriod': 'day',
                                'prizes': [{
                                    name: 'win1',
                                    type: 'level1',
                                    level: 1,
                                    total: 10,
                                    hardRate: 1.0
                                }]
                            }
                        })
                    }).expect(200).end(function(err, res) {
                        var id = res.body._id;
                        cb(err, id);
                    });
            },
            function(id, cb) {
                request(app)
                    .patch('/ajax/event/' + id)
                    .send({
                        cityId: '4'
                    })
                    .expect(200).end(function(err, res) {
                        assert.equal(res.body._id, id);
                        assert.equal(res.body.cityId, '4');
                        assert.equal(res.body.url, 'http://event.dianping.com/event1');
                        assert.equal(res.body.owner, 'tester');

                        cb(err, id);
                    });
            },
            function(id, cb) {
                // test delete
                request(app)
                    .del('/ajax/event/' + id)
                    .expect(200, {
                        id: id
                    }).end(function(err) {
                        cb(err);
                    });
            }
        ], done);
    });


    it('POST|DELETE|GET /event/(create|destroy|query)', function(done) {
        async.waterfall([
            function(cb) {
                request(app)
                    .post('/ajax/event')
                    .send({
                        name: 'xxx',
                        cityId: '3',
                        url: 'http://event.dianping.com/event1',
                        startTime: new Date(y, m, d - 1),
                        endTime: new Date(y, m, d + 1),
                        components: JSON.stringify({
                            'draw': {
                                'limitType': 'byUser',
                                'limitPeriod': 'day',
                                'prizes': [{
                                    name: 'win1',
                                    type: 'level1',
                                    level: 1,
                                    total: 10,
                                    hardRate: 1.0
                                }]
                            }
                        })
                    }).expect(400).end(function(err, res) {
                        cb(err);
                    });
            },
            function(cb) {
                // test create
                request(app)
                    .post('/ajax/event')
                    .send({
                        name: 'xxx',
                        cityId: '5',
                        owner: 'tester',
                        url: 'http://event.dianping.com/event1',
                        startTime: new Date(y, m, d + 3),
                        endTime: new Date(y, m, d + 6),
                        components: JSON.stringify({
                            'draw': {
                                'limitType': 'byUser',
                                'limitPeriod': 'day',
                                'prizes': [{
                                    name: 'win1',
                                    type: 'level1',
                                    level: 1,
                                    total: 10,
                                    hardRate: 1.0
                                }]
                            }
                        })
                    }).expect(200).end(function(err, res) {
                        var id = res.body._id;
                        cb(err, id);
                    });
            },
            function(id, cb) {
                // test get
                request(app)
                    .get('/ajax/event/' + id)
                    .expect(200).end(function(err, res) {
                        assert.equal(res.body._id, id);
                        assert.equal(res.body.name, 'xxx');
                        assert(res.body.components.draw);
                        assert.equal(res.body.cityId, '5');
                        assert.equal(res.body.url, 'http://event.dianping.com/event1');
                        assert.equal(res.body.owner, 'tester');
                        assert.equal(new Date(res.body.startTime).getTime(), new Date(y, m, d + 3).getTime());
                        cb(err, id);
                    });
            },
            function(id, cb) {
                // test delete
                request(app)
                    .del('/ajax/event/' + id)
                    .expect(200, {
                        id: id
                    })
                    .end(function(err) {
                        cb(err);
                    });
            }
        ], done);
    });

    it('GET /event/:id/delete', function(done) {
        request(app)
            .put('/ajax/event')
            .send({
                name: 'xxx',
                cityId: '5',
                owner: 'tester',
                url: 'http://event.dianping.com/event1',
                startTime: new Date(y, m, d + 3),
                endTime: new Date(y, m, d + 6),
                components: JSON.stringify({
                    'draw': {
                        'limitType': 'byUser',
                        'limitPeriod': 'day',
                        'prizes': [{
                            name: 'win1',
                            type: 'level1',
                            level: 1,
                            total: 10,
                            hardRate: 1.0
                        }]
                    }
                })
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                var id = res.body._id;
                request(app)
                    .get('/ajax/event/' + id + '/delete')
                    .expect(200, {
                        id: id
                    })
                    .end(done);
            });
    });


    it('PUT /event/create', function(done) {
        request(app)
            .put('/ajax/event')
            .send({
                name: 'xxx',
                cityId: '5',
                owner: 'tester',
                url: 'http://event.dianping.com/event1',
                startTime: new Date(y, m, d + 3),
                endTime: new Date(y, m, d + 6),
                components: JSON.stringify({
                    'draw': {
                        'limitType': 'byUser',
                        'limitPeriod': 'day',
                        'prizes': [{
                            name: 'win1',
                            type: 'level1',
                            level: 1,
                            total: 10,
                            hardRate: 1.0
                        }]
                    }
                })
            })
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                var id = res.body._id;

                request(app)
                    .del('/ajax/event/' + id)
                    .expect(200, {
                        id: id
                    })
                    .end(done);
            });
    });

});