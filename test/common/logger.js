var logger = require('../../lib/common/logger'),
    assert = require('chai').assert;

describe('logger.js', function() {
    it('logger', function() {
        assert(logger.__logger);
        var log = logger.createLogger('logger test');
        assert.isFunction(log.info);
        assert.isFunction(log.error);
        log.info('Hello');
    });
});