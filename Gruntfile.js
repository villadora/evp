var fs = require('fs'),
    readdirp = require('readdirp'),
    path = require('path');


module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        jshint: {
            all: ['Gruntfilejs', 'lib/**/*.js'],
            options: {
                jshintrc: '.jshintrc'
            }
        },
        watch: {
            options: {
                nospawn: false
            },
            js: {
                files: ['lib/**/*.js', 'test/**/*.js'],
                tasks: ['jshint', 'test']
            }
        }
    });


    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerTask("default", ["jshint"]);

    grunt.registerTask("test", "run mocha test", function() {
        var done = this.async(),
            Mocha = require('mocha'),
            mocha = new Mocha({
                reportor: 'spec'
            });

        readdirp({
            root: './test',
            fileFilter: '*.js'
        }).on('data', function(entry) {
            mocha.addFile(entry.fullPath);
        }).on('end', function() {
            mocha.run(function(failures) {
                done(failures);
            });
        });
    });
};
