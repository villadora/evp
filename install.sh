#!/bin/bash

basedir="$(pwd)"

Modules=('common-encryption' 'event-components' 'cache-service' 'persist', 'user-service')

mkdir -p node_modules || {
    echo "Fail to make directory: ./node_modules"
    exit 1
}

for module in ${Modules[@]};do
    echo "install dependencies ${module}"
    cd "${basedir}/modules/${module}"
    npm install || {
        echo "Fail to install dependencies for: ${module}"
        exit 1
    }
done

cd "${basedir}"
npm install #--force
