## 问题

### url rewrite的处理
java中url rewrite处理方式的影响, 如其他接口是否会有影响, 是forward还是直接redirect
如event.dianping.com/ajax, event.dianping.com/logout等是否和部署在event.dianping.com上的app有关

### 线上是否对404等http status有rewrite
同时两个服务器一个对外一个只对内是否有办法配置?

### pubsub中间件
有哪些pubsub中间件可以用，redis是否有使用如何配置，swallow用何种协议.

### Lion数据读取方式
配置目前是不是通过zookeeper管理，线上java是如何配置的，是使用线上配置文件还是在container中配置? 如果要访问，线上地址是如何配置的?

### SMSService配置 (以后其他接口如优惠券也需要同样的方式解决)
有没有http接口，使用的rpc协议目前有哪些可以提供调用，是否只有hessian和java serialization, 有没有别的方法; thrift是否能用

### MONGODB & MYSQL 数据库访问
mysql的master-slave是否是读取只从slave中，线上有没有集群？数据库访问连接和频率的限制是怎样的？

### 数据库的缓存处理
Java统一使用的avatar-cache，后面是不是使用的memcached接口, 如果可以直接使用memcached接口并且清缓存是统一的，使用接口的java代码是怎样的，可以做一套node的接口版本