test: jshint
	@./node_modules/.bin/mocha --recursive -R spec

jshint:
	@./node_modules/.bin/jshint Gruntfile.js lib/**/*.js bin/*.js modules/**/*.js


.PHONY: jshint test 
